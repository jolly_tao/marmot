import os
import unittest
from marmot.common.raw.page import read_page
from marmot.common.raw.document import Document

class TestReadXMLRawPage(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath('./test/sample/basic/raw/2.xml')
        dir = os.path.abspath('./test/sample/basic/raw')
        raw_doc = Document(dir)
        self.page = read_page(raw_doc, path)

    def tearDown(self):
        del self.page

    def test_read(self):
        print len(self.page)

if __name__ == '__main__':
    unittest.main()
