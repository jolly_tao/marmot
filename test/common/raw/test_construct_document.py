import os
import unittest
from marmot.common.raw.document import Document

class TestConstructDocument(unittest.TestCase):

    def setUp(self):
        self.dir = os.path.abspath('./test/sample/basic/raw')

    def tearDown(self):
        pass

    def test_construct(self):
        doc = Document(self.dir)
        self.assertEqual(len(doc), 1)
        self.assertTrue(2 in doc)

if __name__ == '__main__':
    unittest.main()
