from marmot.common.physical.page import IDManager
import unittest

class TestIDManager(unittest.TestCase):

    def setUp(self):
        self.id_manager = IDManager(15)

    def tearDown(self):
        pass

    def test_allocate_id(self):
        ids = set()
        for i in range(100):
            ids.add(self.id_manager.allocate_id('block'))
        self.assertEqual(len(ids), 100)

    def test_reclaim_id(self):
        ids = [self.id_manager.allocate_id('block')
               for i in xrange(IDManager.max_number)]
        for id in ids:
            self.id_manager.reclaim_id(id)
        next_id = self.id_manager.allocate_id('block')
        self.assertEqual(next_id, 'p15b0')
