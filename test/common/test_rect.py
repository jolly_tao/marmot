from marmot.common.rect import Rect
import unittest

class TestRect(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_unite(self):
        lhs = Rect(0, 0, 2, 3)
        rhs = Rect(2, 3, 3, 2)
        union = lhs.unite(rhs)
        self.assertEqual(union.x, 0)
        self.assertEqual(union.y, 0)
        self.assertEqual(union.width, 5)
        self.assertEqual(union.height, 5)

    def test_intersect(self):
        lhs = Rect.from_point((0, 0), (2, 3))
        rhs = Rect.from_point((1, 2), (4, 4))
        intersection = lhs.intersect(rhs)
        self.assertEqual(intersection.x, 1)
        self.assertEqual(intersection.y, 2)
        self.assertEqual(intersection.width, 1)
        self.assertEqual(intersection.height, 1)

    def test_contain(self):
        lhs = Rect.from_point((0, 0), (2, 3))
        rhs = Rect.from_point((1, 2), (4, 4))
        self.assertFalse(lhs.contain(rhs))

        lhs = Rect.from_point((0, 0), (7, 8))
        rhs = Rect.from_point((1, 2), (3, 4))
        self.assertTrue(lhs.contain(rhs))

        lhs = Rect(1, 2, 3, 4)
        rhs = lhs.copy()
        self.assertTrue(lhs.contain(rhs))

    def test_overlap(self):
        lhs = Rect.from_point((0, 0), (2, 3))
        rhs = Rect.from_point((1, 2), (4, 4))
        self.assertTrue(lhs.overlap(rhs))

        lhs = Rect.from_point((0, 0), (2, 3))
        rhs = Rect.from_point((2, 3), (4, 4))
        self.assertFalse(lhs.overlap(rhs))

    def test_distance_x(self):
        lhs = Rect.from_point((1, 2), (3, 4))
        rhs = Rect.from_point((7, 8), (9, 10))
        self.assertEqual(lhs.distance_x(rhs), 4)

        lhs = Rect.from_point((1, 2), (5, 6))
        rhs = Rect.from_point((3, 4), (7, 8))
        self.assertEqual(lhs.distance_x(rhs), -2)

    def test_distance_y(self):
        lhs = Rect.from_point((1, 2), (3, 4))
        rhs = Rect.from_point((7, 8), (9, 10))
        self.assertEqual(lhs.distance_x(rhs), 4)

        lhs = Rect.from_point((1, 2), (5, 6))
        rhs = Rect.from_point((3, 4), (7, 8))
        self.assertEqual(lhs.distance_x(rhs), -2)

if __name__ == '__main__':
    unittest.main()
