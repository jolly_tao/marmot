from marmot.claw.model.editor import Editor
from marmot.claw.model.command import Command
import unittest

class TestEditor(unittest.TestCase):

    def setUp(self):
        self.editor = Editor()

    def tearDown(self):
        pass

    def test_undo_redo(self):
        n = 10
        for i in range(n):
            self.editor.append(DummyCommand())

        for i in range(n):
            self.assertTrue(self.editor.undo())
        self.assertFalse(self.editor.undo())

        for i in range(n):
            self.assertTrue(self.editor.redo())
        self.assertFalse(self.editor.redo())

    def test_is_dirty(self):
        # []
        self.assertFalse(self.editor.is_dirty())

        # [a0/]
        self.editor.append(DummyCommand())
        self.assertTrue(self.editor.is_dirty())

        # [a0/, b0/]
        self.editor.append(DummyCommand())
        # [a0/, b0/ |]
        self.editor.save()
        self.assertFalse(self.editor.is_dirty())

        # [a0/, b0/, | c0/, d0/]
        self.editor.append(DummyCommand())
        self.editor.append(DummyCommand())
        self.assertTrue(self.editor.is_dirty())

        # [a0/, b0/, | c0/, d0\]
        self.editor.undo()
        self.assertTrue(self.editor.is_dirty())

        # [a0/, b0/, | c0\, d0\]
        self.editor.undo()
        self.assertFalse(self.editor.is_dirty())

        # [a0/, b0\, | c0\, d0\]
        self.editor.undo()
        self.assertTrue(self.editor.is_dirty())

        # [a0/, b0/, | c0\, d0\]
        self.editor.redo()
        self.assertFalse(self.editor.is_dirty())

        # [a0/, b1/]
        self.editor.undo()
        self.editor.append(DummyCommand())
        self.assertTrue(self.editor.is_dirty())

        # [a0/, b1/ |]
        self.editor.save()
        self.assertFalse(self.editor.is_dirty())

class DummyCommand(Command):
    def do(self):
        pass

    def undo(self):
        pass



if __name__ == '__main__':
    unittest.main()
