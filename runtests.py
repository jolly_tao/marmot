import unittest
import os
import sys

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(path)
# sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, path)
# print path

loader = unittest.defaultTestLoader
suite = loader.discover('test', pattern='test_*.py')
unittest.TextTestRunner(verbosity=2).run(suite)
