# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import sys
    import os
    path = os.path.abspath(__file__)
    for i in range(4):
        path = os.path.dirname(path)
    sys.path.insert(0, path)
    from sklearn.metrics import accuracy_score
    from marmot.brain.io import load_data
    from marmot.brain.svm import train_svm
    from marmot.brain.crf import crf_nll
    from marmot.brain.crf import crf_pseudo_nll

import pickle
import numpy as np
import scipy
from model_construction import make_weights
from model_construction import make_weight_map

def train_crf(data, svm_clf, nr_states, weight_maps):
    weights = make_weights(weight_maps)
    res = scipy.optimize.minimize(_obj_func_batch, weights,
                                  method='L-BFGS-B',
                                  args=(crf_pseudo_nll, data, svm_clf, nr_states, weight_maps),
                                  jac=True, options={'disp': True})
    return res.x

def _obj_fun_instance(weights, func, page_data, svm_clf, nr_states, weight_maps):
    node_map, edge_map = weight_maps
    
    # derive *crf* node features from svm classifier
    nodes = page_data.cliques['node']
    node_features = page_data.features['node']
    edges = page_data.cliques['mst']
    edge_features = page_data.features['mst']
    svm_log_proba = svm_clf.predict_log_proba(node_features)

    # make templates
    templates = ((nodes, svm_log_proba, node_map),
                 (edges, edge_features, edge_map))
    nll, grad = func(weights, page_data.y, nr_states, templates)
    return nll, grad

def _obj_func_batch(weights, func, data, svm_clf, nr_states, weight_maps):
    nll_batch = 0
    grad_batch = np.zeros_like(weights, dtype=np.float)

    for page_data in data:
        nll, grad = _obj_fun_instance(weights, func, page_data,
                                      svm_clf, nr_states, weight_maps)
        nll_batch += nll
        grad_batch += grad

    return nll_batch, grad_batch

if __name__ == '__main__':
    dataset = load_data('../io/test-load-all.data')#train.data')
    dataset.clique_names = ['node', 'mst']

    nr_states = dataset.nr_states
    base = 0
    node_map, base = make_weight_map(1, nr_states, nr_states, base)
    edge_map, base = make_weight_map(2, dataset.nr_features['mst'], nr_states, base)
    weight_maps = (node_map, edge_map)
    svm_clf = train_svm(dataset.data)
    weights = train_crf(dataset.data, svm_clf, dataset.nr_states, weight_maps)
