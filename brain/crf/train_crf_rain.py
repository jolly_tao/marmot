"""
Refer to http://www.di.ens.fr/~mschmidt/Software/UGM/trainCRF.html
section "Maximum Likelihood Training of Log-Linear Conditionals Random Fields"
rain.mat is the same data file used in the matlab example.
"""

import scipy.io as sio
import numpy as np
import opengm
import itertools
import scipy.optimize
from model_construction import make_template_cliques, make_weights

from model_construction import make_graphical_model

def crf_pseudo_nll(weights, y, nr_states, templates):
    """Compute pseudo likelihood and gradient of the model.
    @param weights      model parameters.
    @param y            labels, concrete assignment to random variables.
    @param nr_states    number of states a variable can attain.
    @param template_cliques groups of cliques belonging to different
                            templates, e.g.
                            ((node_clique_1, ..., node_clique_n),
                             (edge_clique_1, ..., edge_clique_m),
                             (cliques of template a),
                             ...
                             )
    @return (negtive_log_pseudo_likelihood, gradient_of_weights)
    """
    gm, _, _, var2cliques = make_graphical_model(
        weights, y, nr_states, templates)
    nr_nodes = gm.numberOfVariables
    nll = 0
    grad = np.zeros_like(weights, dtype=np.float)

    for n in range(nr_nodes):
        # factors containing variable n
        factor_indices = gm.factorIndices([n])
        pot = np.ones(nr_states, dtype=np.float)
        for index in factor_indices:
            factor = gm[int(index)]
            variables = np.array(factor.variableIndices)
            # factor with neighbor variables fixed
            fixed_vars = np.where(variables != n)
            fixed_var_states = y[variables[fixed_vars]]
            sub_factor = factor.subFactor(fixed_vars, fixed_var_states)
            pot = np.multiply(pot, sub_factor)
        nll += -np.log(pot[y[n]]) + np.log(pot.sum())

        node_bel = pot / pot.sum()

        crf_pseudo_nll_update_grad(grad, y, nr_states,
                                    node_bel, n, var2cliques[n])

    return (nll, grad)

def crf_pseudo_nll_update_grad(grad, y, nr_states, node_bel, n, cliques):
    """
    @param grad         gradient to be updated, vector, np.array
    @param y            ground-truth, vector, np.array
    @param nr_states    number of states, scalar, int
    @param node_bel     node belief, vector, np.array
    @param n            random variable id, scalar, int
    @param cliques      cliques containing random variable n, list
    """
    obs_s = int(y[n])
    for c in cliques:
        variables = c.variables
        weight_map = c.weight_map
        features = c.features
        
        # clique_states = y[np.array(variables)]
        clique_states = [y[v] for v in variables]
        n_index = variables.index(n)
        # keep assignment of neighbors fixed, change state of n
        for s in xrange(nr_states):
            bel = node_bel[s]
            clique_states[n_index] = s
            w_indices = weight_map[tuple(clique_states)]
            obs = int(s == obs_s)
            for f, w_index in enumerate(w_indices):
                if np.isnan(w_index):
                    continue
                feature = features[f]
                grad[w_index] += feature * (bel - obs)

# from crf_nll import crf_nll
# from crf_pseudo_nll import crf_pseudo_nll, crf_pseudo_nll_update_grad

# load data
mat_contents = sio.loadmat('rain.mat')
x = mat_contents['X']
y = x
x = x.astype(np.float)
nr_instances, nr_nodes = y.shape
nr_states = 2

nodes = tuple((i,) for i in xrange(nr_nodes))
edges = tuple(itertools.imap(lambda x, y: x + y, nodes[:-1], nodes[1:]))
nr_edges = len(edges)

nr_node_features = 1
nr_edge_features = 1
node_features = np.ones((nr_instances, nr_nodes, nr_node_features),
                        dtype=np.float)
edge_features = np.ones((nr_instances, nr_edges, nr_edge_features),
                        dtype=np.float)

def make_clique_template_maps(nr_nodes, nr_edges,
                              nr_node_features, nr_edge_features,
                              nr_states):
    # np.array of float data type, bad idea
    node_map = np.empty((nr_states, nr_node_features),
                        dtype=np.float)
    node_map.fill(np.NaN)
    node_map[0, :] = 0

    edge_map = np.empty((nr_states, nr_states, nr_edge_features),
                        dtype=np.float)
    edge_map.fill(np.NaN)
    edge_map[0, 0, :] = 1
    edge_map[1, 0, :] = 2
    edge_map[0, 1, :] = 3

    return (node_map, edge_map)

def crf_nll_batch(weights, y, nr_states, features, weight_maps):
    nr_instances = y.shape[0]
    nll_batch = 0
    grad_batch = np.zeros(weights.shape, dtype=np.float)

    node_map, edge_map = weight_maps

    for i in range(10):    # subset of data
    # for i in range(nr_instances):
        node_features, edge_features = features
        templates = ((nodes, node_features[i, :], node_map),
                     (edges, edge_features[i, :], edge_map))

        nll, grad = crf_pseudo_nll(weights, y[i, :], nr_states, templates)
        nll_batch += nll
        grad_batch += grad

    return nll_batch, grad_batch

def train_crf(x, y, node_features, edge_features):
    weight_maps = make_clique_template_maps(nr_nodes, nr_edges,
                                            nr_node_features,
                                            nr_edge_features,
                                            nr_states)
    features = (node_features, edge_features)
    weights = make_weights(weight_maps)
    res = scipy.optimize.minimize(crf_nll_batch, weights,
                                  method='L-BFGS-B',
                                  args=(y, nr_states, features, weight_maps),
                                  jac=True, options={'epsilon': 1e-05, 'disp': True})
    print res
    return res.x

if __name__ == '__main__':
    # import cProfile
    # cProfile.run("train_crf(x, y, node_features, edge_features)", "rain.prof")
    # # weights = train_crf(x, y, node_features, edge_features)
    # # print weights
    # import pstats
    # p = pstats.Stats("rain.prof")
    # p.sort_stats("time").print_stats()

    train_crf(x, y, node_features, edge_features)
