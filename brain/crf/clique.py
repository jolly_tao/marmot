# -*- coding: utf-8 -*-

import numpy as np

class Clique(object):
    def __init__(self, variables, nr_states):       
        variables = map(int, variables)
        self._variables = tuple(variables)

    @property
    def variables(self):
        return self._variables

    @property
    def features(self):
        return self._features

    @features.setter
    def features(self, features):
        self._features = features

    @property
    def weight_map(self):
        return self._weight_map

    @weight_map.setter
    def weight_map(self, weight_map):
        self._weight_map = weight_map
