# -*- coding: utf-8 -*-

import itertools
import numpy as np
import opengm
from model_construction import make_graphical_model

def crf_nll(weights, y, nr_states, templates):
    """Compute likelihood and gradient of the model.
    @param weights      model parameters.
    @param y            labels, concrete assignment to random variables.
    @param nr_states    number of states a variable can attain.
    @template_cliques   groups of cliques belonging to different templates,
                        e.g. ((node_clique_1, ..., node_clique_n),
                              (edge_clique_1, ..., edge_clique_m),
                              (cliques of template a),
                              ...
                              )
    @return (negtive_log_likelihood, gradient_of_weights)
    """
    gm, template_cliques, template_clique_fis, _ = make_graphical_model(
        weights, y, nr_states, templates)
    template_clique_bels, log_z = _infer(y, gm, template_clique_fis)
    nll = -np.log(gm.evaluate(y)) + log_z

    # compute gradient
    grad = np.zeros_like(weights, dtype=np.float)
    for bels, cliques in zip(template_clique_bels, template_cliques):
        _crf_nll_update_grad(grad, y, nr_states, bels, cliques)

    return (nll, grad)

def _crf_nll_update_grad(grad, y, nr_states, bels, cliques):
    for i, c in enumerate(cliques):
        state_space = itertools.product(range(nr_states),
                                        repeat=len(c.variables))
        for s in state_space:
            w_indices = c.weight_map[s]
            for f, w_index in enumerate(w_indices):
                if np.isnan(w_index):
                    continue
                grad[w_index] += c.features[f] * bels[i][s]
                if np.array_equal(s, y[c.variables]):
                    grad[w_index] -= c.features[f]
                    
def _infer(y, gm, template_clique_fis):
    # return template_clique_bels, log_z

    inf = opengm.inference.BeliefPropagation(gm, 'integrator')
    inf.infer()

    def _normalize(bels):
        # return a legitimate probability distribution of bels
        # sum over all states of a clique
        axis_sum = bels.sum(axis=tuple(range(1, bels.ndim)))
        print axis_sum
        # reshape axis_sum and use 'broadcasting' to normalize
        new_shape = tuple([len(axis_sum)] + [1] * (bels.ndim - 1))
        return bels / np.reshape(axis_sum, new_shape)

    template_clique_bels = [_normalize(inf.factorMarginals(clique_fis))
                            for clique_fis in template_clique_fis]

    # compute partition function
    # partition function is sum of marginals of an arbitary clique
    # after inference
    log_z = np.log(inf.marginals([0]).sum())

    return (template_clique_bels, log_z)
