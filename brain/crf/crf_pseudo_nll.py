# -*- coding: utf-8 -*-
    
import numpy as np
import opengm
from model_construction import make_graphical_model

def crf_pseudo_nll(weights, y, nr_states, templates):
    """Compute pseudo likelihood and gradient of the model.
    @param weights      model parameters.
    @param y            labels, concrete assignment to random variables.
    @param nr_states    number of states a variable can attain.
    @param template_cliques groups of cliques belonging to different
                            templates, e.g.
                            ((node_clique_1, ..., node_clique_n),
                             (edge_clique_1, ..., edge_clique_m),
                             (cliques of template a),
                             ...
                             )
    @return (negtive_log_pseudo_likelihood, gradient_of_weights)
    """
    gm, _, _, var2cliques = make_graphical_model(
        weights, y, nr_states, templates)
    nr_nodes = gm.numberOfVariables
    nll = 0
    grad = np.zeros_like(weights, dtype=np.float)

    for n in range(nr_nodes):
        # factors containing variable n
        factor_indices = gm.factorIndices([n])
        pot = np.ones(nr_states, dtype=np.float)
        for index in factor_indices:
            factor = gm[int(index)]
            variables = np.array(factor.variableIndices)
            # factor with neighbor variables fixed
            fixed_vars = np.where(variables != n)
            fixed_var_states = y[variables[fixed_vars]]
            sub_factor = factor.subFactor(fixed_vars, fixed_var_states)
            pot = np.multiply(pot, sub_factor)
        nll += -np.log(pot[y[n]]) + np.log(pot.sum())

        node_bel = pot / pot.sum()

        _crf_pseudo_nll_update_grad(grad, y, nr_states,
                                    node_bel, n, var2cliques[n])

    return (nll, grad)

def _crf_pseudo_nll_update_grad(grad, y, nr_states, node_bel, n, cliques):
    """
    @param grad         gradient to be updated, vector, np.array
    @param y            ground-truth, vector, np.array
    @param nr_states    number of states, scalar, int
    @param node_bel     node belief, vector, np.array
    @param n            random variable id, scalar, int
    @param cliques      cliques containing random variable n, list
    """
    for c in cliques:
        variables = c.variables
        weight_map = c.weight_map
        features = c.features
        obs_s = y[n]
        
        # clique_states = y[np.array(variables)]
        clique_states = [y[v] for v in variables]
        n_index = variables.index(n)
        # keep assignment of neighbors fixed, change state of n
        for s in xrange(nr_states):
            bel = node_bel[s]
            clique_states[n_index] = s
            w_indices = weight_map[tuple(clique_states)]
            obs = int(s == obs_s)
            for f, w_index in enumerate(w_indices):
                if np.isnan(w_index):
                    continue
                feature = features[f]
                # grad[w_index] += feature * (node_bel[s] - obs)
                grad[w_index] += feature * (bel - obs)
