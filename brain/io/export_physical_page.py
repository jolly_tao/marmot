# -*- coding: utf-8 -*-

import numpy as np
from marmot.common.label import *
from marmot.brain.feature.node_descriptor import NodeDescriptor
from marmot.brain.feature.edge_descriptor import MSTDescriptor, FigcapDescripor
from marmot.brain.feature.context import Context
# from marmot.brain.util import label_encoder
from marmot.brain.util import transform_fine
from marmot.brain.util import transform_coarse


def _scale_min_max(features):
    """for each instance feature,
    new_feature = (feature - feature_min) / (feature_max - feature_min).
    """
    features_min = np.amin(features, axis=0)
    features_max = np.amax(features, axis=0)
    indices = tuple(np.where(features_min == features_max))
    features_min[indices] = 0
    features_max[indices] = 1
    # use broadcasting
    return (features - features_min) / (features_max - features_min)

def export_physical_page(f, doc_name, physical_doc, page_num, img_path):
    page = physical_doc[page_num]
    context = Context(page, img_path)
    node_descriptors = context.descriptors

    edge_types = context.edge_types

    # ------------------------------------------------------------
    # Document header
    # document name
    f.write('doc_name=%s\n' % doc_name)
    # document page number
    f.write('page_num=%d\n' % page_num)

    # number of nodes
    f.write('nr_nodes=%d\n' % len(node_descriptors))
    # frament ids
    f.write('%s\n' % '\n'.join(d.id for d in node_descriptors))

    # labels, starting from 0
    # labels = label_encoder.transform([d.logical_label
    #                                   for d in node_descriptors])
    labels = transform_fine([d.logical_label for d in node_descriptors])
    f.write('%s\n' % ' '.join(str(label) for label in labels.astype(int)))

    # ------------------------------------------------------------
    # Cliques
    for edge_type in edge_types:
        _write_cliques(f, edge_type, context.edges[edge_type])

    # ------------------------------------------------------------
    # Features

    # node features
    node_features = np.array([d.value() for d in node_descriptors],
                             np.float)
    _write_features(f, 'node', node_features)

    # edge features
    for edge_type in edge_types:
        edge_features = _make_edge_features(node_descriptors, context,
                                       context.edges[edge_type],
                                       context.edge_descriptors[edge_type])
        _write_features(f, edge_type, edge_features)

    f.write('\n')

def _write_cliques(f, clique_type, cliques):
    nr_cliques = len(cliques)
    f.write('clique_type=%s\n' % clique_type)
    f.write('nr_cliques=%s\n' % nr_cliques)
    for clique in cliques:
        f.write('%s\n' % ' '.join(str(node) for node in clique))

def _write_features(f, feature_type, features):
    _, nr_features = features.shape
    f.write('feature_type=%s\n' % feature_type)
    f.write('nr_features=%d\n' % nr_features)
    for row in features:
        f.write('%s\n' % ' '.join(str(value) for value in row))

def _make_edge_features(node_descriptors, context, edges, edge_descriptor):
    if not edges:
        return np.zeros((0, 0), np.float)
        
    edge_descriptors = [edge_descriptor(node_descriptors[i],
                                        node_descriptors[j],
                                        context)
                                        for i, j in edges]
    edge_features = np.array([d.value() for d in edge_descriptors],
                             np.float)
    return _scale_min_max(edge_features)
