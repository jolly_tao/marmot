import numpy as np

def load_y(file_path):
    with open(file_path) as f:
        y = [int(line) for line in f]
        return np.array(y)

if __name__ == '__main__':
    y = load_y('test_y.txt')
    print y.shape
