# -*- coding: utf-8 -*-
"""
Export data as text file:
1>  read physical xml
2>  export labels, adjacent matrix, local features and relational
    features to text file.
"""

import sys
import os
import argparse

# add marmot to python path
path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from export_physical_page import export_physical_page
from marmot.brain.util import label_encoder
from marmot.brain.batch.batch_template import batch_template

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='export physical xmls, export labels, adjacent matrix, '
                    'local features and relational features to text file.')
    parser.add_argument('target',
                        help='index of sample data names and pages.')
    parser.add_argument('text_file',
                        help='output text file.')
    args = parser.parse_args()

    with open(args.text_file, 'w') as f:
        f.write('nr_labels=%d\n' % len(label_encoder.classes_))
        f.write('\n'.join(label_encoder.classes_))
        f.write('\n\n')

        def exporter(name, physical_doc, page_num, img_path):
            return export_physical_page(f, name, physical_doc,
                                        page_num, img_path)
        batch_template(args.target, exporter)
