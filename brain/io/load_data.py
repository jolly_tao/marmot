# -*- coding: utf-8 -*-
import numpy as np


def load_data(data_file_path):
    """Load features and labels of pages from text data file.
    @param data_file_path   file path of text data file
    @return Data of pages. Each page has fields as follow:
    'doc_name':     name of document, string.
    'page_num':     page number, integer.
    'y':            logical labels, integer.
    'cliques':      unary or pairwise cliques, dict.
    'features':     unary or pairwise features, dict.
    """
    dataset = _Dataset()
    with open(data_file_path) as f:
        dataset.nr_states = int(_read_after_equality_sign(f))
        
        for i in range(dataset.nr_states + 1):
            f.readline()

        while True:
            page_data = _load_page_data(f)
            if page_data is None:
                break
            dataset.data.append(page_data)
        # while f.readline():
        #     dataset.data.append(_load_page_data(f))

        # set global number of features and number of variables
        first_data = dataset.data[0]
        get_nr_variables = (lambda d, clique_type:
                            d.cliques[clique_type].shape[1])
        get_nr_features = (lambda d, clique_type:
                           d.features[clique_type].shape[1])
        for clique_type in first_data.cliques:
            dataset.nr_variables[clique_type] = _get_consisteny(
                get_nr_variables, dataset, clique_type)
            dataset.nr_features[clique_type] = _get_consisteny(
                get_nr_features, dataset, clique_type)
        dataset.clique_types = sorted(dataset.nr_variables.keys(),
                                      key=lambda t: dataset.nr_variables[t])
    return dataset

class _Dataset(object):
    def __init__(self):
        self.data = []
        self.nr_states = 0
        self.nr_variables = {}
        self.nr_features = {}

class _PageData(object):

    def __init__(self):   
        self.doc_name = []
        self.page_num = []
        self.y = []
        self.cliques = {}
        self.features = {}

def _read_after_equality_sign(f):
    line = f.readline().strip().split('=')
    return line[-1]

def _read_vector(f, type_func):
    line = f.readline().strip().split()
    return tuple(type_func(x) for x in line)

def _read_cliques_or_features(f, page_data):
    pos = f.tell()
    line = f.readline()
    if line == '\n':
        return False
    line = line.strip().split('=')
    f.seek(pos)
    if line[0] == 'clique_type':
        _read_cliques(f, page_data)
    else:
        _read_features(f, page_data)
    return True

def _read_features(f, page_data):
    feature_type = _read_after_equality_sign(f)
    nr_features = int(_read_after_equality_sign(f))
    nr_cliques = len(page_data.cliques[feature_type])
    if nr_cliques > 0:
        features = tuple(_read_vector(f, float) for i in range(nr_cliques))
        features = np.array(features)
        assert(features.shape[1] == nr_features)
    else:
        features = np.zeros((0, 0), np.float)
    page_data.features[feature_type] = features

def _read_cliques(f, page_data):
    clique_type = _read_after_equality_sign(f)
    nr_cliques = int(_read_after_equality_sign(f))
    if nr_cliques > 0:
        cliques = [sorted(_read_vector(f, int)) for i in range(nr_cliques)]
        cliques = np.array(cliques, np.int)
    else:
        cliques = np.zeros((0, 0), np.int)
    page_data.cliques[clique_type] = cliques

def _load_page_data(f):

    # read basic page info
    doc_name = _read_after_equality_sign(f)
    if not doc_name:
        return None

    page_data = _PageData()
    page_data.doc_name = doc_name
    page_data.page_num = int(_read_after_equality_sign(f))

    nr_nodes = int(_read_after_equality_sign(f))
    nodes = np.arange(nr_nodes).reshape((nr_nodes, 1))
    page_data.cliques['node'] = nodes

    # skip physical ids
    for i in range(nr_nodes):
        f.readline()

    # read ground-truth labels
    page_data.y = np.array(_read_vector(f, int))

    while _read_cliques_or_features(f, page_data):
        pass
    
    # # read edges
    # _read_cliques(f, page_data)
    # _read_cliques(f, page_data)
    # _read_cliques(f, page_data)
    # _read_cliques(f, page_data)
    # _read_cliques(f, page_data)
    # _read_cliques(f, page_data)

    # # read node features
    # _read_features(f, page_data)
    # _read_features(f, page_data)
    # _read_features(f, page_data)
    # _read_features(f, page_data)
    # _read_features(f, page_data)
    # _read_features(f, page_data)
    # _read_features(f, page_data)

    return page_data

def _get_consisteny(getter, dataset, clique_type):
    numbers = [getter(d, clique_type) for d in dataset.data]
    numbers = [n for n in numbers if n > 0]
    if not numbers:
        return 0
    assert(all(n == numbers[0] for n in numbers))
    return numbers[0]

def _print_page_data(page_data):
    print 'doc page: \n', page_data.doc_name, page_data.page_num
    print 'labels: \n', page_data.y
    print 'page cliques: \n', page_data.cliques
    print 'page features: \n', page_data.features

if __name__=='__main__':
    dataset = load_data('v3.data')
    print dataset.nr_states
    print dataset.nr_features
    print dataset.nr_variables
    # for page_data in dataset.data:
    #     _print_page_data(page_data)
