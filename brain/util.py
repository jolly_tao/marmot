# -*- coding: utf-8 -*-

# util: some helper functions

from sklearn.preprocessing import LabelEncoder
from marmot.common.label import *

label_encoder = LabelEncoder()
label_encoder.fit(list(LABEL_LOGICAL))

label_encoder_fine = label_encoder

label_encoder_coarse = LabelEncoder()
label_encoder_coarse.fit(list(LABEL_LOGICAL_COARSE))

fine_to_coarse = dict(zip(LABEL_LOGICAL, LABEL_LOGICAL))
fine_to_coarse[LABEL_FIGURE_CAPTION_CONT] = LABEL_FIGURE_CAPTION
fine_to_coarse[LABEL_LIST_ITEM_CONT] = LABEL_LIST_ITEM

def transform_fine(labels):
    return label_encoder_fine.transform(labels)

def transform_coarse(labels):
    fine_to_coarse = dict(zip(LABEL_LOGICAL, LABEL_LOGICAL))
    fine_to_coarse[LABEL_FIGURE_CAPTION_CONT] = LABEL_FIGURE_CAPTION
    fine_to_coarse[LABEL_LIST_ITEM_CONT] = LABEL_LIST_ITEM
    return label_encoder_coarse.transform(
        [fine_to_coarse[label] for label in labels])
