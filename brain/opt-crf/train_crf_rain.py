"""
Refer to http://www.di.ens.fr/~mschmidt/Software/UGM/trainCRF.html
section "Maximum Likelihood Training of Log-Linear Conditionals Random Fields"
rain.mat is the same data file used in the matlab example.
"""

import scipy.io as sio
import numpy as np
# import opengm
import itertools
import scipy.optimize
# from model_construction import make_template_cliques, make_weights

# from model_construction import make_graphical_model

# from crf_nll import crf_nll
from crf_pseudo_nll import crf_pseudo_nll, crf_pseudo_nll_update_grad

# load data
mat_contents = sio.loadmat('rain.mat')
x = mat_contents['X']
y = x.astype(np.int)
# y = x.astype(np.int32)
x = x.astype(np.float)
nr_instances, nr_nodes = y.shape
nr_states = 2

nodes = tuple((i,) for i in xrange(nr_nodes))
edges = tuple(itertools.imap(lambda x, y: x + y, nodes[:-1], nodes[1:]))
nr_edges = len(edges)

nr_node_features = 1
nr_edge_features = 1
node_features = np.ones((nr_instances, nr_nodes, nr_node_features),
                        dtype=np.float)
edge_features = np.ones((nr_instances, nr_edges, nr_edge_features),
                        dtype=np.float)

def make_clique_template_maps(nr_nodes, nr_edges,
                              nr_node_features, nr_edge_features,
                              nr_states):
    # np.array of float data type, bad idea
    # node_map = np.empty((nr_states, nr_node_features),
    #                     dtype=np.float)
    # node_map.fill(np.NaN)
    node_map = np.empty((nr_states, nr_node_features),
                        dtype=int)
    node_map.fill(-1)

    node_map[0, :] = 0

    # edge_map = np.empty((nr_states, nr_states, nr_edge_features),
    #                     dtype=np.float)
    # edge_map.fill(np.NaN)
    edge_map = np.empty((nr_states, nr_states, nr_edge_features),
                        dtype=int)
    edge_map.fill(-1)
    edge_map[0, 0, :] = 1
    edge_map[1, 0, :] = 2
    edge_map[0, 1, :] = 3

    return (node_map, edge_map)

def make_weights(weight_maps):
    flats = (weight_map.flat for weight_map in weight_maps)
    nr_weights = max(itertools.chain(*flats)) + 1
    return np.ones(nr_weights, dtype=np.float)


def crf_nll_batch(weights, y, nr_states, features, weight_maps):
    nr_instances = y.shape[0]
    nll_batch = 0
    grad_batch = np.zeros(weights.shape, dtype=np.float)

    node_map, edge_map = weight_maps

    # for i in range(100):    # subset of data
    for i in range(nr_instances):
        node_features, edge_features = features
        templates = ((nodes, node_features[i, :], node_map),
                     (edges, edge_features[i, :], edge_map))

        nll, grad = crf_pseudo_nll(weights, y[i, :], nr_states, templates)
        nll_batch += nll
        grad_batch += grad

    return nll_batch, grad_batch

def train_crf(x, y, node_features, edge_features):
    weight_maps = make_clique_template_maps(nr_nodes, nr_edges,
                                            nr_node_features,
                                            nr_edge_features,
                                            nr_states)
    features = (node_features, edge_features)
    weights = make_weights(weight_maps)
    res = scipy.optimize.minimize(crf_nll_batch, weights,
                                  method='L-BFGS-B',
                                  args=(y, nr_states, features, weight_maps),
                                  jac=True, options={'epsilon': 1e-05, 'disp': True})
    print res
    return res.x

if __name__ == '__main__':
    import cProfile
    cProfile.run("train_crf(x, y, node_features, edge_features)", "rain.prof")
    # weights = train_crf(x, y, node_features, edge_features)
    # print weights
    import pstats
    p = pstats.Stats("rain.prof")
    p.sort_stats("time").print_stats()

    # train_crf(x, y, node_features, edge_features)
