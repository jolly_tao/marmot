# -*- coding: utf-8 -*-
# cython: profile=True
import numpy as np
cimport numpy as np
from libc.math cimport exp
from itertools import product, chain

class Clique(object):
    def __init__(self, variables, nr_states):
        self.variables = tuple(map(int, variables))
        self._nr_states = nr_states
        # shape = (self._nr_states,) * len(self._variables)
        shape = (self._nr_states,) * len(self.variables)
        # self._potentials = np.zeros(shape, dtype=np.float)
        self.potentials = np.zeros(shape, dtype=np.float)

    # @property
    # def variables(self):
    #     return self._variables

    # @property
    # def features(self):
    #     return self._features

    # @features.setter
    # def features(self, features):
    #     self._features = features

    # @property
    # def weight_map(self):
    #     return self._weight_map

    # @weight_map.setter
    # def weight_map(self, weight_map):
    #     self._weight_map = weight_map

    def update_potentials(self, np.ndarray[double, ndim=1] weights, log_space=False):
        """Update potentials attached to this clique, using features, weights
        and weight map.
        """
        cdef int nr_states
        cdef np.ndarray[double, ndim=1] features
        cdef int nr_features
        cdef int s, s1, s2, f, wi
        cdef np.ndarray[double, ndim=1] unary_pot
        cdef np.ndarray[long, ndim=2] unary_weight_map
        cdef np.ndarray[double, ndim=2] binary_pot
        cdef np.ndarray[long, ndim=3] binary_weight_map
        cdef double pot

        nr_states = self._nr_states
        # features = self._features
        features = self.features
        # nr_features = len(self._features)
        nr_features = len(self.features)
        # unary
        # if len(self._variables) == 1:
        if len(self.variables) == 1:
            # unary_pot = self._potentials
            unary_pot = self.potentials
            # unary_weight_map = self._weight_map
            unary_weight_map = self.weight_map
            for s in range(nr_states):
                pot = 0
                for f in range(nr_features):
                    wi = unary_weight_map[s, f]
                    if wi >= 0:
                        pot += weights[wi] * features[f]
                if not log_space:
                    pot = exp(pot)
                unary_pot[s] = pot
            # self._potentials = unary_pot
            # self.potentials = unary_pot
        # binary
        else:
            # binary_pot = self._potentials
            binary_pot = self.potentials
            # binary_weight_map = self._weight_map
            binary_weight_map = self.weight_map
            for s1 in range(nr_states):
                for s2 in range(nr_states):
                    pot = 0
                    for f in range(nr_features):
                        wi = binary_weight_map[s1, s2, f]
                        if wi >= 0:
                            pot += weights[wi] * features[f]
                    if not log_space:
                        pot = exp(pot)
                    binary_pot[s1, s2] = pot
            # self._potentials = binary_pot
            # self.potentials = binary_pot

        # if not log_space:
        #     self._potentials = np.exp(self._potentials)

    # @property
    # def potentials(self):
    #     return self._potentials

class CliqueIndexer(object):
    def __init__(self):
        self._index = dict()

    def add_clique(self, clique):
        variables = clique.variables
        order = len(variables)
        for variable in variables:
            dct = self._index.setdefault(variable, {})
            lst = dct.setdefault(order, [])
            lst.append(clique)

    def nr_variables(self):
        return len(self._index)

    def by_variable(self, variable):
        try:
            dct = self._index[variable]
            return list(chain(*dct.itervalues()))
        except KeyError:
            return []

    def by_variable_order(self, variable, order):
        try:
            return self._index[variable][order]
        except KeyError:
            return []
