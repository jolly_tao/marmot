# -*- coding: utf-8 -*-

import pickle
import random

def dump(file_path, obj):
    with open(file_path, 'w') as f:
        pickle.dump(obj, f)

def load(file_path):
    with open(file_path) as f:
        obj = pickle.load(f)
        return obj

def preprocess_node(dataset, svm_clf):
    for page_data in dataset.data:
        node_features = page_data.features['node']        
        svm_log_proba = svm_clf.predict_log_proba(node_features)
        page_data.features['node'] = svm_log_proba
    dataset.nr_features['node'] = dataset.nr_states

def dump_all_yhat(file_path, yhat):
    with open(file_path, 'w') as f:
        for y in yhat:
            f.write('%d\n' % y)
            
        
def dump_indices(train_file, test_file, total_pages):
    indices = range(total_pages)
    random.shuffle(indices)
    train_proportion = 0.667
    nr_train_pages = int(train_proportion * total_pages)
    train_pages = indices[0 : nr_train_pages]
    test_pages = indices[nr_train_pages::]
    print train_pages
    print test_pages
    with open(train_file, 'w') as f:
        pickle.dump(train_pages, f)
    with open(test_file, 'w') as tf:
        pickle.dump(test_pages, tf)
