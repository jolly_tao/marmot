# -*- coding: utf-8 -*-
    
import numpy as np
import scipy
# import opengm
# from model_construction import make_graphical_model
from clique import Clique, CliqueIndexer

def crf_pseudo_nll(weights, y, nr_states, templates):
    """Compute pseudo likelihood and gradient of the model.
    @param weights      model parameters.
    @param y            labels, concrete assignment to random variables.
    @param nr_states    number of states a variable can attain.
    @param template_cliques groups of cliques belonging to different
                            templates, e.g.
                            ((node_clique_1, ..., node_clique_n),
                             (edge_clique_1, ..., edge_clique_m),
                             (cliques of template a),
                             ...
                             )
    @return (negtive_log_pseudo_likelihood, gradient_of_weights)
    """
    nll = 0
    grad = np.zeros_like(weights, dtype=np.float)
    
    clique_indexer = make_cliques(weights, nr_states, templates)
    nr_nodes = clique_indexer.nr_variables()

    for n in range(nr_nodes):
        pot = np.ones(nr_states, dtype=np.float)
        # unary_cliques = clique_indexer.by_variable_order(n, 1)
        # for clique in unary_cliques:
        cliques = clique_indexer.by_variable(n)
        for clique in cliques:
            variables = clique.variables
            slicing = [y[v] for v in variables]
            index = variables.index(n)
            slicing[index] = slice(nr_states)
            sub_pot = clique.potentials[tuple(slicing)]
            np.multiply(pot, sub_pot, pot)

        nll += -np.log(pot[y[n]]) + np.log(pot.sum())
        # print nll
        # exit(0)

        node_bel = pot / pot.sum()

        # crf_pseudo_nll_update_grad(grad, y, nr_states,
        #                            node_bel, n, var2cliques[n])
        crf_pseudo_nll_update_grad(grad, y, nr_states,
                                   node_bel, n, cliques)
        # print grad
        # exit(0)

    # print nll
    # print grad
    # exit(0)
    return (nll, grad)

def crf_pseudo_nll_update_grad(grad, y, nr_states, node_bel, n, cliques):
    """
    @param grad         gradient to be updated, vector, np.array
    @param y            ground-truth, vector, np.array
    @param nr_states    number of states, scalar, int
    @param node_bel     node belief, vector, np.array
    @param n            random variable id, scalar, int
    @param cliques      cliques containing random variable n, list
    """
    for c in cliques:
        variables = c.variables
        weight_map = c.weight_map
        features = c.features
        obs_s = y[n]
        
        # clique_states = y[np.array(variables)]
        clique_states = [y[v] for v in variables]
        n_index = variables.index(n)
        
        # keep assignment of neighbors fixed, change state of n
        w_indices = weight_map[tuple(clique_states)]
        flags = (w_indices >= 0)
        grad[w_indices[flags]] -= features[flags]
        for s in xrange(nr_states):
            bel = node_bel[s]
            clique_states[n_index] = s
            w_indices = weight_map[tuple(clique_states)]
            # obs = int(s == obs_s)
            flags = (w_indices >= 0)
            # grad[w_indices[flags]] += features[flags] * (bel - obs)
            grad[w_indices[flags]] += features[flags] * bel

def make_template_cliques(weights, template, nr_states, clique_indexer):
    """Make cliques belonging to a template.
    @param template     variables, features and weight map.
    @param nr_states    number of states a variable can attain.
    @param clique_indexer  {variable: {order: [cliques containing variable]}}
    """
    variables, features, weight_map = template
    for i, v in enumerate(variables):
        clique = Clique(v, nr_states)
        clique.features = features[i, :]
        clique.weight_map = weight_map
        clique.update_potentials(weights)
        clique_indexer.add_clique(clique)

def make_cliques(weights, nr_states, templates):
    """Make cliques from templates.
    @param weights      model parameters
    @param nr_states    number of states
    @param templates    [(variables, features, weight_map)]
    @return An indexer that indexes cliques
    """
    clique_indexer = CliqueIndexer()
    for template in templates:
        make_template_cliques(weights, template, nr_states, clique_indexer)
    return clique_indexer
