# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import sys
    import os
    path = os.path.abspath(__file__)
    for i in range(4):
        path = os.path.dirname(path)
    sys.path.insert(0, path)
    from marmot.brain.io import load_data
    from marmot.brain.svm import train_svm
    # from marmot.brain.crf import crf_nll
    # from marmot.brain.crf import crf_pseudo_nll

from crf_pseudo_nll import crf_pseudo_nll
import pickle
import numpy as np
import scipy
from model_construction import make_weights
from model_construction import make_weight_map
from helper import load, dump, preprocess_node

def train_crf(data, svm_clf, nr_states, weight_maps):
    weights = make_weights(weight_maps.itervalues())
    obj_fun = lambda weights: _obj_func_batch(weights, crf_pseudo_nll,
                                              data, svm_clf, nr_states, weight_maps)
    penalized_obj_fun = lambda weights: penalized_l2(weights, obj_fun,
                                                     np.ones_like(weights, dtype=float))
    # res = scipy.optimize.minimize(_obj_func_batch, weights,
    #                               method='L-BFGS-B',
    #                               args=(crf_pseudo_nll, data, svm_clf, nr_states, weight_maps),
    #                               jac=True, options={'disp': True, 'maxiter': 500})
    res = scipy.optimize.minimize(penalized_obj_fun, weights,
                                  method='L-BFGS-B',
                                  args=(),
                                  jac=True, options={'disp': True, 'maxiter': 500})

    return res.x

def _obj_fun_instance(weights, func, page_data, svm_clf, nr_states, weight_maps):
    templates = []
    for clique_type in weight_maps:
        templates.append((page_data.cliques[clique_type],
                          page_data.features[clique_type],
                          weight_maps[clique_type]))
        
    nll, grad = func(weights, page_data.y, nr_states, templates)
    return nll, grad

def _obj_func_batch(weights, func, data, svm_clf, nr_states, weight_maps):
    nll_batch = 0
    grad_batch = np.zeros_like(weights, dtype=np.float)

    for page_data in data:
        nll, grad = _obj_fun_instance(weights, func, page_data,
                                      svm_clf, nr_states, weight_maps)
        nll_batch += nll
        grad_batch += grad

    return nll_batch, grad_batch

def penalized_l2(weights, obj_fun, lambdas):
    nll, grad = obj_fun(weights)
    nll += np.inner(lambdas, np.square(weights))
    grad += 2 * lambdas * weights
    return (nll, grad)

def make_weight_map_from_dataset(dataset):
    weight_maps = dict()
    base = 0
    for clique_type in dataset.clique_types:
        weight_map, base = make_weight_map(dataset.nr_variables[clique_type],
                                           dataset.nr_features[clique_type],
                                           dataset.nr_states, base)
        weight_maps[clique_type] = weight_map
    return weight_maps

if __name__ == '__main__':
    import cProfile
    import pstats

    indices = load('train_indices.pickle')
    
    dataset = load_data('../io/v3.data')
    # dataset = load_data('../io/v3_coarse.data')
    dataset.clique_types = ['node',
                            'mst',
                            # 'text_align',
                            # 'text_non_align',
                            # 'non_text_contain_text',
                            # 'non_text_above_text'
                            ]
    # dataset.clique_types = ['node', 'figcap', 'figanno', 'align']
    data = [dataset.data[i] for i in indices]
    
    svm_clf = train_svm(data)
    dump('svm_clf.pickle', svm_clf)
    
    preprocess_node(dataset, svm_clf)


    weight_maps = make_weight_map_from_dataset(dataset)
    # weights = train_crf(dataset.data, svm_clf, dataset.nr_states, weight_maps)
    weights = train_crf(data, svm_clf, dataset.nr_states, weight_maps)

    dump('weights.pickle', weights)
 
    # cProfile.run("train_crf(dataset.data, svm_clf, dataset.nr_states, weight_maps)", "real.prof")

