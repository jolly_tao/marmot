from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("crf_pseudo_nll", ["crf_pseudo_nll.pyx"]),
               Extension("clique", ["clique.pyx"])]

setup(
    name = 'extension',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)
