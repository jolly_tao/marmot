
import sys
import os
path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from marmot.brain.io import load_data
from marmot.brain.svm import train_svm
# from marmot.brain.crf import crf_nll
# from marmot.brain.crf import crf_pseudo_nll
from crf_pseudo_nll import crf_pseudo_nll
from train_crf import train_crf, make_weight_map_from_dataset
from test_crf import test_crf
import opengm
import cProfile
import pstats
import pickle
import numpy as np
import sklearn.metrics as metrics
from model_construction import make_weights
from model_construction import make_weight_map
from helper import load, dump, preprocess_node, dump_all_yhat

train_indices = load('train_indices.pickle')
test_indices = load('test_indices.pickle')

dataset = load_data('../io/v3.data')

configs = {'run1' : ['node',
                     'mst'],
           'run2' : ['node',
                     'mst',
                     'text_align'],
           'run3' : ['node',
                     'mst',
                     'text_align',
                     'text_non_align'],
           'run4' : ['node',
                     'mst',
                     'text_align',
                     'text_non_align',
                     'non_text_contain_text'],
           'run5' : ['node',
                     'mst',
                     'text_align',
                     'text_non_align',
                     'non_text_contain_text',
                     'non_text_above_text']
           }

train_data = [dataset.data[i] for i in train_indices]
test_data = [dataset.data[i] for i in test_indices] 
y_true = np.hstack([page_data.y for page_data in test_data])
nr_states = dataset.nr_states


svm_clf = train_svm(train_data)
preprocess_node(dataset, svm_clf)

dump_all_yhat('y_true.output', y_true)

for run, config in configs.iteritems():
    dataset.clique_types = config

    weight_maps = make_weight_map_from_dataset(dataset)
    weights = train_crf(train_data, svm_clf, dataset.nr_states, weight_maps)
  
    y_pred = test_crf(test_data, svm_clf, nr_states, weight_maps, weights)
    micro_average_precision = metrics.precision_score(y_true, y_pred,
                                                      labels=None, pos_label=1,
                                                      average='micro')
    print micro_average_precision

    dump_all_yhat(run + '_y_crf.output', y_pred)
   
