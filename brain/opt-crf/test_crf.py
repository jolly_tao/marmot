# -*- coding: utf-8 -*-

import opengm
import numpy as np
import sklearn.metrics as metrics
from model_construction import make_cliques
from model_construction import make_graphical_model
from model_construction import make_weight_map
from helper import load, preprocess_node, dump_all_yhat

if __name__ == '__main__':
    import sys
    import os
    import pickle
    path = os.path.abspath(__file__)
    for i in range(4):
        path = os.path.dirname(path)
    sys.path.insert(0, path)
    from marmot.brain.io import load_data
    from marmot.brain.svm import train_svm
    from train_crf import make_weight_map_from_dataset

def test_crf(data, svm_clf, nr_states, weight_maps, weights):
    y_hat = [_test_instance(page_data, svm_clf, nr_states, weight_maps, weights)
             for page_data in data]
    return np.hstack(y_hat)

def _test_instance(page_data, svm_clf, nr_states, weight_maps, weights):
    templates = []
    for clique_type in weight_maps:
        templates.append((page_data.cliques[clique_type],
                          page_data.features[clique_type],
                          weight_maps[clique_type]))

    nodes = page_data.cliques['node']
    cliques = make_cliques(templates, nr_states, weights, log_space=True)

    gm = opengm.gm(np.ones(len(nodes), dtype=opengm.index_type) * nr_states,
                   operator='adder')
    for clique in cliques:
        fid = gm.addFunction(clique.potentials)
        # gm.addFactor(fid, clique.variables)
        gm.addFactor(fid, tuple(map(int, clique.variables)))

    inf = opengm.inference.BeliefPropagation(gm, 'maximizer')
    inf.infer()
    argmin = inf.arg()
    return argmin

if __name__ == '__main__':
    # dataset = load_data('../io/train.data')

    indices = load('test_indices.pickle')
    
    dataset = load_data('../io/v3.data')
    # dataset = load_data('../io/v3_coarse.data')
    # dataset.clique_types = ['node', 'mst', 'figcap', 'figanno', 'align', 'non_align']
    dataset.clique_types = ['node',
                            'mst',
                            # 'text_align',
                            # 'text_non_align',
                            # 'non_text_contain_text',
                            # 'non_text_above_text'
                            ]
    # dataset.clique_types = ['node', 'figcap', 'figanno', 'align']
    data = [dataset.data[i] for i in indices]
    
    svm_clf = load('svm_clf.pickle')
    preprocess_node(dataset, svm_clf)

    y_true = np.hstack([page_data.y for page_data in data])
    nr_states = dataset.nr_states

    weight_maps = make_weight_map_from_dataset(dataset)
    weights = load('weights.pickle')

    y_pred = test_crf(data, svm_clf, nr_states, weight_maps, weights)
    micro_average_precision = metrics.precision_score(y_true, y_pred,
                                                      labels=None, pos_label=1,
                                                      average='micro')
    print micro_average_precision

    dump_all_yhat('y_crf.output', y_pred)
    dump_all_yhat('y_true.output', y_true)
    
