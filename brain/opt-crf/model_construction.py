# -*- coding: utf-8 -*-

import numpy as np
from itertools import product
from itertools import repeat
from itertools import chain
import opengm
from clique import Clique

def make_template_cliques(template, nr_states, weights, log_space=False):
    """Make cliques belonging to a template.
    @param template     variables, features and weight map.
    @param nr_states    number of states a variable can attain.
    @return cliques belonging to template.
    """
    variables, features, weight_map = template
    cliques = []
    for i, v in enumerate(variables):
        clique = Clique(v, nr_states)
        clique.features = features[i, :]
        clique.weight_map = weight_map
        clique.update_potentials(weights, log_space)
        cliques.append(clique)
    return cliques

def make_cliques(templates, nr_states, weights, log_space=False):
    cliques = []
    for template in templates:
        cliques.extend(make_template_cliques(template, nr_states, weights, log_space))
    return cliques

def make_clique_indexer(cliques):
    clique_indexer = CliqueIndexer()
    for clique in cliques:
        clique_indexer.add_clique(clique)
    return clique_indexer

def make_graphical_model(cliques, nr_variables, nr_states):
    gm = opengm.gm(np.ones(nr_nodes, dtype=opengm.index_type) * nr_states,
                   operator='multiplier')
    for clique in cliques:
        fid = gm.addFunction(clique.potentials)
        gm.addFactor(fid, clique.variables)
    return gm

# def _add_factor(weights, y, nr_states, gm, cliques):
#     factor_ids = []
#     for c in cliques:
#         shape = (nr_states,) * len(c.variables)
#         pot = np.zeros(shape, dtype=np.float)
#         state_space = product(range(nr_states),
#                               repeat=len(c.variables))
#         for s in state_space:
#             wi = c.weight_map[s]
#             flags = (wi >= 0)
#             # w = np.array([0 if np.isnan(i) else weights[i] for i in wi])
#             # f = c.features
#             w = weights[wi[flags]]
#             f = c.features[flags]
#             pot[s] = np.inner(w, f)
#         pot = np.exp(pot)
#         fid = gm.addFunction(pot)
#         factor_ids.append(gm.addFactor(fid, c.variables))
#     return factor_ids

# def make_graphical_model(weights, y, nr_states, templates):
#     """
#     @param weights      model parameters.
#     @param y            labels, concrete assignment to random variables.
#     @param nr_states    number of states a variable can attain.
#     @template_cliques   groups of cliques belonging to different templates,
#                         e.g. ((node_clique_1, ..., node_clique_n),
#                               (edge_clique_1, ..., edge_clique_m),
#                               (cliques of template a),
#                               ...
#                               )
#     @return (graphical_model, groups_of_clique_factor_ids)
#     The group structure of clique factor ids are the same as template cliques.
             
#     """
#     # construct template cliques from clique templates
#     var2cliques = {}
#     template_cliques = []
#     for template in templates:
#         cliques = make_template_cliques(template, nr_states, var2cliques)
#         template_cliques.append(cliques)

#     # calculate total variable numbers
#     variables = set()
#     for cliques in template_cliques:
#         for clique in cliques:
#             variables.update(clique.variables)
#     nr_nodes = len(variables)

#     # construct graphical model
#     gm = opengm.gm(np.ones(nr_nodes, dtype=opengm.index_type) * nr_states,
#                    operator='multiplier')

#     # make factors from cliques and add them to graphical model
#     template_clique_fis = [_add_factor(weights, y, nr_states, gm, cliques)
#                            for cliques in template_cliques]

#     return (gm, template_cliques, template_clique_fis, var2cliques)

def make_weight_map(nr_variables, nr_features, nr_states, base):
    """Construct problem specific parameter maps.
    """
    shape = list(repeat(nr_states, nr_variables)) + [nr_features]
    weight_map = np.empty(shape, dtype=int)
    space = product(*(range(x) for x in shape))
    for map_index in space:
        weight_map[map_index] = base
        base += 1

    return weight_map, base

def make_weights(weight_maps):
    flats = (weight_map.flat for weight_map in weight_maps)
    nr_weights = max(chain(*flats)) + 1
    return np.ones(nr_weights, dtype=np.float)
