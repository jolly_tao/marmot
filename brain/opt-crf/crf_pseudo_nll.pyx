# -*- coding: utf-8 -*-
# cython: profile=True

import numpy as np
cimport numpy as np
from libc.math cimport log
import scipy
import itertools
# import opengm
# from model_construction import make_graphical_model
from clique import Clique, CliqueIndexer

print 'using extension'

def crf_pseudo_nll(np.ndarray weights, np.ndarray[long, ndim=1] y, int nr_states, templates):
# def crf_pseudo_nll(weights, y, nr_states, templates):
    """Compute pseudo likelihood and gradient of the model.
    @param weights      model parameters.
    @param y            labels, concrete assignment to random variables.
    @param nr_states    number of states a variable can attain.
    @param template_cliques groups of cliques belonging to different
                            templates, e.g.
                            ((node_clique_1, ..., node_clique_n),
                             (edge_clique_1, ..., edge_clique_m),
                             (cliques of template a),
                             ...
                             )
    @return (negtive_log_pseudo_likelihood, gradient_of_weights)
    """
    cdef double nll = 0
    cdef np.ndarray[double, ndim=1] grad = np.zeros_like(weights, dtype=np.float)
    cdef int nr_nodes
    cdef int n, i
    cdef int s, s1, s2, index, v1, v2
    cdef np.ndarray[double, ndim=1] pot = np.empty(nr_states,
                                                   dtype=np.float)
    cdef np.ndarray[double, ndim=1] unary_pot
    cdef np.ndarray[double, ndim=2] binary_pot
    cdef double pot_sum
    cdef np.ndarray[double, ndim=1] node_bel = np.empty(nr_states,
                                                        dtype=np.float)
    
    clique_indexer = make_cliques(weights, nr_states, templates)
    nr_nodes = clique_indexer.nr_variables()

    for n in range(nr_nodes):
        # pot = np.ones(nr_states, dtype=np.float)
        for s in range(nr_states):
            pot[s] = 1
        unary_cliques = clique_indexer.by_variable_order(n, 1)
        for clique in unary_cliques:
            unary_pot = clique.potentials
            for s in range(nr_states):
                pot[s] *= unary_pot[s]
                
        binary_cliques = clique_indexer.by_variable_order(n, 2)
        for clique in binary_cliques:
            binary_pot = clique.potentials
            v1 = clique.variables[0]
            v2 = clique.variables[1]
            # v1, v2 = clique.variables
            index = 0 if (v1 == n) else 1
            s1 = y[v1]
            s2 = y[v2]
            for s in range(nr_states):
                if index == 0:
                    s1 = s
                else:
                    s2 = s
                pot[s] *= binary_pot[s1, s2]
                
        pot_sum = 0
        for s in range(nr_states):
            pot_sum += pot[s]

        nll += -log(pot[y[n]]) + log(pot_sum)
        # nll += -np.log(pot[y[n]]) + np.log(pot_sum)
        # nll += -np.log(pot[y[n]]) + np.log(pot.sum())
        # print nll
        # exit(0)

        # node_bel = pot / pot.sum()
        for s in range(nr_states):
            node_bel[s] = pot[s] / pot_sum
        # node_bel = pot / pot_sum

        # crf_pseudo_nll_update_grad(grad, y, nr_states,
        #                            node_bel, n, var2cliques[n])
        cliques = itertools.chain(unary_cliques, binary_cliques)
        crf_pseudo_nll_update_grad(grad, y, nr_states,
                                   node_bel, n, cliques)
        # print grad
        # exit(0)

    # print nll
    # print grad
    # exit(0)
    return (nll, grad)

def crf_pseudo_nll_update_grad(np.ndarray[double, ndim=1] grad, np.ndarray[long, ndim=1] y, int nr_states,
                               np.ndarray[double, ndim=1] node_bel, int n, cliques):
# def crf_pseudo_nll_update_grad(grad, y, nr_states, node_bel, n, cliques):
    """
    @param grad         gradient to be updated, vector, np.array
    @param y            ground-truth, vector, np.array
    @param nr_states    number of states, scalar, int
    @param node_bel     node belief, vector, np.array
    @param n            random variable id, scalar, int
    @param cliques      cliques containing random variable n, list
    """
    cdef int obs_s = y[n]
    cdef int obs
    cdef double bel
    cdef int s, s1, s2, f, v1, v2
    cdef int i, wi
    cdef int nr_features
    cdef np.ndarray[double, ndim=1] features
    cdef np.ndarray[long, ndim=2] unary_weight_map
    cdef np.ndarray[long, ndim=3] binary_weight_map

    for c in cliques:
        variables = c.variables
        features = c.features
        nr_features = len(features)


        # unary
        if len(variables) == 1:
            unary_weight_map = c.weight_map
            for s in range(nr_states):
                bel = node_bel[s]
                obs = int(s == obs_s)
                for f in range(nr_features):
                    wi = unary_weight_map[s, f]
                    if wi >= 0:
                        grad[wi] += features[f] * (bel - obs)
        # binary
        else:
            binary_weight_map = c.weight_map
            v1 = variables[0]
            v2 = variables[1]
            s1 = y[v1]
            s2 = y[v2]
            for s in range(nr_states):
                bel = node_bel[s]
                obs = int(s == obs_s)
                if v1 == n:
                    s1 = s
                else:
                    s2 = s
                for f in range(nr_features):
                    wi = binary_weight_map[s1, s2, f]
                    if wi >= 0:
                        grad[wi] += features[f] * (bel - obs)

def make_template_cliques(weights, template, nr_states, clique_indexer):
    """Make cliques belongng to a template.
    @param template     variables, features and weight map.
    @param nr_states    number of states a variable can attain.
    @param clique_indexer  {variable: {order: [cliques containing variable]}}
    """
    cdef i, nr_cliques
    
    clique_variables, clique_features, weight_map = template
    nr_cliques = len(clique_variables)
    for i in range(nr_cliques):
    # for i, v in enumerate(variables):
        v = clique_variables[i]
        clique = Clique(v, nr_states)
        clique.features = clique_features[i, :]
        clique.weight_map = weight_map
        clique.update_potentials(weights)
        clique_indexer.add_clique(clique)

def make_cliques(weights, nr_states, templates):
    """Make cliques from templates.
    @param weights      model parameters
    @param nr_states    number of states
    @param templates    [(variables, features, weight_map)]
    @return An indexer that indexes cliques
    """
    clique_indexer = CliqueIndexer()
    for template in templates:
        make_template_cliques(weights, template, nr_states, clique_indexer)
    return clique_indexer
