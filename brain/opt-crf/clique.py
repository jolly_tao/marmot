# -*- coding: utf-8 -*-
# cython: profile=True

import numpy as np
from itertools import product, chain

class Clique(object):
    def __init__(self, variables, nr_states):
        # self._variables = np.array(variables, dtype=np.int)
        self._variables = tuple(variables)
        self._nr_states = nr_states
        shape = (self._nr_states,) * len(self._variables)
        self._potentials = np.zeros(shape, dtype=np.float)

    @property
    def variables(self):
        return self._variables

    @property
    def features(self):
        return self._features

    @features.setter
    def features(self, features):
        self._features = features

    @property
    def weight_map(self):
        return self._weight_map

    @weight_map.setter
    def weight_map(self, weight_map):
        self._weight_map = weight_map

    def update_potentials(self, weights, log_space=False):
        """Update potentials attached to this clique, using features, weights
        and weight map.
        """
        state_space = product(range(self._nr_states),
                              repeat=len(self._variables))
        for s in state_space:
            wi = self._weight_map[s]
            flags = (wi >= 0)
            w = weights[wi[flags]]
            f = self._features[flags]
            self._potentials[s] = np.inner(w, f)
        if not log_space:
            self._potentials = np.exp(self._potentials)

    @property
    def potentials(self):
        return self._potentials

class CliqueIndexer(object):
    def __init__(self):
        self._index = dict()

    def add_clique(self, clique):
        variables = clique.variables
        order = len(variables)
        for variable in variables:
            dct = self._index.setdefault(variable, {})
            lst = dct.setdefault(order, [])
            lst.append(clique)

    def nr_variables(self):
        return len(self._index)

    def by_variable(self, variable):
        try:
            dct = self._index[variable]
            return list(chain(*dct.itervalues()))
        except KeyError:
            return []

    def by_variable_order(self, variable, order):
        try:
            return self._index[variable][order]
        except KeyError:
            return []
