# svm_predict: script using svm to predict new sample with learnt
# model.

import sys
import os

path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

import argparse
import pickle
import pprint
from sklearn import metrics

from marmot.brain.util import label_encoder
from svm_load_data import svm_load_data


# init arg parser
parser = argparse.ArgumentParser(
    description='predict a testing set with learnt model.')
parser.add_argument('testing_set',
                    help='a text file indicating testing set.')
parser.add_argument('model_file',
                    help='model learnt from the training set.')

args = parser.parse_args()

# load model
with open(args.model_file) as f:
    classifier = pickle.load(f)
    # scaler = pickle.load(f)

# load data
print 'loading data ...'
try:
    X, Y = svm_load_data(args.testing_set)
    print '%d fragments are loaded.' % len(Y)
except IOError:
    print 'path of training set files is not well-formed.'
    exit(1)

# scaling
# print 'scaling ...'
# X = scaler.transform(X)

print 'predicting ...'
prediction = classifier.predict(X)
prediction = list(prediction.astype(int))

print metrics.classification_report(Y, prediction,
                                    target_names=label_encoder.classes_)
