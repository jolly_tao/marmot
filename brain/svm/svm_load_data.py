# -*- coding: utf-8 -*-

from marmot.brain.batch import batch_template
from marmot.brain.util import label_encoder

from marmot.brain.feature import extract_all_local
from marmot.brain.feature import Context

def svm_load_data(dataset):
    def extract_xy(physical_doc, page_num):
        page = physical_doc[page_num]
        context = Context(page)
        fragments = context.fragments

        Y = label_encoder.transform([fragment.logical_label
                                     for fragment in fragments])
        Y = list(Y.astype(int))
        X = extract_all_local(context)
        return (X, Y)
    return batch_template(dataset, extract_xy, ([], []), _combine_xy)

def _combine_xy(old_xy, delta_xy):
    old_x, old_y = old_xy
    delta_x, delta_y = delta_xy
    return (old_x + delta_x, old_y + delta_y)
