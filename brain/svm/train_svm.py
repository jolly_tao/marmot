# -*- coding: utf-8 -*-

import numpy as np
import pickle
from sklearn.svm import SVC

def train_svm(data, file_path=None):
    y = np.hstack([d.y for d in data])
    x = np.vstack([d.features['node'] for d in data])
    classifier = SVC(probability=True)
    classifier.fit(x, y)
    if file_path:
        with open(file_path, 'w') as f:
            pickle.dump(classifier, f)
    return classifier
