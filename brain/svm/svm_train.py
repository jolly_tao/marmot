# svm_train: script using svm to learn local distribution from
# training set.

import sys
import os

path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

import argparse
import pickle
import numpy as np
from sklearn.svm import SVC
from sklearn.preprocessing import Scaler
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV

from marmot.brain.util import label_encoder
from svm_load_data import svm_load_data

# init arg parser
parser = argparse.ArgumentParser(
    description='Fit a model from a training set.')
parser.add_argument('training_set',
                    help='a text file indicating training set.')
parser.add_argument('model_file',
                    help='model learnt from the training set.')

args = parser.parse_args()

# label_encoder = create_label_encoder()

# load data
print 'loading data ...'
try:
    X, Y = svm_load_data(args.training_set)
    print '%d fragments are loaded.' % len(Y)
except IOError:
    print 'path of training set files is not well-formed.'
    exit(1)

# scaling
print 'scaling ...'
scaler = Scaler().fit(X)
X = scaler.transform(X)

# # grid search
# print 'grid searching ...'
# C_range = 10.0 ** np.arange(-2, 9)
# gamma_range = 10.0 ** np.arange(-5, 4)
# param_grid = dict(gamma=gamma_range, C=C_range)

# grid = GridSearchCV(SVC(probability=True), param_grid=param_grid,
#                     cv=StratifiedKFold(y=Y, k=3))
# grid.fit(X, Y)
# classifier = grid.best_estimator_

print 'training ...'
classifier = SVC(probability=True)
classifier.fit(X, Y)

# serialization
print 'writing model to %s' % args.model_file
with open(args.model_file, 'w') as f:
    pickle.dump(classifier, f)
    pickle.dump(scaler, f)
