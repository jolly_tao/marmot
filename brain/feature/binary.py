# -*- coding: utf-8 -*-

from lazy import lazy
from marmot.common.label import LABEL_CHAR
from marmot.common.label import LABEL_IMAGE
from marmot.common.label import LABEL_PATH
from marmot.brain.feature import text_pattern

def child_label_check(label_lhs, label_rhs):
    def _child_label_check(method):
        def __child_label_check(self):
            if (label_lhs != self._lhs.child_label or
                label_rhs != self._rhs.child_label):
                return 0
            return method(self)
        return __child_label_check
    return _child_label_check

class BinaryFeature(object):
    """Binary of two fragments.
    """
    
    def __init__(self, lhs, rhs, context):
        """
        @param lhs one of the unary features.
        @param rhs the other of the unary features.
        @param context global contextual info
        """
        self._lhs = lhs
        self._rhs = rhs
        self._context = context

    def value(self):
        return [self.is_figure_and_caption,
                self.is_above_figure_caption,
                self.is_above_ends_with_sentence_termial,
                self.is_above_not_ends_with_sentence_termial,
                self.is_above_list_start,
                # self.is_below_list_start,
                # self.is_same_font_id,
                self.eq_font_size,
                self.lt_font_size,
                self.gt_font_size,
                # self.is_above_longer,
                # self.is_below_longer
                self.is_left_align,
                # self.is_center_align,
                # self.is_right_align,
                self.is_below_indent_deeper,
                ]

    @lazy
    @child_label_check(LABEL_IMAGE, LABEL_CHAR)
    def is_figure_and_caption(self):
        return self._rhs.starts_with_figure_caption

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_above_figure_caption(self):
        return self._lhs.starts_with_figure_caption

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_above_ends_with_sentence_termial(self):
        return float(bool(text_pattern.sentence_terminal.search(
            self._lhs.text)))

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_above_not_ends_with_sentence_termial(self):
        return float(not self.is_above_ends_with_sentence_termial)
    
    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_above_list_start(self):
        return self._lhs.starts_with_bullet
                
    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_below_list_start(self):
        return self._rhs.starts_with_bullet
    
    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_same_font_id(self):
        return float(self._lhs.font_id == self._rhs.font_id)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def eq_font_size(self):
        return float(abs(self._lhs.font_size - self._rhs.font_size)
                     < 0.01)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def lt_font_size(self):
        return float(self._lhs.font_size - self._rhs.font_size < -0.01)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def gt_font_size(self):
        return float(self._lhs.font_size - self._rhs.font_size > 0.01)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_above_longer(self):
        return ((self._lhs.box.x1 - self._rhs.box.x1)
                > 3 * self._context.dominant_font_size)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_below_longer(self):
        return ((self._rhs.box.x1 - self._lhs.box.x1)
                > 3 * self._context.dominant_font_size)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_align_without_bullet(self):
        return (abs(self._lhs.x_without_bullet -
                    self._rhs.x_without_bullet) <
                0.5 * self._context.dominant_font_size)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_same_indent(self):
        return float(self._lhs.indent_level ==
                     self._rhs.indent_level)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_above_indent_deeper(self):
        return float(self._lhs.box.x - self._rhs.box.x >
                     self._context.dominant_font_size)
    
    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_below_indent_deeper(self):
        return float(self._rhs.box.x - self._lhs.box.x >
                     self._context.dominant_font_size)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_left_align(self):
        return (abs(self._lhs.box.x0 - self._rhs.box.x0) <
                0.5 * self._context.dominant_font_size)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_center_align(self):
        return (abs((self._lhs.box.x0 + self._lhs.box.x1) -
                    (self._rhs.box.x0 + self._rhs.box.x1)) <
                1.0 * self._context.dominant_font_size)

    @lazy
    @child_label_check(LABEL_CHAR, LABEL_CHAR)
    def is_right_align(self):
        return (abs(self._lhs.box.x1 - self._rhs.box.x1) <
                0.5 * self._context.dominant_font_size)
