# -*- coding: utf-8 -*-

import numpy as np
from heapq import heappush, heappop
from marmot.common.rect import Rect

def find_whitespace(bound, rectangles, quality, pruner):
    """Find whitespaces that do not overlap any of rectangles inside
    bound.

    @param bound problem boundary
    @param rectangles obstacles
    @param quality priority of problem, larger value implies higher priority
    @param pruner predicator function, if pruner(bound) is True,
           problem will not be subdivided.

    """
    whitespaces = []
    queue = []
    # heapq module uses minimum heap, so nagative quality is used
    heappush(queue, (-quality(bound), bound, rectangles))
    while queue:
        (q, r, obstacles) = heappop(queue)
        
        # existing whitespace results are also treated as obstacles
        obstacles.extend(w for w in whitespaces
                         if not w in obstacles and w.overlap(r))

        # empty obstacles implies a whitespace
        if not obstacles:
            whitespaces.append(r)
            continue
        pivot = min(obstacles,
                    key=lambda x: np.linalg.norm(np.array(x.centroid) -
                                                 np.array(r.centroid)))
        r0 = Rect.from_point((pivot.x1, r.y0), (r.x1, r.y1))
        r1 = Rect.from_point((r.x0, r.y0), (pivot.x0, r.y1))
        r2 = Rect.from_point((r.x0, pivot.y1), (r.x1, r.y1))
        r3 = Rect.from_point((r.x0, r.y0), (r.x1, pivot.y0))
        for sub_bound in (r0, r1, r2, r3):
            if pruner(sub_bound):
                continue
            sub_q = quality(sub_bound)
            sub_obstacles = [obstacle for obstacle in obstacles
                             if sub_bound.overlap(obstacle)]
            heappush(queue, (-sub_q, sub_bound, sub_obstacles))

    return whitespaces
