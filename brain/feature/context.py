# -*- Coding: utf-8 -*-
"""
Global context used to compute features and relations.
Context contains:
1. content box
2. statistical global context
3. graphs
"""
import numpy as np
import scipy.spatial.qhull as qhull
import itertools
import cv2
import math, os, sys
from pygraph.algorithms.minmax import minimal_spanning_tree
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from lazy import lazy
from rtree import index

# add marmot to python path
path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from marmot.common.rect import Rect
from marmot.common.label import LABEL_FRAGMENT
from marmot.common.label import LABEL_CHAR
from marmot.common.util import almost_equal
from marmot.common.freq import freq_dist, most_freq
from find_whitespace import find_whitespace
from node_descriptor import NodeDescriptor
from edge_descriptor import MSTDescriptor, FigcapDescripor, FigannoDescriptor
from edge_descriptor import TrivialDescriptor
from edge_filter import *
from gabor import Gabor
from marmot.common.raw.document import Document as RawDoc
from marmot.common.physical.document import Document as PhysicalDoc


index_table = 0

class Context(object):
    def __init__(self, page, img_path):
        fragments = [content for content in page.itervalues()
                     if content.label == LABEL_FRAGMENT]
        self._descriptors = tuple(NodeDescriptor(f, i, self) for (i, f)
                                  in enumerate(fragments))
        self._id2descriptors = dict((d.id, d) for d in self._descriptors)
            
        self._make_column_graph()
        img = cv2.imread(img_path)
        self._page_img =  cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        self.page_box = page.raw_page.box
        self._l2p_scale = self._page_img.shape[0] / self.page_box.height
        self._p2l_scale = 1.0 / self._l2p_scale
       
    @property
    def descriptors(self):
        return self._descriptors

    @property
    def l2p_scale(self):
        return self._l2p_scale

    @property
    def p2l_scale(self):
        return self._p2l_scale

    @lazy
    def content_box(self):
        boxes = (d.box for d in self.descriptors)
        return reduce(lambda lhs, rhs: lhs.unite(rhs), boxes)

    def by_id(self, id):
        return self._id2descriptors[id]

    @property
    def edge_types(self):
        return ['mst',
                'figcap',
                'figanno',
                'text_align',
                'text_non_align',
                'non_text_contain_text',
                'non_text_above_text'
                ]
    
    @property
    def edges(self):
        return { 'mst': self.mst_graph,
                 'figcap': self.figcap_graph,
                 'figanno': self.figanno_graph,
                 'text_align': self.text_align_graph,
                 'text_non_align': self.text_non_align_graph,
                 'non_text_contain_text': self.non_text_contain_text_graph,
                 'non_text_above_text': self.non_text_above_text_graph
                 }

    @property
    def edge_descriptors(self):
        return { 'mst': TrivialDescriptor,
                 'figcap': FigcapDescripor,
                 'figanno': FigannoDescriptor,
                 'text_align': TrivialDescriptor,
                 'text_non_align': TrivialDescriptor,
                 'non_text_contain_text': TrivialDescriptor,
                 'non_text_above_text': TrivialDescriptor
                 }

    ############################################################
    # statistical context
    @lazy
    def dominant_font_size(self):
        text_descriptors = (d for d in self.descriptors
                            if d.content_type == LABEL_CHAR)
        freqs = [(d.font_size, len(d.text))
                  for d in text_descriptors]
        [dominant_font_size, _] = most_freq(freq_dist(freqs))
        return dominant_font_size

    @lazy
    def dominant_font_id(self):
        text_descriptors = (d for d in self.descriptors
                            if d.content_type == LABEL_CHAR)
        freqs = [(d.font_id, len(d.text))
                  for d in text_descriptors]
        [dominant_font_id, _] = most_freq(freq_dist(freqs))
        return dominant_font_id

    @lazy
    def width_mean(self):
        widths = [d.box.width for d in self.descriptors]
        return np.mean(widths)

    @lazy
    def width_std(self):
        widths = [d.box.width for d in self.descriptors]
        return np.std(widths)
    
    @lazy
    def height_mean(self):
        heights = [d.box.height for d in self.descriptors]
        return np.mean(heights)
    
    @lazy
    def height_std(self):
        heights = [d.box.height for d in self.descriptors]
        return np.std(heights)

    @lazy
    def area_mean(self):
        areas = [d.box.area for d in self.descriptors]
        return np.mean(areas)
    
    @lazy
    def area_std(self):
        areas = [d.box.area for d in self.descriptors]
        return np.std(areas)

    @lazy
    def _line_spaces(self):
        boxes = [d.box for d in self.descriptors]
        edges = self.column_digraph.edges()
        spaces = [boxes[s].distance_y(boxes[e]) for s, e in edges]
        return spaces
        
    @lazy
    def line_space_mean(self):
        return np.mean(self._line_spaces)

    @lazy
    def line_space_std(self):
        return np.std(self._line_spaces)

    @lazy
    def box_indexer(self):
        boxes = (d.box for d in self.descriptors)
        indexer = index.Index()
        for i, box in enumerate(boxes):
            indexer.insert(i, (box.x0, box.y0, box.x1, box.y1))
        return indexer

    ############################################################
    # graphs
    def _make_column_graph(self):
        self._column_graph = graph()
        self._column_digraph = digraph()
        self._column_graph.add_nodes(range(len(self.descriptors)))
        self._column_digraph.add_nodes(range(len(self.descriptors)))

        boxes = [d.box for d in self.descriptors]
        indexer = self.box_indexer
        for i, box in enumerate(boxes):
            # find boxes that is below current box
            query = Rect(box.x0, box.y1 + 1e-6, box.width, 4 * box.height)
            below = list(indexer.intersection((query.x0, query.y0,
                                               query.x1, query.y1)))
            # remove current box
            if i in below:
                below.remove(i)
            below = [j for j in below if not boxes[j].overlap(box)]
            # pick a nearest box as neighbor
            if below:
                neighbor = min(below,
                               key=lambda x: boxes[i].distance_y(boxes[x]))
                self._column_graph.add_edge((i, neighbor))
                self._column_digraph.add_edge((i, neighbor))

    @property
    def column_graph(self):
        return self._column_graph

    @property
    def column_digraph(self):
        return self._column_digraph

    @lazy
    def overlap_graph(self):
        edges = []
        for d in self._descriptors:
            neighbors = self.box_indexer.intersection((d.box.x0, d.box.y0,
                                                       d.box.x1, d.box.y1))
            for n in neighbors:
                if n != d.index:
                    edges.append(tuple(sorted((d.index, n))))
        return list(set(edges))
    
    @lazy 
    def figcap_graph(self):
        return [(d.index, d.below.index) for d in self._descriptors 
                if d.starts_with_figure_caption and d.below]

    @lazy
    def figanno_graph(self):
        edges = []
        for d in self._descriptors:
            if d.content_type != LABEL_CHAR:
                indexer = self.box_indexer
                query = d.box
                intersection = list(indexer.intersection((query.x0, query.y0,
                                                          query.x1, query.y1)))
                for i in intersection:
                    descriptor = self._descriptors[i]
                    if descriptor.id != d.id:
                        edges.append((d.index, descriptor.index))
        return edges

    def _filter_edges(self, edges, filter_func):
        filtered = []
        for i, j in edges:
            lhs = self._descriptors[i]
            rhs = self._descriptors[j]
            if filter_func(lhs, rhs):
                filtered.append((i, j))
        return filtered

    # def both_text_filter(self, lhs, rhs):
    #     return (lhs.content_type == LABEL_CHAR and
    #             rhs.content_type == LABEL_CHAR)

    # def text_non_text_filter(self, lhs, rhs):
    #     return (bool(lhs.content_type == LABEL_CHAR) !=
    #             bool(rhs.content_type == LABEL_CHAR))

    # def align_filter(self, lhs, rhs):
    #     delta = 0.5 * self.dominant_font_size
    #     return ((almost_equal(lhs.box.x0, rhs.box.x0, tol=delta) and
    #              lhs.is_full_on_right) or
    #             (almost_equal(lhs.box.x1, rhs.box.x1, tol=delta) and
    #              rhs.is_full_on_left))

    # def one_contain_the_other_filter(self, lhs, rhs):
    #     return lhs.box.contain(rhs.box) or rhs.box.contain(lhs.box)
    
    @lazy
    def text_align_graph(self):
        delta = 0.5 * self.dominant_font_size
        text_and_align = (lambda lhs, rhs:
                          filter_both_text(lhs, rhs) and
                          filter_alignment(lhs, rhs, delta))
        return self._filter_edges(self.column_digraph.edges(),
                                  text_and_align)

    @lazy
    def text_non_align_graph(self):
        delta = 0.5 * self.dominant_font_size
        text_non_align = (lambda lhs, rhs:
                          filter_both_text(lhs, rhs) and
                          not filter_alignment(lhs, rhs, delta))
        return self._filter_edges(self.column_digraph.edges(),
                                  text_non_align)

    @lazy
    def non_text_contain_text_graph(self):
        non_text_contain_text = (lambda lhs, rhs:
                                 filter_text_non_text(lhs, rhs) and
                                 filter_containment(rhs, lhs))
        return self._filter_edges(self.overlap_graph,
                                  symmetrize(non_text_contain_text))

    @lazy
    def non_text_above_text_graph(self):
        non_text_above_text = (lambda lhs, rhs:
                               filter_text_non_text(rhs, lhs))
        return self._filter_edges(self.column_digraph.edges(),
                                  non_text_above_text)

    
    ############################################################
    # column and indentation
    @lazy
    def whitespaces(self):
        boxes = [d.box for d in self.descriptors]
        def weight(x):
            if x < 3: return 0.5
            elif 3 <= x < 5: return 1.5
            else: return 1
        def quality(r):
            return r.area * weight(abs(math.log(r.height / r.width, 2)))
        pruner = (lambda r: 2 * r.width <= self.dominant_font_size or
                  r.height < self.content_box.height / 3)
        return find_whitespace(self.content_box, boxes,
                               lambda r: r.area * r.height, pruner)

    @lazy
    def column_separators(self):
        epsilon = 1e-6
        return ([Rect(self.content_box.x0 - epsilon, self.content_box.y0,
                      0, self.content_box.height)] +
                self.whitespaces +
                [Rect(self.content_box.x1 + epsilon, self.content_box.y0,
                      0, self.content_box.height)])
                      
    def abs_indent_level(self, indent):
        return int(indent / (0.5 * self.dominant_font_size))

    @property
    def max_indent_level(self):
        return 4

    @lazy
    def indent_levels(self):
        # the last bin means indent is too large
        max_abs_indent_level = 8
        appearences = [False] * max_abs_indent_level
        for d in self.descriptors:
            abs_indent_level = self.abs_indent_level(d.left_indent)
            if abs_indent_level < max_abs_indent_level:
                appearences[abs_indent_level] = True
        abs_indent_levels = [level for level, appearence
                             in enumerate(appearences) if appearence]
      
        return dict((v, i) for (i, v) in
                    enumerate(abs_indent_levels[:self.max_indent_level]))

    ############################################################
    # unused
    @lazy
    def tri_edges(self):
        boxes = (d.box for d in self.descriptors)
        centroids = np.array([box.centroid for box in boxes],
                             dtype=np.double)
        tri = qhull.Delaunay(centroids)
        edge_set = set()
        for triangle in tri.vertices:
            triangle = triangle[:]
            triangle.sort()
            edge_set.update(itertools.combinations(triangle, 2))
        edges = list(edge_set)
        edges.sort()
        return edges
        
    @lazy
    def mst_graph(self):
        g = graph()
        g.add_nodes(range(len(self.descriptors)))
        for edge in self.tri_edges:
            lhs = np.array(self.descriptors[edge[0]].box.centroid)
            rhs = np.array(self.descriptors[edge[1]].box.centroid)
            weight = np.linalg.norm(lhs - rhs)
            g.add_edge(edge, wt=weight)
        mst = minimal_spanning_tree(g)
        mst = [(k, v) for (k, v) in mst.iteritems() if not v is None]
        return mst

    @lazy
    def vertical_edges(self): 
        boxes = [d.box for d in self.descriptors]
        edges = []
        indexer = index.Index()
        for i, box in enumerate(boxes):
            indexer.insert(i, (box.x0, box.y0, box.x1, box.y1))
        for i, box in enumerate(boxes):
            query = Rect(box.x0, box.y1 + 1e-6, box.width, 4 * box.height)
            below = list(indexer.intersection((query.x0, query.y0,
                                           query.x1, query.y1)))
            # remove current box
            if i in below:
                below.remove(i)
            below = [j for j in below if not boxes[j].overlap(box)]
            if below:
                neighbor = min(below,
                               key=lambda x: boxes[i].distance_y(boxes[x]))
                edges.append((i, neighbor))
        return edges

   ############################################################
    # units_transform
    def _scale_box(self, box, scale):
        return Rect(box.x * scale, box.y * scale,
                    box.width * scale, box.height * scale)

    def p2l_box(self, box):
        return self._scale_box(box, self._p2l_scale)

    def l2p_box(self, box):       
        return self._scale_box(box, self._l2p_scale)

    def crop_img(self, box):
        '''Confine the box to the page image shape.
        '''
        height, width = self._page_img.shape
        # return self._page_img[max(box.y0, 0):min(box.y1, self._page_img.shape[0]),
        #                       max(box.x0, 0):min(box.x1, self._page_img.shape[1])]
        return self._page_img[max(box.y0, 0):min(box.y1, height),
                              max(box.x0, 0):min(box.x1, width)]


   ############################################################
    # table_bounding_box
    @lazy
    def table_bounding_box(self):
        '''Detect all the intersect points of vertical and horizontal lines.
        @return a bounding box (x1,y1,x2,y2) bounding all the intersect points 
        '''
        global index_table

        img = self._page_img
        linewh_ratio_thresh = 20
        # detect vertical lines
        # apply sobel operator with 1st order derivative in x direction 
        dx = cv2.Sobel(img, cv2.CV_16S, 1, 0)
        dx = cv2.convertScaleAbs(dx)
        dx = cv2.dilate(dx, None, iterations = 5)
        contour, hi = cv2.findContours(dx, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # can we consider length_filter to filter short lines?
        for c in contour:
            x, y, w, h = cv2.boundingRect(c)
            #print h/w, self._page_img.shape[0]/4
            if h/w >= linewh_ratio_thresh: #and h >= (self._page_img.shape[0]/6):
                cv2.drawContours(dx, [c], 0, 255, -1)
            else:
                cv2.drawContours(dx, [c], 0, 0, -1)
                
        # detect horizontal lines
        # apply sobel operator with 1st order derivative in y direction 
        dy = cv2.Sobel(img, cv2.CV_16S, 0, 1)
        dy = cv2.convertScaleAbs(dy)
        contour, hi = cv2.findContours(dy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for c in contour:
            x, y, w, h = cv2.boundingRect(c)
            if (w/h >= linewh_ratio_thresh): #and w >= (self._page_img.shape[1]/6):
                cv2.drawContours(dy, [c], 0, 255, -1)
            else:
                cv2.drawContours(dy, [c], 0, 0, -1)
            
        # detect the intersect points by "and" two images
        img_detected = cv2.bitwise_and(dx, dy)
   
        # display the detected intersect points
        contour, hi = cv2.findContours(img_detected, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        #img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        # find the centroids of coutours of points
        centroids = []
        for c in contour:
            mom = cv2.moments(c)           
            if mom['m00'] != 0:
                (x,y) = int(mom['m10']/mom['m00']), int(mom['m01']/mom['m00'])
                cv2.circle(img, (x,y), 10, (255, 0, 0), -1)
                centroids.append((x,y))

        # find the bounding box of points
        if len(centroids) < 5:
            return []
        else:
            maxx, maxy = map(max, zip(*centroids))
            minx, miny = map(min, zip(*centroids))
            if minx == maxx or miny == maxy:
                return []
        table_box = Rect(minx, miny, maxx-minx, maxy-miny)
        index_table = index_table + 1
        
        return self.p2l_box(table_box)

   ############################################################
    # image_bounding_box
    @lazy
    def image_bounding_box(self):
        img = self._page_img
        c_boxes = (self.l2p_box(d.box) for d in self.descriptors if d.content_type == LABEL_CHAR)
        for b in c_boxes:
            cv2.rectangle(img, (int(b.x0), int(b.y0)), (int(b.x1), int(b.y1)), [255], cv2.cv.CV_FILLED)
        thresh = cv2.adaptiveThreshold(img, 255, 1, 1, 5, 2)
        thresh = cv2.dilate(thresh, None, iterations = 5)

        contours, hi = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for c in contours:
            if cv2.contourArea(c) > 200:
                x, y, w, h = cv2.boundingRect(c)
                cv2.rectangle(img, (x, y), (x+w, y+h), (0), 5)

        cv2.namedWindow('peeled', cv2.cv.CV_WINDOW_NORMAL)
        cv2.imshow('peeled', img)
        cv2.waitKey(0)
        return 0

    @lazy
    def gabor_kernels(self):
        wavelength = 8
        kernels = []
        for angle in np.arange(0, np.pi, np.pi / 4):
            g = Gabor(31, 1.5, wavelength, 0.02, 0, np.float32, angle)
            kernels.append(g.get_kernel(angle))
        return kernels

    def gabor_features(self, img, kernel):
        filtered = cv2.filter2D(img, cv2.CV_8U, kernel)
        # power = np.sqrt(imgFilter**2)
        power = filtered
        freqs, bins = np.histogram(power, 3)
        hist = freqs/float(sum(freqs))
        return hist


if __name__ == "__main__":
    dir = '/home/xuch/data-marmot/marmot-data-v2/'   
    doc_name = '10.1.1.1.2018'
    outputdir = './test/'
    page_num = 4

    img_path = os.path.join(dir, doc_name, 'raw', '4.png')

    raw_doc = RawDoc.from_index_xml(
        os.path.join(dir, doc_name, 'raw', 'index.xml'))
    page = raw_doc[page_num]
    phy_doc = PhysicalDoc.from_index_xml(
        raw_doc, os.path.join(dir, doc_name, 'physical', 'index.xml'))
    phy_page = phy_doc[page_num]

    c = Context(phy_page, img_path)
    print c.figcap_graph
