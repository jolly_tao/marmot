# -*- coding: utf-8 -*-

import unicodedata
import itertools
import numpy as np
import cv2
from gabor import Gabor
from lazy import lazy
from marmot.common.label import LABEL_CHAR
from marmot.common.label import LABEL_IMAGE
from marmot.common.label import LABEL_PATH
from marmot.brain.feature import text_pattern


def content_type_check(content_type):
    def _content_type_check(method):
        def __content_type_check(self):
            if content_type != self.content_type:
                return 0
            return method(self)
        return __content_type_check
    return _content_type_check

# TODO: handle negative conditions
def binarize_mean_std(value, mean, std, nr_bins):
    """
    @remark nr_bins should be even.
    """
    half_nr_bins = nr_bins / 2
    neg = [0] * half_nr_bins
    pos = [0] * half_nr_bins
    delta = value - mean
    index = int(abs(delta) * half_nr_bins / std)
    vector = neg if delta < 0 else pos
    try:
        vector[index] = 1
    except IndexError:
        vector[-1] = 1
    return list(reversed(neg)) + pos

class NodeDescriptor(object):
    """Descriptor of a fragment.
    """
    
    def __init__(self, fragment, index, context):
        """
        @param fragment target fragment
        @param index index number of fragment in all fragments
        Context is initialized later.
        """
        self._fragment = fragment
        self._index = index
        self._context = context

    @property
    def context(self):
        return self._context

    @property
    def index(self):
        return self._index

    # @context.setter
    # def context(self, context):
    #     self._context = context

    ############################################################

    @property
    def geometric(self):
        return [ ('x pos', self.x_pos),
                 ('y pos', self.y_pos),
                 ('normalized width', self.normalized_width),
                 ('normalized height', self.normalized_height),
                 ('normalized area', self.normalized_area),
                 ('aspect ratio', self.aspect_ratio),
                 ('is full on right', self.is_full_on_right),
                 ('is full on left', self.is_full_on_left),
                 ]

    @property
    def textual(self):
        return [ ('text', self.text),
                 ('contains digit', self.contains_digit),
                 ('is digit', self.is_digit),
                 ('is upper', self.is_upper),
                 ('contains math', self.contains_math),
                 ('starts with title number', self.starts_with_title_number),
                 ('starts with figure caption', self.starts_with_figure_caption),
                 ('starts with table caption', self.starts_with_table_caption),
                 ('starts with bullet number', self.starts_with_bullet_number),
                 ('starts with bullet alpha', self.starts_with_bullet_alpha),
                 ('starts with bullet symbol', self.starts_with_bullet_symbol),
                 ('starts with bullet roman', self.starts_with_bullet_roman),
                 ('starts with note pattern', self.starts_with_note_symbol),
                 ('ends with sentence terminal', self.ends_with_sentence_terminal),
                 ('contains greek', self.contains_greek),
                 ]
    @property
    def typesetting(self):
        return [ ('is char', self.is_char),
                 ('is image', self.is_image),
                 ('is path', self.is_path),
                 ('greater than dominant font', self.gt_dominant_font_size),
                 ('less than dominant font', self.lt_dominant_font_size),
                 ('equal to dominant font', self.eq_dominant_font_size),
                 ('is dominant font id', self.is_dominant_font_id),
                 ]

    @property
    def local_context(self):
        return [ ('has above', self.has_above),
                 ('above starts with figure caption', self.above_starts_with_figure_caption),
                 ('above starts with table caption', self.above_starts_with_table_caption),
                 ('above starts with bullet', self.above_starts_with_bullet),
                 ('above starts with note', self.above_starts_with_note_symbol),
                 ('above ends with sentence terminal', self.above_ends_with_sentence_terminal),
                 ('above has same indent', self.above_has_same_indent),
                 ('above has deeper indent', self.above_has_deeper_indent),
                 ('above has shallower indent', self.above_has_shallow_indent),
                 ('above is full on right', self.above_is_full_on_right),
                 ('above is full on left', self.above_is_full_on_left),
                 ('above has equal font size', self.above_eq_font_size),
                 ('above has less font size', self.above_lt_font_size),
                 ('above has greater font size', self.above_gt_font_size),
                 ('above is image', self.above_is_non_text),

                 ('has below', self.has_below),
                 ('below starts with bullet', self.below_starts_with_bullet),
                 ('below has same indent', self.below_has_same_indent),
                 ('below has deeper indent', self.below_has_deeper_indent),
                 ('below has shallower indent', self.below_has_shallow_indent),
                 ('below is full on right', self.below_is_full_on_right),
                 ('below is full on left', self.below_is_full_on_left),
                 ('below has equal font size', self.below_eq_font_size),
                 ('below has less font size', self.below_lt_font_size),
                 ('below has greater font size', self.below_gt_font_size),  
                 
                 ('normalized distance from above', self.normalized_distance_from_above),
                 ('normalized distance from below', self.normalized_distance_from_below),
                 ('is in table box', self.is_in_table_box),
                 ('is in image or path', self.is_in_image_or_path),
                 ]

    def _scalar(self):
        return [self.x_pos,
                self.y_pos,
                self.normalized_width,
                self.normalized_height,
                self.normalized_area,
                self.aspect_ratio,
                self.is_full_on_right,
                self.is_full_on_left,
                self.is_char,
                self.is_image,
                self.is_path,
                self.contains_digit,
                self.is_digit,
                self.is_upper,
                self.contains_math,
                self.starts_with_title_number,
                self.starts_with_figure_caption,
                self.starts_with_table_caption,
                self.starts_with_bullet_number,
                self.starts_with_bullet_alpha,
                self.starts_with_bullet_symbol,
                self.starts_with_bullet_roman,
                #self.starts_with_note_symbol,
                self.ends_with_sentence_terminal,
                self.contains_greek,
                self.gt_dominant_font_size,
                self.lt_dominant_font_size,
                self.eq_dominant_font_size,
                self.is_dominant_font_id,
                self.above_starts_with_figure_caption,
                self.above_starts_with_table_caption,
                self.above_starts_with_bullet,
                #self.above_starts_with_note_symbol,
                self.above_ends_with_sentence_terminal,
                self.above_has_same_indent,
                self.above_has_deeper_indent,
                self.above_has_shallow_indent,
                self.above_is_full_on_right,
                self.above_is_full_on_left,
                self.above_eq_font_size,
                self.above_lt_font_size,
                self.above_gt_font_size,
                self.above_is_non_text,
                self.has_above,
                self.has_below,
                self.below_starts_with_bullet,
                self.below_has_same_indent,
                self.below_has_deeper_indent,
                self.below_has_shallow_indent,
                self.below_is_full_on_right,
                self.below_is_full_on_left,
                self.below_eq_font_size,
                self.below_lt_font_size,
                self.below_gt_font_size,
                self.normalized_distance_from_above,
                self.normalized_distance_from_below,
                # self.ratio_of_distance_from_above_and_below,
                self.is_in_table_box,
                self.is_in_image_or_path,
                ]
        
    def _vector(self):
        return [self.binarized_width,
                self.binarized_height,
                self.binarized_area,
                self.binarized_indent_level,
                self.weighted_rel_indent_hist,
                # self.gabor_texture
                ]
    
    def value(self):
        return list(itertools.chain(self._scalar(), *self._vector()))


    ############################################################
    # fragment property forwarding

    @property
    def box(self):
        return self._fragment.box

    @property
    def text(self):
        return self._fragment.text

    @property
    def content_type(self):
        return self._fragment.child_label

    @property
    def font_id(self):
        return self._fragment.font_id

    @property
    def font_size(self):
        return self._fragment.font_size

    @property
    def id(self):
        return self._fragment.id

    @property
    def logical_label(self):
        return self._fragment.logical_label

    ############################################################
    # geo etric
    
    @lazy
    def x_pos(self):
        xc, _ = self.box.centroid
        return ((xc - self.context.content_box.x0) /
                self.context.content_box.width)

    @lazy
    def y_pos(self):
        _, yc = self.box.centroid
        return ((yc - self.context.content_box.y0) /
                self.context.content_box.height)

    @lazy
    def normalized_width(self):
        return ((self.box.width - self.context.width_mean)
                / self.context.width_std)

    @lazy
    def binarized_width(self):
        return binarize_mean_std(self.normalized_width, 0, 1, 4)

    @lazy
    def normalized_height(self):
        return ((self.box.height - self.context.height_mean)
                / self.context.height_std)

    @lazy
    def binarized_height(self):
        return binarize_mean_std(self.normalized_height, 0, 1, 4)

    @lazy
    def normalized_area(self):
        return ((self.box.area - self.context.area_mean)
                / self.context.area_std)

    @lazy
    def binarized_area(self):
        return binarize_mean_std(self.normalized_area, 0, 1, 4)

    @lazy
    def aspect_ratio(self):
        w = self.box.width
        h = self.box.height
        return min(w, h) / max(w, h)
    
    @lazy
    def indent_level(self):       
        abs_indent_level = self.context.abs_indent_level(self.left_indent)
        level = self.context.indent_levels.get(
            abs_indent_level, self.context.max_indent_level)
        return level

    @lazy
    def binarized_indent_level(self):
        v = [0] * (self.context.max_indent_level + 1)     
        v[self.indent_level] = 1
        return v

    @lazy
    def is_full_on_right(self):
        return (self.right_indent < 3 * self.context.dominant_font_size)

    @lazy
    def is_full_on_left(self):
        return (self.left_indent < self.context.dominant_font_size)

    ############################################################
    # content type
    @lazy
    @content_type_check(LABEL_CHAR)
    def is_char(self):
        return 1

    @lazy
    @content_type_check(LABEL_IMAGE)
    def is_image(self):
        return 1

    @lazy
    @content_type_check(LABEL_PATH)
    def is_path(self):
        return 1

    ############################################################
    # typesetting

    @lazy
    @content_type_check(LABEL_CHAR)
    def gt_dominant_font_size(self):
        return (self._fragment.font_size -
                self.context.dominant_font_size > 0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def lt_dominant_font_size(self):
        return (self._fragment.font_size -
                self.context.dominant_font_size < -0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def eq_dominant_font_size(self):
        return (abs(self._fragment.font_size -
                    self.context.dominant_font_size) <= 0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def is_dominant_font_id(self):
        return (self._fragment.font_id ==
                self.context.dominant_font_id)

    @lazy
    @content_type_check(LABEL_CHAR)
    def not_dominant_font_id(self):
        return (self._fragment.font_id !=
                self.context.dominant_font_id)

    ############################################################
    # textual
    @lazy
    def contains_digit(self):
        return any(c.isdigit() for c in self.text)

    @lazy
    def is_digit(self):
        return self.text.isdigit()

    @lazy
    def is_upper(self):
        return self.text.isupper()

    @lazy
    @content_type_check(LABEL_CHAR)
    def is_capitital(self):
        pass
        
    @lazy
    def contains_math(self):
        return any(unicodedata.category(c) == 'Sm'
                   for c in self.text)

    @lazy
    def starts_with_title_number(self):
        return bool(text_pattern.title_number.search(self.text))

    @lazy
    def starts_with_figure_caption(self):
        return bool(text_pattern.figure_caption.search(self.text))

    @lazy
    def starts_with_table_caption(self):
        # if bool(text_pattern.table_caption.search(self.text)) == True:
        #     print 'table caption'
        #     print 'lg :', self.logical_label
        return bool(text_pattern.table_caption.search(self.text))

    @lazy
    def starts_with_bullet_number(self):
        return bool(text_pattern.bullet_number.search(self.text))

    @lazy
    def starts_with_bullet_alpha(self):
        return bool(text_pattern.bullet_alpha.search(self.text))

    @lazy
    def starts_with_bullet_symbol(self):
        return bool(text_pattern.bullet_symbol.search(self.text))

    @lazy
    def starts_with_bullet_roman(self):
        return bool(text_pattern.bullet_roman.search(self.text))

    @lazy
    def starts_with_note_symbol(self):
        return bool(text_pattern.note_symbol.search(self.text))

    @lazy
    def starts_with_bullet(self):
        return (self.starts_with_bullet_number or
                self.starts_with_bullet_alpha or
                self.starts_with_bullet_symbol)

    @lazy
    def contains_greek(self):
        return bool(text_pattern.greek.search(self.text))

    @lazy
    def ends_with_sentence_terminal(self):
        return bool(text_pattern.sentence_terminal.search(self.text))

    ############################################################
    # neighborhood

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_starts_with_figure_caption(self):
        if not self.above:
            return 0
        return self.above.starts_with_figure_caption

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_starts_with_table_caption(self):
        if not self.above:
            return 0
        # if self.above.starts_with_table_caption == True:
        #     print 'above table caption'
        #     print 'lg :', self.logical_label
        return self.above.starts_with_table_caption

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_starts_with_bullet(self):
        if not self.above:
            return 0
        return self.above.starts_with_bullet

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_starts_with_note_symbol(self):
        if not self.above:
            return 0
        return self.above.starts_with_note_symbol

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_ends_with_sentence_terminal(self):
        if not self.above:
            return 0
        return self.above.ends_with_sentence_terminal

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_has_same_indent(self):
        if not self.above:
            return 0
        return self.indent_level == self.above.indent_level

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_has_deeper_indent(self):
        if not self.above:
            return 0
        return self.indent_level < self.above.indent_level

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_has_shallow_indent(self):
        if not self.above:
            return 0
        return self.indent_level > self.above.indent_level

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_is_full_on_right(self):
        if not self.above:
            return 0
        return self.above.is_full_on_right

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_is_full_on_left(self):
        if not self.above:
            return 0
        return self.above.is_full_on_left


    @lazy
    @content_type_check(LABEL_CHAR)
    def above_eq_font_size(self):
        if not self.above:
            return 0
        if self.above.content_type != LABEL_CHAR:
            return 0
        return (abs(self.above.font_size - self.font_size) < 0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_lt_font_size(self):
        if not self.above:
            return 0
        if self.above.content_type != LABEL_CHAR:
            return 0
        return (self.above.font_size - self.font_size < -0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_gt_font_size(self):
        if not self.above:
            return 0
        if self.above.content_type != LABEL_CHAR:
            return 0
        return (self.above.font_size - self.font_size > 0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def above_is_non_text(self):
        if not self.above:
            return 0
        return self.above.content_type != LABEL_CHAR

    @lazy
    @content_type_check(LABEL_CHAR)
    def has_above(self):
        if not self.above:
            return 0
        return 1

    @lazy
    @content_type_check(LABEL_CHAR)
    def has_below(self):
        if not self.below:
            return 0
        return 1

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_starts_with_bullet(self):
        if not self.below:
            return 0
        return self.below.starts_with_bullet

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_has_same_indent(self):
        if not self.below:
            return 0
        return self.indent_level == self.below.indent_level

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_has_deeper_indent(self):
        if not self.below:
            return 0
        return self.indent_level < self.below.indent_level

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_has_shallow_indent(self):
        if not self.below:
            return 0
        return self.indent_level > self.below.indent_level

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_is_full_on_right(self):
        if not self.below:
            return 0
        return self.below.is_full_on_right

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_is_full_on_left(self):
        if not self.below:
            return 0
        return self.below.is_full_on_left


    @lazy
    @content_type_check(LABEL_CHAR)
    def below_eq_font_size(self):
        if not self.below:
            return 0
        if self.below.content_type != LABEL_CHAR:
            return 0
        return (abs(self.below.font_size - self.font_size) < 0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_lt_font_size(self):
        if not self.below:
            return 0
        if self.below.content_type != LABEL_CHAR:
            return 0
        return (self.below.font_size - self.font_size < -0.01)

    @lazy
    @content_type_check(LABEL_CHAR)
    def below_gt_font_size(self):
        if not self.below:
            return 0
        if self.below.content_type != LABEL_CHAR:
            return 0
        return (self.below.font_size - self.font_size > 0.01)

    @lazy
    def y_distance_from_above(self):
        if not self.above:
            return 0
        return self.box.distance_y(self.above.box)

    @lazy
    def normalized_distance_from_above(self):
        return ((self.y_distance_from_above - self.context.line_space_mean) /
                self.context.line_space_std)
    
    @lazy
    def y_distance_from_below(self):
        if not self.below:
            return 0
        return self.box.distance_y(self.below.box)

    @lazy
    def normalized_distance_from_below(self):
        return ((self.y_distance_from_below - self.context.line_space_mean) /
                self.context.line_space_std)

    @lazy
    def ratio_of_distance_from_above_and_below(self):
        if self.above and self.below:
            y1 = self.y_distance_from_above
            y2 = self.y_distance_from_below
            return min(y1, y2) / max(y1, y2)
        return 0

    @lazy
    def rel_indents(self):
        """Return a vector indicating local relative indentations.
        -1 for shallower, 0 for same, 1 for deeper, None for absent.
        """
        rel_indent = lambda d: d.box.x - self.box.x if d else None
        above_indents = [rel_indent(d) for d in self.aboves(2)]
        below_indents = [rel_indent(d) for d in self.belows(2)]
        return list(itertools.chain(reversed(above_indents),
                                    below_indents))

    @lazy
    def weighted_rel_indent_hist(self):
        def rel_indent_level(rel_indent):
            if not rel_indent:
                return None
            threshold = self.context.dominant_font_size
            if rel_indent < -threshold:
                return 0
            elif rel_indent > threshold:
                return 2
            return 1
        hist = [0, 0, 0,]
        rel_indent_levels = [rel_indent_level(x) for x in self.rel_indents]
        weights = [1] * 4
        for l, w in zip(rel_indent_levels, weights):
            if l is not None:
                hist[l] += w
        s = sum(hist)
        if s > 0:
            hist = map(lambda x: x/s, hist)
        return hist

    ############################################################
    # secondary properties

    @property
    def x_without_bullet(self):
        for pattern in text_pattern.bullet_pattern:
            m = pattern.search(self.text)
            if m:
                return self._fragment.children[m.end()].box.x
        return self._fragment.box.x

    @property
    def left_indent(self):
        left_bound, _ = self.column_bound
        return self.box.x0 - left_bound

    @property
    def right_indent(self):
        _, right_bound = self.column_bound
        return right_bound - self.box.x1

    @lazy
    def column_bound(self):
        candidates = [s for s in self.context.column_separators
                      if -s.distance_y(self.box) > self.box.height / 2]
        left_bound = max(c.x1 for c in candidates if self.box.x0 >= c.x1)
        right_bound = min(c.x0 for c in candidates if self.box.x1 <= c.x0)
        return (left_bound, right_bound)

    @lazy
    def above(self):
        above_indice = self.context.column_digraph.incidents(self._index)
        if not above_indice:
            return None
        else:
            descriptors = self.context.descriptors
            aboves = [self.context.descriptors[i] for i in above_indice]
            return min(aboves,
                       key=lambda d: self.box.distance_y(d.box))

    def aboves(self, k):
        v = []
        d = self
        for i in range(k):
            above = d.above if d else None
            v.append(above)
            d = above
        return v
        
    @lazy
    def below(self):
        below_indice = self.context.column_digraph.neighbors(self._index)
        if not below_indice:
            return None
        else:
            descriptors = self.context.descriptors
            belows = [self.context.descriptors[i] for i in below_indice]
            return min(belows,
                       key=lambda d: self.box.distance_y(d.box))

    def belows(self, k):
        v = []
        d = self
        for i in range(k):
            below = d.below if d else None
            v.append(below)
            d = below
        return v

    ############################################################
    # image features
    @lazy
    def gabor_texture(self):
        angles = np.arange(0, np.pi, np.pi / 4)
        wavelengths = [8]       
   
        img_box = self.context.l2p_box(self.box)
        crop_img = self.context.crop_img(img_box)

        features = [self.context.gabor_features(crop_img, kernel)
                    for kernel in self.context.gabor_kernels]

        # for wavelength in wavelengths:
        #     for angle in angles:
        #         g = Gabor(31, 1.5, wavelength, 0.02, 0, np.float32, angle)
        #         kernel = g.get_kernel(angle)
        #         f = g.get_features(crop_img, kernel) 
        #         features.append(f)
        return list(itertools.chain(*features))

    ############################################################
    # table features
    @lazy
    def is_in_table_box(self):
        table_box = self.context.table_bounding_box
        if table_box and self.is_image == 0:
            return table_box.contain(self.box)
        return False

    @lazy
    def is_in_image_or_path(self):
        idx = self.context.box_indexer
        descriptors = self.context.descriptors
        query = self.box
        intersection = list(idx.intersection((query.x0, query.y0,
                                              query.x1, query.y1)))
        for i in intersection:
            descriptor = descriptors[i]
            if (descriptor.id != self.id and
                descriptor.box.contain(self.box) and
                descriptor.content_type != LABEL_CHAR):
                return True
        return False
