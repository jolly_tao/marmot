import cv2, sys
import numpy as np
import math, time
import itertools

class Gabor(object):
    def __init__(self, ksize=31, sigma=1.5, lambd=15, gamma=0.02, psi=0, ktype=np.float32, theta=[]): 
        self.ksize = ksize
        self.sigma = sigma
        self.lambd = lambd
        self.gamma = gamma
        self.psi = psi
        self.ktype = ktype
        self.theta = theta
           
    def get_kernel(self, theta):
        # TODO: compute and store
        sigma_x = self.sigma
        sigma_y = self.sigma/self.gamma
        nstds = 3 
        c = math.cos(theta)
        s = math.sin(theta)
        if( self.ksize > 0 ):
            xmax = self.ksize/2
        else:
            xmax = max(math.fabs(nstds*sigma_x*c), math.fabs(nstds*sigma_y*s))
        ymax = xmax
        ymin = -ymax
        xmin = -xmax
        kernel = np.zeros((ymax - ymin + 1, xmax - xmin + 1), dtype = self.ktype)
        scale = 1.0
        ex = -0.5/(sigma_x*sigma_x)
        ey = -0.5/(sigma_y*sigma_y)
       
        cscale = np.pi*2/self.lambd
        
        for y in range(ymin, ymax):
            for x in range(xmin, xmax):
                xr = x*c + y*s
                yr = -x*s + y*c
                v = scale*math.exp(ex*xr*xr + ey*yr*yr)*math.cos(cscale*xr + self.psi)
                kernel[ymax + y, xmax + x] = v
        return np.real(kernel)   

    def get_features(self, img, kernel):   
        filtered = cv2.filter2D(img, cv2.CV_8U, kernel)
        # power = np.sqrt(imgFilter**2)
        power = filtered
        freqs, bins = np.histogram(power, 3)
        hist = freqs/float(sum(freqs))
        return hist

