# -*- coding: utf-8 -*-

import re
import regex

title_number = re.compile(ur'^\d{1,2}(\.\d{1,2})+', re.U)
title_chinese = re.compile(ur'^第(.{1,2})[章节]', re.U)
figure_caption = re.compile(ur'^(fig\.|figure|图)\s*\d+', re.U|re.I)
table_caption = re.compile(ur'^(tab\.|table|表)\s*(\d+|(IX|IV|VI{0,3}|I{1,3}))', re.U|re.I)
greek = regex.compile(ur'\p{Block=Greek}')
sentence_terminal = regex.compile(ur'\p{IsSTerm}$')

# list bullet

bullet_number = re.compile(ur'^[(（]{0,1}(\d{1,2})[)）.]', re.U)
bullet_alpha = re.compile(ur'^[(（]{0,1}([a-zA-Z])[)）.]', re.U)

_bullet_symbol_names = (
    u'MIDDLE DOT',
    u'GREEK ANO TELEIA',
    u'HYPHENATION POINT',
    u'HALFWIDTH KATAKANA MIDDLE DOT',
    u'BLACK SQUARE',
    u'BLACK LARGE SQUARE',

    u'BULLET',
    u'ONE DOT LEADER',
    u'WORD SEPARATOR MIDDLE DOT',
    u'BULLET OPERATOR',
    u'DOT OPERATOR',
    u'CANADIAN SYLLABICS FINAL MIDDLE DOT',
    u'RUNIC SINGLE PUNCTUATION',
    u'KATAKANA MIDDLE DOT',
    # this character is of length 2 in Windows
    # u'AEGEAN WORD SEPARATOR DOT',

    u'TRIANGULAR BULLET',
    u'BLACK RIGHT-POINTING SMALL TRIANGLE',

    u'BLACK DIAMOND',
    u'WHITE DIAMOND',
    u'EN DASH',
    u'EM DASH',
    u'MINUS SIGN',
    u'LOWER RIGHT DROP-SHADOWED WHITE SQUARE',   
    )
_bullet_symbol_expr = u'^(' + \
    u'|'.join([ur'\N{%s}' % name for name in _bullet_symbol_names]) + \
    u')'

_note_symbol_names = (
    u'ASTERISK',
    u'DAGGER',
    u'DOUBLE DAGGER',
    u'SECTION SIGN',
    u'PILCROW SIGN',
)
_note_symbol_expr = u'^(' + \
    u'|'.join([ur'\N{%s}' % name for name in _note_symbol_names]) + \
    u')'

bullet_symbol = regex.compile(_bullet_symbol_expr)

bullet_roman = re.compile(ur'^[(（]{0,1}(IX|IV|VI{0,3}|I{1,3})[)）]', re.U|re.I)

bullet_pattern = (bullet_number, bullet_alpha, bullet_symbol, bullet_roman)

note_symbol = re.compile(ur'^(\d{1,2})', re.U)
#note_symbol = regex.compile(_note_symbol_expr)
# note_pattern = (note_number, note_symbol)
