# -*- coding: utf-8 -*-

import numpy as np
from abc import ABCMeta
from abc import abstractmethod

from lazy import lazy
from marmot.common.label import LABEL_CHAR
from marmot.common.label import LABEL_IMAGE
from marmot.common.label import LABEL_PATH

def content_type_check(type_lhs, type_rhs):
    def _content_type_check(method):
        def __content_type_check(self):
            if (type_lhs != self._lhs.content_type or
                type_rhs != self._rhs.content_type):
                return 0
            return method(self)
        return __content_type_check
    return _content_type_check

class EdgeDescriptor(object):
    """Descriptor of an edge.
    """
    
    def __init__(self, lhs, rhs, context):
        """
        @param lhs one of the node descriptors.
        @param rhs the other of node descriptors.
        @param context global contextual info
        """
        self._lhs = lhs
        self._rhs = rhs
        self._context = context

    @abstractmethod
    def value(self):
        pass

    @lazy
    def normalized_distance(self):
        return (abs(self._rhs.box.distance_y(self._lhs.box) -
                    self._context.line_space_mean) 
                / self._context.line_space_std)

    @lazy
    def same_content_type(self):
        return (self._lhs.content_type == self._rhs.content_type)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def have_same_font_id(self):
        return (self._lhs.font_id == self._rhs.font_id)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def eq_font_size(self):
        return float(abs(self._lhs.font_size - self._rhs.font_size)
                     < 0.01)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def align_without_bullet(self):
        return (abs(self._lhs.x_without_bullet -
                    self._rhs.x_without_bullet) <
                0.5 * self._context.dominant_font_size)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def have_same_indentation(self):
        return float(self._lhs.indent_level ==
                     self._rhs.indent_level)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def are_left_align(self):
        return (abs(self._lhs.box.x0 - self._rhs.box.x0) <
                0.5 * self._context.dominant_font_size)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def are_center_align(self):
        return (abs((self._lhs.box.x0 + self._lhs.box.x1) -
                    (self._rhs.box.x0 + self._rhs.box.x1)) <
                1.0 * self._context.dominant_font_size)

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def are_right_align(self):
        return (abs(self._lhs.box.x1 - self._rhs.box.x1) <
                0.5 * self._context.dominant_font_size)

    @lazy
    def are_overlap(self):
        return self._lhs.box.overlap(self._rhs.box)

    @lazy
    def height_ratio(self):
        return (min(self._lhs.box.height, self._rhs.box.height) /
                max(self._lhs.box.height, self._rhs.box.height))

    @lazy
    def width_ratio(self):
        return (min(self._lhs.box.width, self._rhs.box.width) /
                max(self._lhs.box.width, self._rhs.box.width))

    @lazy
    def area_ratio(self):
        return (min(self._lhs.box.area, self._rhs.box.area) /
                max(self._lhs.box.area, self._rhs.box.area))

    @lazy
    def does_one_contain_the_other(self):
        return (self._lhs.box.contain(self._rhs.box) or
                self._rhs.box.contain(self._lhs.box))

    @lazy
    def gabor_texture_diff(self):
        return -np.linalg.norm(np.array(self._lhs.gabor_texture) - np.array(self._rhs.gabor_texture)) 

    @lazy
    @content_type_check(LABEL_CHAR, LABEL_CHAR)
    def gabor_texture_diff_eq(self):
        if self._lhs.logical_label == self._rhs.logical_label:
            if self._lhs.logical_label == 'figure_caption_cont':
                return np.linalg.norm(np.array(self._lhs.gabor_texture) - np.array(self._rhs.gabor_texture)) 
        return 0

    @lazy
    @content_type_check(LABEL_IMAGE, LABEL_CHAR)
    def gabor_texture_diff_ineq(self):
        if self._lhs.logical_label != self._rhs.logical_label:
            return np.linalg.norm(np.array(self._lhs.gabor_texture) - np.array(self._rhs.gabor_texture)) 
        return 0


class TrivialDescriptor(EdgeDescriptor):
    def __init__(self, lhs, rhs, context):
        EdgeDescriptor.__init__(self, lhs, rhs, context)

    def value(self):
        # bias only
        return [1]
    
# Concrete descriptors
class MSTDescriptor(EdgeDescriptor):
    def __init__(self, lhs, rhs, context):
        EdgeDescriptor.__init__(self, lhs, rhs, context)

    def value(self):
        return [1,
                # self.have_same_font_id,
                # self.eq_font_size,
                # self.are_left_align,
                # self.are_overlap,
                # self.height_ratio,
                # self.width_ratio,
                # self.area_ratio,
                # self.does_one_contain_the_other,
                # self.are_center_align,
                # self.are_right_align,
                # self.normalized_distance,
                # self.same_content_type,
                #self.gabor_texture_diff,
                
                # self.gabor_texture_diff_eq,
                # self.gabor_texture_diff_ineq
                ]

class FigcapDescripor(EdgeDescriptor):
    def __init__(self, lhs, rhs, context):
        EdgeDescriptor.__init__(self, lhs, rhs, context)

    def value(self):
        return [1,
                self.have_same_font_id,
                self.eq_font_size,
                self.are_left_align,
                self.height_ratio,
                self.are_center_align,
                self.normalized_distance,
                ]
    
class FigannoDescriptor(EdgeDescriptor):
    def __init__(self, lhs, rhs, context):
        EdgeDescriptor.__init__(self, lhs, rhs, context)

    def value(self):
        return [1,
                self.are_overlap,
                self.height_ratio,
                self.width_ratio,
                self.area_ratio,
                self.does_one_contain_the_other,
                self.same_content_type,
                ]
