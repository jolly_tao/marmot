# -*- coding: utf-8 -*-

from marmot.common.util import almost_equal
from marmot.common.label import LABEL_CHAR

def symmetrize(function):
    return lambda lhs, rhs: function(lhs, rhs) or function(rhs, lhs)

def filter_both_text(lhs, rhs):
    return (lhs.content_type == LABEL_CHAR and
            rhs.content_type == LABEL_CHAR)

def filter_text_non_text(lhs, rhs):
    return (bool(lhs.content_type == LABEL_CHAR) and
            bool(rhs.content_type != LABEL_CHAR))

def filter_alignment(lhs, rhs, delta):
    return ((almost_equal(lhs.box.x0, rhs.box.x0, tol=delta) and
             lhs.is_full_on_right) or
            (almost_equal(lhs.box.x1, rhs.box.x1, tol=delta) and
             rhs.is_full_on_left))

def filter_containment(lhs, rhs):
    return lhs.box.contain(rhs.box)
    
