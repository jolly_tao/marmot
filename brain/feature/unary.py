# -*- coding: utf-8 -*-

import unicodedata
import itertools
from lazy import lazy
from marmot.common.label import LABEL_CHAR
from marmot.common.label import LABEL_IMAGE
from marmot.common.label import LABEL_PATH
from marmot.brain.feature import text_pattern

def child_label_check(label):
    def _child_label_check(method):
        def __child_label_check(self):
            if label != self._fragment.child_label:
                return 0
            return method(self)
        return __child_label_check
    return _child_label_check

class UnaryFeature(object):
    """Unary feature of a fragment.
    """
    
    def __init__(self, fragment, context, index):
        """
        @param fragment target fragment
        @param context  global contextual info
        @param index index number of fragment in all fragments
        """
        self._fragment = fragment
        self._context = context
        self._index = index

    def _scalar(self):
        return [self.x_pos,
                self.y_pos,
                self.normalized_width,
                self.normalized_height,
                self.normalized_area,
                self.aspect_ratio,
                self.is_char,
                self.is_image,
                self.is_path,
                self.contain_digit,
                self.is_digit,
                self.is_upper,
                self.contain_math,
                self.starts_with_title_number,
                self.starts_with_figure_caption,
                self.starts_with_bullet_number,
                self.starts_with_bullet_alpha,
                self.starts_with_bullet_symbol,
                self.contain_greek,
                self.is_short,
                self.gt_dominant_font_size,
                self.lt_dominant_font_size,
                self.eq_dominant_font_size,
                self.is_dominant_font_id,
                self.above_has_figure_caption_pattern
                ]
        
    def _vector(self):
        return [self.indent_level
                ]
    
    def value(self):
        return list(itertools.chain(self._scalar(), *self._vector()))

    def svm(self):
        return self.value()

    def crf(self):
        return [self.starts_with_bullet_number,
                self.starts_with_bullet_alpha,
                self.starts_with_bullet_symbol,
                ]

    ############################################################
    # geometric features
    @lazy
    def x_pos(self):
        # page_xc, _ = self._context.content_box.centroid
        # xc, _ = self._fragment.box.centroid
        # return (xc - page_xc) * 2 / self._context.content_box.width
        xc, _ = self._fragment.box.centroid
        return ((xc - self._context.content_box.x0) /
                self._context.content_box.width)

    @lazy
    def y_pos(self):
        # _, page_yc = self._context.content_box.centroid
        # _, yc = self._fragment.box.centroid
        # return (yc - page_yc) * 2 / self._context.content_box.height
        _, yc = self._fragment.box.centroid
        return ((yc - self._context.content_box.y0) /
                self._context.content_box.height)

    @lazy
    def normalized_width(self):
        return ((self._fragment.box.width - self._context.width_mean)
                / self._context.width_std)

    @lazy
    def normalized_height(self):
        return ((self._fragment.box.height - self._context.height_mean)
                / self._context.height_std)

    @lazy
    def normalized_area(self):
        return ((self._fragment.box.area - self._context.area_mean)
                / self._context.area_std)

    @lazy
    def aspect_ratio(self):
        w = self._fragment.box.width
        h = self._fragment.box.height
        return min(w, h) / max(w, h)

    @lazy
    def indent_level(self):
        return self._context.indent_level(self.left_indent)
        
    @lazy
    @child_label_check(LABEL_CHAR)
    def is_char(self):
        return 1

    @lazy
    @child_label_check(LABEL_IMAGE)
    def is_image(self):
        return 1

    @lazy
    @child_label_check(LABEL_PATH)
    def is_path(self):
        return 1


    ############################################################
    # text feature
    @lazy
    def contain_digit(self):
        return any(c.isdigit() for c in self._fragment.text)

    @lazy
    def is_digit(self):
        return self._fragment.text.isdigit()

    @lazy
    def is_upper(self):
        return self._fragment.text.isupper()

    @lazy
    def contain_math(self):
        return any(unicodedata.category(c) == 'Sm'
                   for c in self._fragment.text)

    @lazy
    def starts_with_title_number(self):
        return bool(text_pattern.title_number.search(
            self._fragment.text))

    @lazy
    def starts_with_figure_caption(self):
        return bool(text_pattern.figure_caption.search(
            self._fragment.text))

    @lazy
    def starts_with_bullet_number(self):
        return bool(text_pattern.bullet_number.search(
            self._fragment.text))

    @lazy
    def starts_with_bullet_alpha(self):
        return bool(text_pattern.bullet_alpha.search(
            self._fragment.text))

    @lazy
    def starts_with_bullet_symbol(self):
        return bool(text_pattern.bullet_symbol.search(
            self._fragment.text))

    @lazy
    def starts_with_bullet(self):
        return (self.starts_with_bullet_number or
                self.starts_with_bullet_alpha or
                self.starts_with_bullet_symbol)

    @lazy
    def contain_greek(self):
        return bool(text_pattern.greek.search(self._fragment.text))

    @lazy
    @child_label_check(LABEL_CHAR)
    def is_short(self):
        return len(self._fragment.text) <= 3

    ############################################################
    # typesetting feature

    @lazy
    @child_label_check(LABEL_CHAR)
    def gt_dominant_font_size(self):
        return (self._fragment.font_size -
                self._context.dominant_font_size > 0.01)

    @lazy
    @child_label_check(LABEL_CHAR)
    def lt_dominant_font_size(self):
        return (self._fragment.font_size -
                self._context.dominant_font_size < -0.01)

    @lazy
    @child_label_check(LABEL_CHAR)
    def eq_dominant_font_size(self):
        return (abs(self._fragment.font_size -
                    self._context.dominant_font_size) <= 0.01)

    @lazy
    @child_label_check(LABEL_CHAR)
    def is_dominant_font_id(self):
        return (self._fragment.font_id ==
                self._context.dominant_font_id)

    @lazy
    @child_label_check(LABEL_CHAR)
    def not_dominant_font_id(self):
        return (self._fragment.font_id !=
                self._context.dominant_font_id)

    ############################################################
    # features from neighbors
    @lazy
    @child_label_check(LABEL_CHAR)
    def above_has_figure_caption_pattern(self):
        if not self.above:
            return 0
        return bool(text_pattern.figure_caption.search(
            self.above.text))

    ############################################################
    # descripter interface, not part of features

    @property
    def box(self):
        return self._fragment.box

    @property
    def text(self):
        return self._fragment.text

    @property
    def child_label(self):
        return self._fragment.child_label

    @property
    def font_id(self):
        return self._fragment.font_id

    @property
    def font_size(self):
        return self._fragment.font_size

    @property
    def x_without_bullet(self):
        for pattern in text_pattern.bullet_pattern:
            m = pattern.search(self.text)
            if m:
                return self._fragment.children[m.end()].box.x
        return self._fragment.box.x

    @lazy
    def left_indent(self):
        left_bound, _ = self._context.column_bounds[self._fragment.id]
        return self.box.x0 - left_bound

    @lazy
    def right_indent(self):
        _, right_bound = self._context.column_bounds[self._fragment.id]
        return right_bound - self.box.x1

    @lazy
    def above(self):
        above_indice = self._context.column_digraph.incidents(self._index)
        if not above_indice:
            return None
        else:
            fragments = self._context.fragments
            aboves = [self._context.fragments[i] for i in above_indice]
            return min(aboves,
                       key=lambda f: self._fragment.box.distance_y(f.box))
