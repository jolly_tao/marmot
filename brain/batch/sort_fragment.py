# -*- coding: utf-8 -*-

'''
sort_fragment:
1> read physical xml
2> sort IDs for char fragments in xml
3> write back to xml
'''
import sys
import os
import numpy
import argparse
from sklearn.preprocessing import LabelEncoder

# add marmot to python path
path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from marmot.common.raw.document import Document as RawDoc
from marmot.common.physical.document import Document as PhysicalDoc
from marmot.common.label import *
from batch_template import batch_template

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='read physical xmls, sort IDs for char fragments in xmls '
                    'according to fragment orientation and write back to xmls.')
    parser.add_argument('target',
                        help='index of sample data names and pages.')

    args = parser.parse_args()

    def sort_page_fragment(doc_name, physical_doc, page_num):
        page = physical_doc[page_num]
        fragments = (content for content in page.itervalues()
                     if content.label == LABEL_FRAGMENT)
        for fragment in fragments:
            if fragment.child_label == LABEL_CHAR:
                if fragment.box.width > fragment.box.height:
                    fragment.sort(key=lambda child: child.box.x)
                    print fragment.text
                else:
                    fragment.sort(key=lambda child: child.box.y)
                    print fragment.text                        
        physical_doc.flush_page(page_num)

    batch_template(args.target, sort_page_fragment, None, lambda x, y: None)
