# -*- coding: utf-8 -*-
'''
export text for matlab:
1>  read physical xml
2>  export labels, adjacent matrix, local features and relational
    features to text file.
'''

import sys
import os
import numpy as np
import argparse
import itertools

# add marmot to python path
path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from marmot.common.label import *
from marmot.brain.feature.node_descriptor import NodeDescriptor
from marmot.brain.feature.edge_descriptor import EdgeDescriptor
from marmot.brain.feature.context import Context
# from marmot.brain.feature.unary import UnaryFeature
# from marmot.brain.feature.binary import BinaryFeature
from marmot.brain.batch.batch_template import batch_template
from marmot.brain.util import label_encoder

def _scale_min_max(features):
    """for each instance feature,
    new_feature = (feature - feature_min) / (feature_max - feature_min).
    """
    features_min = np.amin(features, axis=0)
    features_max = np.amax(features, axis=0)
    indices = tuple(np.where(features_min == features_max))
    features_min[indices] = 0
    features_max[indices] = 1
    # use broadcasting
    return (features - features_min) / (features_max - features_min)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='read physical xmls, export labels, adjacent matrix, '
                    'local features and relational features to text file.')
    parser.add_argument('target',
                        help='index of sample data names and pages.')
    parser.add_argument('text_file',
                        help='output text file.')
    args = parser.parse_args()

    with open(args.text_file, 'w') as f:
        f.write('nLabels=%d\n\n' % (len(label_encoder.classes_) - 1))
        f.write('\n'.join(label_encoder.classes_[:-1]))
        f.write('\n\n')
            
        def export_page_to_text(doc_name, physical_doc, page_num, img_path):
            page = physical_doc[page_num]
            context = Context(page, img_path)
            node_descriptors = context.descriptors
            # context = Context(page)
            # fragments = context.fragments

            # edges = context.column_digraph.edges()
            edges = context.mst
            figcap_edges = context.figcap_graph
                       
            # sort_edge = lambda (x, y): (x, y) if x < y else (y, x)
            # edges = set()
            # for edge in itertools.chain(context.mst,
            #                             context.column_digraph.edges()):
            #     edges.add(sort_edge(edge))
            
            # edges = set(context.mst + context.column_digraph.edges())

            node_features = np.array([d.value() for d in node_descriptors],
                                     np.float)
            node_num, node_feature_num = node_features.shape


            edge_descriptors = [EdgeDescriptor(node_descriptors[i],
                                               node_descriptors[j],
                                               context)
                                for i, j in edges]
            edge_features = np.array([d.value() for d in edge_descriptors],
                                     np.float)
            edge_features = _scale_min_max(edge_features)
            edge_num, edge_feature_num = edge_features.shape

            if figcap_edges:
                figcap_edge_descriptors = [EdgeDescriptor(node_descriptors[i],
                                                          node_descriptors[j],
                                                          context)
                                           for i, j in figcap_edges]
                figcap_edge_features = np.array(
                    [d.figcap_value() for d in figcap_edge_descriptors],
                    np.float)
                figcap_edge_features = _scale_min_max(figcap_edge_features)
                figcap_edge_num, figcap_edge_feature_num = \
                    figcap_edge_features.shape
          

            # document name
            f.write('docName=%s\n' % doc_name)
            # document page number
            f.write('pageNum=%d\n' % page_num)

            # number of nodes
            f.write('nNodes=%d\n' % node_num)
            # frament ids
            f.write('%s\n' % '\n'.join(d.id for d in node_descriptors))

            # labels
            labels = label_encoder.transform(
                [d.logical_label for d in node_descriptors])
            # label starts from 1
            f.write('%s\n' % ' '.join(str(label) for label in labels.astype(int)))
            
            # number of edges
            f.write('nEdges=%d\n' % edge_num)
            # edges
            for s, e in edges:
                f.write('%d %d\n' % (s, e)) # variable numbering starts from 0
                
            f.write('nFigcapEdges=%d\n' % len(figcap_edges))
            if figcap_edges:
                for s, e in figcap_edges:
                    f.write('%d %d\n' % (s, e)) 

            # svm unary features
            f.write('nNodeFeatures=%d\n' % node_feature_num)
            for value in node_features:
                f.write('%s\n' % ' '.join(str(v) for v in value))

            # binary features
            f.write('nEdgeFeatures=%d\n' % edge_feature_num)
            for value in edge_features:
                f.write('%s\n' % ' '.join(str(v) for v in value))

            # figcap binary features
            if figcap_edges:            
                f.write('nFigcapEdgeFeatures=%d\n' % figcap_edge_feature_num)
                for value in figcap_edge_features:
                    f.write('%s\n' % ' '.join(str(v) for v in value))
            f.write('\n')
        batch_template(args.target, export_page_to_text, [], lambda x, y: None)
