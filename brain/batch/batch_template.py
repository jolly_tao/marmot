# template functionf for batch processing

import os

# from marmot.common.raw.document import Document as RawDoc
# from marmot.common.physical.document import Document as PhysicalDoc
from marmot.common.document import Document

def batch_template(path, handler, init=None, combiner=None):
    """
    This is a template function for batch processing.
    @param path     Target path.
    @param handler  Handler function for each page.
                    should be handler(phy_doc, page_num)
    @param init     Inital return value
    @param combiner Function combining return value of handler function.
    @return Combination of return values of handler function on every page.
    """
    final_ret = init
    dir = os.path.dirname(path)
    with open(path) as f:
        for line in f:
            # split line into document name and page numbers
            name, page_nums = line.split(':')
            page_nums = [int(page_num) for page_num in page_nums.split()]
            print 'processing: %s' % name

            doc = Document(os.path.join(dir, name))
            # # read document
            # raw_doc = RawDoc.from_index_xml(
            #     os.path.join(dir, name, 'raw', 'index.xml'))
            # physical_doc = PhysicalDoc.from_index_xml(
            #     raw_doc, os.path.join(dir, name, 'physical', 'index.xml'))
           
            # handle each target page
            for page_num in page_nums:
                # page = physical_doc[page_num]
                img_path = os.path.join(dir, name, 'raw', str(page_num) + '.png')
                ret = handler(name, doc.physical, page_num, img_path)
                if combiner:
                    final_ret = combiner(final_ret, ret)
    return final_ret
