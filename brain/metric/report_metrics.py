# -*- coding: utf-8 -*-

import sys
import os
import numpy as np
import argparse
import sklearn.metrics as metrics

# add marmot to python path
path = os.path.abspath(__file__)
for i in range(4):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from marmot.brain.io import load_y

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='report classificatino metrics from '
                    'ground-truth and prediction.')
    parser.add_argument('groundtruth_file',
                        help='groundtruth_file_path')
    parser.add_argument('prediction_file',
                        help='prediction_file_path')
    args = parser.parse_args()

    y_true = load_y(args.groundtruth_file)
    y_pred = load_y(args.prediction_file)

   # precision
    precision = metrics.precision_score(y_true, y_pred,
                                        average=None)
    print 'precision = \n', '\n'.join('%.2f' % (100*i) for i in precision)

    # recall
    recall = metrics.recall_score(y_true, y_pred,
                                  average=None)
    print 'recall = \n', '\n'.join('%.2f' % (i*100) for i in recall)

    # F1
    f1 = metrics.f1_score(y_true, y_pred,
                          average=None)
    print 'f1 measure = \n', '\n'.join('%.2f' % (100*i) for i in f1)

    # precision average
    micro_average_precision = metrics.precision_score(y_true, y_pred,
                                                      labels=None, pos_label=1,
                                                      average='micro')
    print 'micro average precision = %.2f ' %  (micro_average_precision*100)

    macro_average_precision = metrics.precision_score(y_true, y_pred,
                                                      labels=None, pos_label=1,
                                                      average='macro')
    print 'macro average precision = %.2f ' % (macro_average_precision*100)

    # recall average
    micro_average_recall = metrics.recall_score(y_true, y_pred,
                                                labels=None, pos_label=1,
                                                average='micro')
    print 'micro average recall = %.2f' % (micro_average_recall*100)

    macro_average_recall = metrics.recall_score(y_true, y_pred,
                                                labels=None, pos_label=1,
                                                average='macro')
    print 'macro average recall = %.2f' % (macro_average_recall*100)

    # f1 measure average
    micro_average_f1 = metrics.f1_score(y_true, y_pred,
                                      labels=None, pos_label=1,
                                      average='micro')
    print 'micro average f1 = %.2f' % (micro_average_f1*100)

    macro_average_f1 = metrics.f1_score(y_true, y_pred,
                                      labels=None, pos_label=1,
                                      average='macro')
    print 'macro average f1 = %.2f' % (macro_average_f1*100)
