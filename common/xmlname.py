# -*- coding: utf-8 -*-

# namespaces
NAMESPACE_RAW = 'http://www.founderrd.com/marmot/schema/1.1/raw'
NAMESPACE_PHYSICAL = 'http://www.founderrd.com/marmot/schema/1.1/physical'

# element tags
ELEM_DOC = 'doc'
ELEM_PAGE = 'page'
ELEM_CONTENTS = 'contents'
ELEM_CHAR = 'char'
ELEM_IMAGE = 'image'
ELEM_PATH = 'path'
ELEM_FRAGMENT = 'fragment'
ELEM_BLOCK = 'block'
ELEM_BOX = 'box'
ELEM_TEXT_STATE = 'textState'

# attribute tags
ATTR_XML = 'xml'
ATTR_THUMB = 'thumb'
ATTR_PAGE_NUM = 'pageNum'
ATTR_ID = 'id'
ATTR_CHAR = 'char'
ATTR_CHILDREN_IDS = 'children'
ATTR_LOGICAL = 'logical'
ATTR_X = 'x'
ATTR_Y = 'y'
ATTR_W = 'w'
ATTR_H = 'h'
ATTR_TEXT_STATE = 'textState'
ATTR_FONT = 'font'
ATTR_SIZE = 'size'
