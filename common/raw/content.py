# -*- coding: utf-8 -*-

from marmot.common.content import Content
from marmot.common.label import LABEL_CHAR
from marmot.common.label import LABEL_IMAGE
from marmot.common.label import LABEL_PATH

class _RawContent(Content):
    """Raw content class.
    """
    def __init__(self, id, box, text='', font_id=None, font_size=None):
        """
        @param rid  Raw content id
        @param box  Bounding box of content
        """
        Content.__init__(self, id)
        self._box = box
        self._text = text
        self._font_id = font_id
        self._font_size = font_size

    @property
    def category(self):
        return 'raw'

    @property
    def box(self):
        return self._box

    @property
    def text(self):
        return self._text

    @property
    def font_id(self):
        return self._font_id

    @property
    def font_size(self):
        return self._font_size

class Char(_RawContent):
    @property
    def label(self):
        return LABEL_CHAR

class Image(_RawContent):
    @property
    def label(self):
        return LABEL_IMAGE

class Path(_RawContent):
    @property
    def label(self):
        return LABEL_PATH
