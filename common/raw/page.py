# -*- coding: utf-8 -*-

from marmot.common.page import Page as CommonPage

from lxml import etree
from marmot.common.rect import Rect
from marmot.common.raw.content import Char
from marmot.common.raw.content import Image
from marmot.common.raw.content import Path
from marmot.common.xmlname import *

class Page(CommonPage):
    """Raw page class.
    """

    def __init__(self, doc, page_num):
        CommonPage.__init__(self, page_num)
        self._doc = doc

    @classmethod
    def from_xml(cls, doc, xml):
        return read_page(doc, xml)

    @property
    def box(self):
        return self._box

    @box.setter
    def box(self, box):
        self._box = box

    @property
    def thumb(self):
        return self._thumb

    @thumb.setter
    def thumb(self, thumb):
        self._thumb = thumb

# functions to read page from an xml file
def read_page(doc, xml):
    tree = etree.parse(xml)
    root = tree.getroot()
    page_num = int(root.get(ATTR_PAGE_NUM))

    page = Page(doc, page_num)
    _read_page_box(root, page)
    _read_contents(root, doc, page)
    return page

def _read_page_box(root, page):
    page.box = _read_box(root[0])

def _read_contents(root, doc, page):
    elem2class = {ELEM_CHAR:    Char,
                  ELEM_IMAGE:   Image,
                  ELEM_PATH:    Path
                  }
    text_states = doc.text_states
    for node in root.iter(ELEM_CHAR, ELEM_IMAGE, ELEM_PATH):
        klass = elem2class[node.tag]
        id, box, text, text_state_index = _read_content(node)
        font_id = None
        font_size = None
        if not text_state_index is None:
            font_id, font_size = text_states[text_state_index]
        content = klass(id, box, text, font_id, font_size)
        page[content.id] = content

def _read_content(node):
    attributes = node.attrib
    id = attributes[ATTR_ID]
    box = _read_box(node[0])
    text = node.get(ATTR_CHAR, '')
    text = unicode(text)
    text_state_index = node.get(ATTR_TEXT_STATE, None)
    if not text_state_index is None:
        text_state_index = int(text_state_index)
    return (id, box, text, text_state_index)

def _read_box(node):
    attributes = node.attrib
    x = float(attributes[ATTR_X])
    y = float(attributes[ATTR_Y])
    w = float(attributes[ATTR_W])
    h = float(attributes[ATTR_H])
    return Rect(x, y, w, h)
