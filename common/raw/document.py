# -*- coding: utf-8 -*-

import os
import re
import collections
from lxml import etree

from marmot.common.xmlname import *
from marmot.common.raw.page import Page

xml_pattern = re.compile('(\d+)\.xml')
png_pattern = re.compile('(\d+)\.png')

# TODO: use pickle/shelv to handle memory usage
# TODO: use mixin/inheritence
class Document(collections.Mapping):
    """Raw document class.
    """
    # TODO: exception handling
    def __init__(self, dir): # , xmls, thumbs):
        self._dir = dir
        self._scan_dir()
        self._data = dict()
        self._text_states = read_text_states(
            os.path.join(dir, 'textstate.xml'))
        for key in self._xmls:
            self._data[key] = None

    def _scan_dir(self):
        self._xmls = {}
        self._pngs = {}
        for file_name in os.listdir(self._dir):
            m = xml_pattern.match(file_name)
            if m:
                self._xmls[int(m.group(1))] = file_name
            m = png_pattern.match(file_name)
            if m:
                self._pngs[int(m.group(1))] = file_name
        for key in self._xmls:
            assert key in self._pngs

    def __getitem__(self, key):
        # this may unnecessarily load page when testing contain
        if key not in self._data:
            raise KeyError

        if not self._data[key]:
            xml = os.path.join(self._dir, self._xmls[key])
            thumb = os.path.join(self._dir, self._pngs[key])
            page = Page.from_xml(self, xml)
            page.thumb = thumb
            self._data[key] = page

        return self._data[key]
            
    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    @property
    def page_count(self):
        return len(self._xmls)

    @property
    def text_states(self):
        return self._text_states

def read_text_states(path):
    tree = etree.parse(path)
    root = tree.getroot()

    text_states = dict()

    for node in root.iter(ELEM_TEXT_STATE):
        attributes = node.attrib
        id = int(attributes[ATTR_ID])
        font_id = int(attributes[ATTR_FONT])
        font_size = float(attributes[ATTR_SIZE])
        text_states[id] = (font_id, font_size)
    return text_states
