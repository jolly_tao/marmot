# -*- coding: utf-8 -*-
import sys
# print sys.path

from lxml import etree
from marmot.common.rect import Rect
from marmot.common.raw.page import Page
from marmot.common.raw.content import Char
from marmot.common.raw.content import Image
from marmot.common.raw.content import Path


"""common.raw.reader.read_page reads a raw page from an XML file.
"""

def read_page(path):
    tree = etree.parse(path)
    root = tree.getroot()
    page_num = int(root.get('pageNum'))
    
    page = Page(page_num)
    _read_page_box(root, page)
    _read_contents(root, page)
    return page

def _read_page_box(root, page):
    page.box = _read_box(root[0])

def _read_contents(root, page):
    for node in root.iter('char', 'image', 'path'):
        content = None
        if node.tag == 'char':
            content = _read_char(node)
        elif node.tag == 'image':
            content = _read_image(node)
        elif node.tag == 'path':
            content = _read_path(node)
        if content:
            page[content.id] = content

def _read_char(node):
    attributes = node.attrib
    id = attributes['id']
    box = _read_box(node[0])
    return Char(id, box)

def _read_image(node):
    attributes = node.attrib
    id = attributes['id']
    box = _read_box(node[0])
    return Image(id, box)

def _read_path(node):
    attributes = node.attrib
    id = attributes['id']
    box = _read_box(node[0])
    return Path(id, box)

def _read_box(node):
    attributes = node.attrib
    x = float(attributes['x'])
    y = float(attributes['y'])
    w = float(attributes['w'])
    h = float(attributes['h'])
    return Rect(x, y, w, h)

"""common.raw.reader.read_index reads a raw page index from an XML file.
"""

def read_index(path):
    tree = etree.parse(path)
    root = tree.getroot()

    xml_paths = dict()
    thumb_paths = dict()

    for node in root.iter('page'):
        attributes = node.attrib
        page_num = int(attributes['pageNum'])
        xml_paths[page_num] = attributes['xml']
        thumb_paths[page_num] = attributes['thumb']
    return (xml_paths, thumb_paths)
