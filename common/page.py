# -*- coding: utf-8 -*-

import collections

class Page(collections.MutableMapping):
    """ document class.
    """
    def __init__(self, page_num):
        self._data = dict()
        self._page_num = page_num

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __delitem__(self, key):
        del self._data[key]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    @property
    def page_num(self):
        return self._page_num
