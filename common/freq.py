# -*- coding: utf-8 -*-

from itertools import groupby
from operator import itemgetter

# This file provide useful functions

def most_common(lst):
    """Find most common element in a list.

    lst is a list of *hashable*.
    """
    return max(set(lst), key=lst.count)

def freq_dist(freqs):
    """Compute (key, freq) from freqs.

    freqs is *iterable* with form (key, freq), where key is *hashable*.
    The keys are unique in returned dist.
    """
    dist = dict()
    
    # make keys unique
    keys = [key for key, freq in freqs]
    unique_keys = set(keys)

    # accumulate freqs
    for key in unique_keys:
        dist.setdefault(key, 0)
    for key, freq in freqs:
        dist[key] += freq
    return dist

def most_freq(dist):
    """Find most frequent element and its frequency.

    Return (most_freq_key, most_freq)
    """
    return max(dist.iteritems(), key=itemgetter(1))
