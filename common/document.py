# -*- coding: utf-8 -*-

import os.path
from raw import Document as RawDoc
from physical import Document as PhyDoc

class Document(object):
    def __init__(self, dir):
        raw_dir = os.path.join(dir, 'raw')
        self.raw = RawDoc(raw_dir)

        physical_dir = os.path.join(dir, 'physical')
        if not os.path.isdir(physical_dir):
            os.makedirs(physical_dir)
        self.physical = PhyDoc(self.raw, physical_dir)
        
        self.doc_name = os.path.basename(dir)
