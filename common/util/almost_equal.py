# -*- coding: utf-8 -*-

def almost_equal(x, y, tol=None, rel=None):
    if tol is rel is None:
        raise TypeError('cannot specify both absolute and relative errors as None')
    tests = []
    if tol is not None:
        tests.append(tol)
    if rel is not None:
        tests.append(rel * abs(x))
    assert tests
    return abs(x - y) <= max(tests)
