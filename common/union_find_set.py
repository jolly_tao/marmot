# -*- coding: utf-8 -*-

class Node(object):
    def __init__(self, key):
        self.key = key
        self.parent = key
        self.rank = 0

class UnionFindSet(object):
    def __init__(self, keys):
        self._nodes = dict()
        for key in keys:
            self._nodes[key] = Node(key)

    def union(self, x, y):
        x_root = self.find(x)
        y_root = self.find(y)
        if x_root.rank > y_root.rank:
            y_root.parent = x_root.parent
        elif x_root.rank < y_root.rank:
            x_root.parent = y_root.parent
        else:
            y_root.parent = x_root.parent
            x_root.rank += 1

    def find(self, x):
        node = self._nodes[x]
        if node.parent == x:
            return node
        else:
            root = self.find(node.parent)
            node.parent = root.parent
            return root

    def sets(self):
        nodes = self._nodes.values()
        for node in nodes:
            self.find(node.key)
        roots = set(node.parent for node in nodes)
        sets = list()
        for r in roots:
            s = [node.key for node in nodes if node.parent == r]
            sets.append(s)
        return sets

if __name__ == '__main__':
    keys = range(10)
    union_find_set = UnionFindSet(keys)
    union_find_set.union(2, 3)
    assert(union_find_set.find(2) == union_find_set.find(3))
    assert(union_find_set.find(1) != union_find_set.find(5))
