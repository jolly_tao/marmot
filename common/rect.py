# -*- coding: utf-8 -*-

class Rect(object):
    """Rectangle class.
    """
    def __init__(self, x=0, y=0, w=0, h=0):
        """
        @param x    The less of left and right
        @param y    The less of top and bottm
        @param w    Width
        @param h    Height
        """
        self._x = x
        self._y = y
        self._w = w
        self._h = h

    def __str__(self):
        return '[x0: %.2f, y0: %.2f, x1: %.2f, y1: %.2f, w: %.2f, h: %.2f]' % \
               (self.x0, self.y0, self.x1, self.y1, self.width, self.height)
    @classmethod
    def from_point(cls, origin, opposite):
        """
        @param origin   A tuple (x0, y0)
        @param opposite A tuple (x1, y1)
        """
        x0, y0 = origin
        x1, y1 = opposite
        return cls(x0, y0, x1 - x0, y1 - y0)

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def width(self):
        return self._w

    @property
    def height(self):
        return self._h

    @property
    def x0(self):
        return self._x

    @property
    def y0(self):
        return self._y

    @property
    def x1(self):
        return self._x + self._w

    @property
    def y1(self):
        return self._y + self._h

    @property
    def area(self):
        return self._w * self._h

    @property
    def centroid(self):
        """
        @return (x_center, y_center)
        """
        return (self._x + self._w / 2,
                self._y + self._h / 2)

    def copy(self):
        return Rect(self._x, self._y, self._w, self._h)

    def unite(self, other):
        """
        @param other    The other rect object
        @return The union rect of self and the other.
        @remark This method returns a *new* rect object.
        """
        x0 = min(self.x, other.x)
        y0 = min(self.y, other.y)
        x1 = max(self.x + self.width, other.x + other.width)
        y1 = max(self.y + self.height, other.y + other.height)
        return Rect.from_point((x0, y0), (x1, y1))

    def intersect(self, other):
        """
        @param other    The other rect object
        @return The intersection rect of current rect and the other.
        @remark This method returns a *new* rect object.
        """
        x0 = max(self.x, other.x)
        y0 = max(self.y, other.y)
        x1 = min(self.x + self.width, other.x + other.width)
        y1 = min(self.y + self.height, other.y + other.height)
        return Rect.from_point((x0, y0), (x1, y1))

    def contain(self, other):
        """
        @param other    The other rect object
        @return True if current rect contains the other;
                False otherwise.
        """
        return (self.x <= other.x and
                self.y <= other.y and
                self.x + self.width >= other.x + other.width and
                self.y + self.height >= other.y + other.height)

    def overlap(self, other):
        """
        @param other    The other rect object
        @return True if current rect overlaps the other;
                False otherwise.
        """
        return (self.distance_x(other) < -1e-6 and
                self.distance_y(other) < -1e-6)

    def distance_x(self, other):
        """
        @param other    The other rect object
        @return distance along x axis.
        @remark If the rects overlap along x axis, distance_x will
                return a negative value, whose absolute value
                indicates how much they overlap along x axis;\n
                otherwise the distance is positive.
        """
        return (max(self.x, other.x) -
                min(self.x + self.width, other.x + other.width))

    def distance_y(self, other):
        """
        @param other    The other rect object
        @return distance along x axis.
        @remark If the rects overlap along y ayis, distance_y will
                return a negative value, whose absolute value
                indicates how much they overlap along y axis;\n
                otherwise the distance is positive.
        """
        return (max(self.y, other.y) -
                min(self.y + self.height, other.y + other.height))
