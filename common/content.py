# -*- coding: utf-8 -*-

from abc import ABCMeta
from abc import abstractproperty

class Content(object):
    """Abstract base class for content."""
    __metaclass__ = ABCMeta

    def __init__(self, id):
        self._id = id

    @property
    def id(self):
        return self._id

    @abstractproperty
    def category(self):
        pass

    @abstractproperty
    def label(self):
        pass

    @abstractproperty
    def text(self):
        pass
