# -*- coding: utf-8 -*-

from abc import ABCMeta
from abc import abstractproperty
from functools import reduce
from lazy import lazy

from marmot.common.content import Content
from marmot.common.label import LABEL_FRAGMENT
from marmot.common.label import LABEL_BLOCK
from marmot.common.label import LABEL_UNKOWN
from marmot.common.freq import most_common

class _PhysicalContent(Content):
    """Physical content class.
    """
    __metaclass__ = ABCMeta

    def __init__(self, id, container, children_ids):
        Content.__init__(self, id)
        self._container = container
        self._children_ids = children_ids

    @property
    def category(self):
        return 'physical'

    @property
    def children_ids(self):
        return self._children_ids

    @property
    def children(self):
        return [self._children_container[id] for id in self._children_ids]

    def sort(self, cmp=None, key=None, reverse=False):
        """
        Sort children ids based on children contents.
        cmp and key should be applied to content object.
        e.g., key=lambda content: content.box.width

        @param cmp      Compare function.
        @param key      Key function, specifying what to sort.
                        Should _never_ be None.
        @param reverse  Indicating whether to sort in reverse order.
        """
        self._children_ids.sort(cmp,
                                lambda x: key(self._children_container[x]),
                                reverse)
        lazy.invalidate(self, 'text')

    @abstractproperty
    def _children_container(self):
        pass

    @lazy
    def box(self):
        container = self._children_container
        boxes = (container[id].box for id in self._children_ids)
        return reduce(lambda lhs, rhs: lhs.unite(rhs), boxes)

    @lazy
    def text(self):
        container = self._children_container
        texts = (container[id].text for id in self._children_ids)
        return ''.join(texts)

class Fragment(_PhysicalContent):
    def __init__(self, id, container, children_ids,
                 logical_label=LABEL_UNKOWN):
        _PhysicalContent.__init__(self, id, container, children_ids)
        self._logical_label = logical_label

    @property
    def label(self):
        return LABEL_FRAGMENT

    @property
    def logical_label(self):
        return self._logical_label

    @logical_label.setter
    def logical_label(self, logical_label):
        self._logical_label = logical_label

    @lazy
    def child_label(self):
        children_labels = [child.label for child in self.children]
        return most_common(children_labels)

    @lazy
    def font_id(self):
        children_font_ids = [child.font_id for child in self.children]
        return most_common(children_font_ids)

    @property
    def font_size(self):
        children_font_sizes = [child.font_size for child in self.children]
        return most_common(children_font_sizes)
    
    @property
    def _children_container(self):
        return self._container.raw_page

class Block(_PhysicalContent):
    def __init__(self, id, container, children_ids):
        _PhysicalContent.__init__(self, id, container, children_ids)

    @property
    def label(self):
        return LABEL_BLOCK

    @property
    def _children_container(self):
        return self._container
