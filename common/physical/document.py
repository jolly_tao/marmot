# -*- coding: utf-8 -*-

import os
import re
import collections
from lxml import etree

from marmot.common.physical.page import Page
from marmot.common.xmlname import *

xml_pattern = re.compile('(\d+)\.xml')

# TODO: use pickle/shelv to handle memory usage
# TODO: use mixin/inheritence
class Document(collections.Mapping):
    """Physical document class.
    """
    def __init__(self, raw_doc, dir): # , xmls):
        self._raw_doc = raw_doc
        self._dir = dir
        self._scan_dir()
        self._data = dict()
        for key in self._raw_doc:
            self._data[key] = None

    def _scan_dir(self):
        self._xmls = {}
        for file_name in os.listdir(self._dir):
            m = xml_pattern.match(file_name)
            if m:
                self._xmls[int(m.group(1))] = file_name

    def __getitem__(self, key):
        if key not in self._data:
            raise KeyError

        if not self._data[key]:
        # if key not in self._data:
            if key in self._xmls:
                # read physical page from xml
                xml = os.path.join(self._dir, self._xmls[key])
                page = Page.from_xml(xml)
            else:
                # create an empty physical page
                page = Page(key)

            page.raw_page = self._raw_doc[key]
            self._data[key] = page
        return self._data[key]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __contains__(self, key):
        return key in self._raw_doc

    def flush(self, page_num):
        try:
            page = self[page_num]
            if page:
                self._xmls[page_num] = '%d.xml' % page_num
                xml = os.path.join(self._dir, self._xmls[page_num])
                page.to_xml(xml)
        except:
            pass

