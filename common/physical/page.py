# -*- coding: utf-8 -*-

import re
from lxml import etree
from itertools import ifilter

from marmot.common.page import Page as CommonPage
from marmot.common.label import LABEL_FRAGMENT
from marmot.common.label import LABEL_BLOCK
from marmot.common.label import LABEL_UNKOWN
from marmot.common.physical.content import Fragment
from marmot.common.physical.content import Block
from marmot.common.xmlname import *

class Page(CommonPage):
    """Physical page class.
    """
    def __init__(self, page_num):
        CommonPage.__init__(self, page_num)
        self._raw_page = None
        self._id_manager = IDManager(self.page_num)

    @classmethod
    def from_xml(cls, xml):
        return read_page(xml)

    def to_xml(self, xml):
        write_page(self, xml)

    @property
    def raw_page(self):
        return self._raw_page

    @raw_page.setter
    def raw_page(self, raw_page):
        self._raw_page = raw_page

    @property
    def id_manager(self):
        return self._id_manager

class IDManager(object):
    """IDManager allocates and reclaims ids.
    """
    pattern = re.compile('p\d+([fb])(\d+)')
    # need more?
    max_number = 1000

    def __init__(self, page_num):
        self._page_num = page_num
        self._records = {LABEL_FRAGMENT:   set(),
                         LABEL_BLOCK:      set()
                         }

    def allocate_id(self, label):
        prefix = 'f'
        if label == LABEL_BLOCK:
            prefix = 'b'
        record = self._records[label]
        number = 0
        for i in xrange(IDManager.max_number):
            if not i in record:
                number = i
                record.add(number)
                break
        return ''.join(['p', str(self._page_num), prefix, str(number)])

    def claim_id(self, id):
        # TODO: check collision
        label, number = self._parse(id)
        self._records[label].add(number)

    def reclaim_id(self, id):
        label, number = self._parse(id)
        self._records[label].remove(number)

    def _parse(self, id):
        match = IDManager.pattern.match(id)
        # TODO: when not matched, raise exception
        label = LABEL_FRAGMENT
        if match.group(1) == 'b':
            label = LABEL_BLOCK
        number = int(match.group(2))
        return (label, number)


# functions for xml writing

def write_page(page, xml):
    namespace_map = {'physical': NAMESPACE_PHYSICAL}
    physical_prefix = '{%s}' % NAMESPACE_PHYSICAL
    root = etree.Element(physical_prefix + ELEM_PAGE,
                         nsmap=namespace_map)
    root.set(ATTR_PAGE_NUM, str(page.page_num))
    _write_contents(page, root)
    tree = etree.ElementTree(root)
    tree.write(xml, xml_declaration=True, encoding='UTF-8',
               method='xml', pretty_print=True)

def _write_contents(page, root):
    contents = etree.SubElement(root, ELEM_CONTENTS)
    _write_content_group(page, contents, ELEM_FRAGMENT)
    _write_content_group(page, contents, ELEM_BLOCK)

def _write_content_group(page, parent, elem_tag):
    group = etree.SubElement(parent, '%ss' % elem_tag)
    # TODO: map element tag to label
    iter_contents = ifilter(lambda x: x.label == elem_tag,
                            page.itervalues())
    for content in iter_contents:
        node = etree.SubElement(group, elem_tag)
        _write_content(content, node)

def _write_content(content, node):
    node.set(ATTR_ID, content.id)
    node.set(ATTR_CHILDREN_IDS, ' '.join(content.children_ids))
    if content.label == LABEL_FRAGMENT:
        node.set(ATTR_LOGICAL, content.logical_label)
    box = etree.SubElement(node, ELEM_BOX)
    _write_box(content, box)

def _write_box(content, node):
    box = content.box
    node.set(ATTR_X, str(box.x))
    node.set(ATTR_Y, str(box.y))
    node.set(ATTR_W, str(box.width))
    node.set(ATTR_H, str(box.height))

# functions for xml reading
def read_page(xml):
    tree = etree.parse(xml)
    root = tree.getroot()
    page_num = int(root.get(ATTR_PAGE_NUM))

    page = Page(page_num)
    _read_contents(root, page)
    return page

def _read_contents(root, page):
    label2class = {LABEL_FRAGMENT:  Fragment,
                   LABEL_BLOCK:     Block
                   }
    for node in root.iter(LABEL_FRAGMENT, LABEL_BLOCK):
        if node.tag == LABEL_FRAGMENT:
            content = _read_fragment(node, page)
        else:
            content = _read_block(node, page)
        page[content.id] = content
        page.id_manager.claim_id(content.id)

def _read_fragment(node, page):
    id, children_ids = _read_content(node)
    fragment = Fragment(id, page, children_ids)
    logical_label = node.get(ATTR_LOGICAL)
    if logical_label:
        fragment.logical_label = logical_label
    return fragment

def _read_block(node, page):
    id, children_ids = _read_content(node)
    block = Block(id, page, children_ids)
    return block

def _read_content(node):
    attributes = node.attrib
    id = attributes[ATTR_ID]
    children_ids = attributes[ATTR_CHILDREN_IDS].split()
    return (id, children_ids)

def _read_box(node):
    attributes = node.attrib
    x = float(attributes[ATTR_X])
    y = float(attributes[ATTR_Y])
    w = float(attributes[ATTR_W])
    h = float(attributes[ATTR_H])
    return Rect(x, y, w, h)
