# -*- coding: utf-8 -*-
LABEL_CHAR = 'char'
LABEL_IMAGE = 'image'
LABEL_PATH = 'path'
LABEL_RAW = (LABEL_CHAR, LABEL_IMAGE, LABEL_PATH)

LABEL_FRAGMENT = 'fragment'
LABEL_BLOCK = 'block'
LABEL_PHYSICAL = (LABEL_FRAGMENT, LABEL_BLOCK)

LABEL_UNKOWN = 'unknown'
LABEL_BODY = 'body'
LABEL_TITLE = 'title'
LABEL_EQUATION = 'equation'
LABEL_FIGURE = 'figure'
LABEL_FIGURE_ANNOTATION = 'figure_annotation'
LABEL_FIGURE_CAPTION = 'figure_caption'
LABEL_FIGURE_CAPTION_CONT = 'figure_caption_cont'
LABEL_TABLE_CELL = 'table_cell'
LABEL_TABLE_CAPTION = 'table_caption'
LABEL_FOOTER = 'footer'
LABEL_HEADER = 'header'
LABEL_MARGINAL = 'marginal'
LABEL_LIST_ITEM = 'list_item'
LABEL_LIST_ITEM_CONT = 'list_item_cont'
LABEL_NOTE = 'note'
LABEL_PAGE_NUMBER = 'page_number'
LABEL_LOGICAL = (LABEL_BODY,
                 LABEL_TITLE,
                 LABEL_EQUATION,
                 LABEL_FIGURE,
                 LABEL_FIGURE_ANNOTATION,
                 LABEL_FIGURE_CAPTION,
                 LABEL_FIGURE_CAPTION_CONT,
                 LABEL_TABLE_CELL,
                 LABEL_TABLE_CAPTION,
                 LABEL_FOOTER,
                 LABEL_HEADER,
                 LABEL_MARGINAL,
                 LABEL_LIST_ITEM,
                 LABEL_LIST_ITEM_CONT,
                 LABEL_NOTE,
                 LABEL_PAGE_NUMBER,
                 )

LABEL_LOGICAL_COARSE = (LABEL_BODY,
                        LABEL_TITLE,
                        LABEL_EQUATION,
                        LABEL_FIGURE,
                        LABEL_FIGURE_ANNOTATION,
                        LABEL_FIGURE_CAPTION,
                        LABEL_TABLE_CELL,
                        LABEL_TABLE_CAPTION,
                        LABEL_FOOTER,
                        LABEL_HEADER,
                        LABEL_MARGINAL,
                        LABEL_LIST_ITEM,
                        LABEL_NOTE,
                        LABEL_PAGE_NUMBER
                        )

LABEL_LOGICAL_SELECTION = [LABEL_UNKOWN] + list(LABEL_LOGICAL)
LABEL_ALL = LABEL_RAW + LABEL_PHYSICAL + LABEL_LOGICAL
