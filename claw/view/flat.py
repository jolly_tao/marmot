# -*- coding: utf-8

import wx
import wx.lib.scrolledpanel as scrolledpanel
import traceback
import ConfigParser
from itertools import chain
from itertools import ifilter

from marmot.common.rect import Rect
from marmot.common.label import *
from marmot.claw.view.selector import Selector
from marmot.claw.view.renderer import ThumbRenderer
from marmot.claw.view.renderer import ContentRenderer
from marmot.claw.view.renderer import SelectionRenderer
from marmot.claw.view.renderer import PredictionRenderer
from marmot.claw.view.renderer import MSTreeRenderer
from marmot.claw.view.renderer import VerticalEdgeRenderer
from marmot.claw.view.renderer import OverlapEdgeRenderer
from marmot.claw.view.renderer import TextAlignEdgeRenderer
from marmot.claw.view.renderer import TextNonAlignEdgeRenderer
from marmot.claw.view.renderer import NonTextAboveTextEdgeRenderer
from marmot.claw.view.renderer import WhitespaceRender
from marmot.claw.view.renderer import LogicalLabelRenderer
from marmot.claw.view.renderer import ErrorLabelRenderer
from marmot.claw.data.menudata import FLAT_POPUP_MENU_ITEM_DATA
from marmot.claw.data.iddata import *

LAYER_THUMB = 'thumb'
LAYER_CONTENT = 'content'
LAYER_GT_LABEL = 'gt-label'
LAYER_SELECTION = 'selection'
LAYER_PREDICTION = 'prediction'
LAYER_MST = 'mst'
LAYER_COL = 'col'
LAYER_OVERLAP = 'overlap'
LAYER_TT_A = 'tt-a'
LAYER_TT_N = 'tt-n'
LAYER_NT_C = 'nt-c'
LAYER_WHITESPACE = 'whitespace'
LAYER_ERROR_LABEL = 'error-label'

RENDERER_CONSTRUCTOR = {
    LAYER_THUMB:        ThumbRenderer,
    LAYER_CONTENT:      ContentRenderer,
    LAYER_SELECTION:    SelectionRenderer,
    LAYER_PREDICTION:   PredictionRenderer,
    LAYER_MST:          MSTreeRenderer,
    LAYER_COL:          VerticalEdgeRenderer,
    LAYER_OVERLAP:      OverlapEdgeRenderer,
    LAYER_TT_A:         TextAlignEdgeRenderer,
    LAYER_TT_N:         TextNonAlignEdgeRenderer,
    LAYER_NT_C:         NonTextAboveTextEdgeRenderer,
    LAYER_WHITESPACE:   WhitespaceRender,
    LAYER_GT_LABEL:     LogicalLabelRenderer,
    LAYER_ERROR_LABEL:  ErrorLabelRenderer,
    }

class FlatView(scrolledpanel.ScrolledPanel):
    """View class displaying page on 2-d plane.
    Actually a scrollable wrapper around real page view.
    """

    def __init__(self, parent, frame):
        """
        @param  parent  Parent wx object
        """
        scrolledpanel.ScrolledPanel.__init__(self, parent)
        self._content_manager = None
        self._factor = 1.0
        self._old_size = (0, 0)

        # real page view
        self._page_view = PageView(self, frame)
        self._page_view.SetSize(self.GetClientSize())

        # set sizer
        vsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer.AddStretchSpacer()
        vsizer.Add(self._page_view, 0, wx.ALIGN_CENTER)
        vsizer.AddStretchSpacer()
        self.SetSizer(vsizer)

        self.SetAutoLayout(1)
        # make sure to set the parameters to false
        # or the page will be scrolled to top
        # when resizing or getting focus
        self.SetupScrolling(scrollToTop=False, scrollIntoView=False)

        self.Bind(wx.EVT_SIZE, self.OnSize)

    def OnSize(self, event):
        size = self.GetSizeTuple()
        if size == self._old_size:
            return
        self._old_size = size

        self._ResizePageView()

    def Invalidate(self):
        self._page_view.Invalidate()

    def ZoomIn(self):
        if self._factor < 2:
            self._factor += 0.2
            self._ResizePageView()

    def ZoomOut(self):
        if self._factor > 0.2:
            self._factor -= 0.2
            self._ResizePageView()

    def _UpdateScale(self):
        if not self._content_manager:
            return
        client_width, client_height = self.GetClientSizeTuple()
        page = self._content_manager.raw_page
        self._page_view.scale = client_width / page.box.width

    def _ResizePageView(self):
        client_width, client_height = self.GetClientSizeTuple()

        # if no doc is open, set page view in same size with client
        if not self._content_manager:
            self._page_view.SetMinSize((client_width, client_height))
        else:
            # if some doc is open, keep its aspect ratio,
            # and consider scaling factor
            page = self._content_manager.raw_page
            page_width = client_width * self._factor
            aspect_ratio = page.box.width / page.box.height
            page_height = page_width / aspect_ratio
            self._page_view.scale = page_width / page.box.width

            self._page_view.SetMinSize((page_width, page_height))
        self.SetupScrolling(scrollToTop=False, scrollIntoView=False)

    @property
    def content_manager(self):
        return self._content_manager

    @content_manager.setter
    def content_manager(self, content_manager):
        self._content_manager = content_manager
        self._page_view.content_manager = content_manager
        self._ResizePageView()

    @property
    def page_view(self):
        return self._page_view

class PageView(wx.Window):
    
    def __init__(self, parent, frame):
        wx.Window.__init__(self, parent)
        self._frame = frame
        self._scale = 1.0
        self._content_manager = None
        self._selector = Selector(self)
        self._dirty_flags = dict()
        self._buffers = dict()

        self._InitRenderer()
        self._InitLayerFlag()
        self._InitLabelFilter()
        self._InitBuffer()
        self._DrawBuffer()

        self._InitMenuEventMap()
        self._InitPopupMenu()

        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.OnErase)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnLeftDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftUp)
        self.Bind(wx.EVT_MOTION, self.OnMotion)
        self.Bind(wx.EVT_CONTEXT_MENU, self.OnPopupMenu)

    def _InitMenuEventMap(self):
        self._menu_event_map = {
            ID_EDIT_CREATE:     self.OnCreate,
            ID_EDIT_DELETE:     self.OnDelete,
            ID_EDIT_LABEL:      self.OnLabel,
            ID_EDIT_SORT_X:     self.OnSortX,
            ID_EDIT_SORT_Y:     self.OnSortY,
            ID_EDIT_UNDO:       self.OnUndo,
            ID_EDIT_REDO:       self.OnRedo
            }

    def _InitPopupMenu(self):
        self._popup_menu = wx.Menu()
        first = True
        for id, text, help, kind in FLAT_POPUP_MENU_ITEM_DATA:
            item = self._popup_menu.Append(id, text, help, kind)
            self.Bind(wx.EVT_MENU, self._menu_event_map[id], item)

    def _InitRenderer(self):
        self._renderers = dict()
        config = ConfigParser.ConfigParser()
        config.read('config/color.cfg')
        palette = dict()
        for section in config.sections():
            palette.update(config.items(section))

        for layer, constructor in RENDERER_CONSTRUCTOR.iteritems():
            self._renderers[layer] = constructor(self, palette)

    def _InitLayerFlag(self):
        self._layer_flags = dict()
        config = ConfigParser.ConfigParser()
        config.read('config/layer.cfg')
        for layer in config.options('display'):
            self._layer_flags[layer] = config.getboolean('display', layer)

    def _InitLabelFilter(self):
        self._label_filter = {}
        for label in LABEL_RAW:
            self._label_filter[label] = True
        for label in LABEL_PHYSICAL:
            self._label_filter[label] = False

    def _InitBuffer(self):
        """init buffer according to client size, and draw layers."""
        w, h = self.GetClientSizeTuple()
        self._client_buffer = wx.EmptyBitmap(w, h)

        for layer in self._layer_flags:
            self._buffers[layer] = wx.EmptyBitmap(w, h)
            self._dirty_flags[layer] = True

        self._need_reinit_buffer = False

    def _DrawBuffer(self):
        # white background
        dc = wx.BufferedDC(None, self._client_buffer)
        dc.SetBackground(wx.Brush('white', wx.SOLID))
        dc.Clear()

        for layer in self._layer_flags:
            if not self._layer_flags[layer]:
                continue
            layer_buffer = self._buffers[layer]
            layer_dc = wx.BufferedDC(None, layer_buffer)
            if self._dirty_flags[layer]:
                renderer = self._renderers[layer]
                renderer.update()
                renderer.render(layer_dc)
                self._dirty_flags[layer] = False
            if self._layer_flags[layer]:
                w, h = layer_dc.GetSizeTuple()
                dc.Blit(0, 0, w, h, layer_dc, 0, 0, wx.AND)

    def OnPaint(self, event):
        dc = wx.BufferedPaintDC(self, self._client_buffer)

    def OnSize(self, event):
        self.Invalidate()

    def OnIdle(self, event):
        if self._need_reinit_buffer:
            self._InitBuffer()

        if any(self._dirty_flags[layer] and self._layer_flags[layer]
               for layer in self._layer_flags):
        # if any(self._dirty_flags.itervalues()):
            self._DrawBuffer()
            self.Refresh()

    def OnErase(self, event):
        pass

    def OnLeftDown(self, event):
        if self._content_manager:
            self._x0, self._y0 = event.GetPosition()
            if not event.ControlDown():
                self._selector.clear()
        event.Skip()

    def OnLeftUp(self, event):
        if self._content_manager:
            self._selector.update_selection()
            self._selector.box = Rect(0, 0, 0, 0)
            self._dirty_flags[LAYER_SELECTION] = True
            self._frame.UpdateList()
        event.Skip()

    def OnMotion(self, event):
        if (not self._content_manager or
            not event.Dragging() or
            not event.LeftIsDown()):
            return

        # create selection box
        self._x1, self._y1 = event.GetPosition()
        x0 = min(self._x0, self._x1) / self._scale
        y0 = min(self._y0, self._y1) / self._scale
        x1 = max(self._x0, self._x1) / self._scale
        y1 = max(self._y0, self._y1) / self._scale
        box = Rect.from_point((x0, y0), (x1, y1))

        self._selector.box = box
        self._selector.update_delta()

        self._dirty_flags[LAYER_SELECTION] = True

    def OnCreate(self, event):
        self._frame.OnCreate(event)

    def OnDelete(self, event):
        self._frame.OnDelete(event)

    def OnLabel(self, event):
        self._frame.OnLabel(event)

    def OnSortX(self, event):
        self._frame.OnSortX(event)

    def OnSortY(self, event):
        self._frame.OnSortY(event)

    def OnUndo(self, event):
        self._frame.OnUndo(event)

    def OnRedo(self, event):
        self._frame.OnRedo(event)

    def OnPopupMenu(self, event):
        self.PopupMenu(self._popup_menu)

    def Invalidate(self):
        self._need_reinit_buffer = True

    @property
    def content_manager(self):
        return self._content_manager

    @content_manager.setter
    def content_manager(self, content_manager):
        self._content_manager = content_manager

    @property
    def label_filter(self):
        return self._label_filter

    @property
    def dirty_flags(self):
        return self._dirty_flags

    @property
    def layer_flags(self):
        return self._layer_flags

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, scale):
        self._scale = scale

    @property
    def selector(self):
        return self._selector

    def iter_visible_contents(self):
        if not self._content_manager:
            return []
        raw_page = self._content_manager.raw_page
        physical_page = self._content_manager.physical_page
        iter_filtered_raw = ifilter(lambda x: self._label_filter[x.label],
                                    raw_page.itervalues())
        filtered_physical = list(ifilter(lambda x: self._label_filter[x.label],
                                         physical_page.itervalues()))
        # warning: here using the assumption that
        # raw and physical contents share the same id space without collision
        children_ids = set(chain(*(x.children_ids for x in filtered_physical)))
        iter_filtered_physical = iter(filtered_physical)
        return ifilter(lambda x: x.id not in children_ids,
                       chain(iter_filtered_raw, iter_filtered_physical))
