# -*- coding: utf-8 -*-

import wx

class TreeView(wx.Panel):
    def __init__(self, parent, frame):
        wx.Panel.__init__(self, parent)
        self._frame = frame
        self._tree = wx.TreeCtrl(self)

        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnActivated)

    def OnSize(self, event):
        self._tree.SetSize(self.GetSize())

    def Invalidate(self):
        self._tree.DeleteAllItems()
        content_manager = self._frame.content_manager
        if not content_manager:
            return
        doc_name = content_manager.doc_name
        root_id = self._tree.AddRoot(doc_name)
        
        raw_doc = content_manager.doc.raw
        page_nums = list(sorted(raw_doc.keys()))
        for page_num in page_nums:
            self._tree.AppendItem(root_id, str(page_num))

    def OnActivated(self, event):
        page_num = int(self._tree.GetItemText(event.GetItem()))
        self._frame.TurnPage(page_num)
        
