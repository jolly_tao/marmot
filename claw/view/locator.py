# -*- coding: utf-8 -*-

import wx
import re
import traceback

class PageLocator(wx.TextCtrl):
    def __init__(self, parent, frame, id=wx.ID_ANY):
        self._frame = frame
        v = PageNumberValidator(frame)
        wx.TextCtrl.__init__(self, parent, id,
                             style=wx.TE_CENTER|wx.TE_PROCESS_ENTER,
                             validator=v)

        self.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnter)

    def Invalidate(self):
        content_manager = self._frame.content_manager
        if not content_manager:
            self.SetValue('0/0')
        else:
            text = '%d/%d' % (content_manager.page_num,
                              content_manager.page_count)
            self.SetValue(text)
        self.Refresh()

    def OnTextEnter(self, event):
        self.GetParent().Validate()

pattern = re.compile('\s*(\d+)\s*(/\s*\d+)*')

class PageNumberValidator(wx.PyValidator):
    """Page number validator."""
    def __init__(self, frame):
        wx.PyValidator.__init__(self)
        self._frame = frame

    def Clone(self):
        return PageNumberValidator(self._frame)

    def TransferToWindow(self):
        """Overridden to skip data transfer."""
        return True

    def TransferFromWindow(self):
        """Overridden to skip data transfer."""
        return True

    def Validate(self, win):
        textctrl = self.GetWindow()
        text = textctrl.GetValue()

        content_manager = self._frame.content_manager
        if not content_manager:
            # textctrl.SetValue('0 / 0')
            # textctrl.Refresh()
            return True

        match = pattern.match(text)
        if match:
            page_num = int(match.group(1))
            self._frame.TurnPage(page_num)
            # text = '%d/%d' % (content_manager.page_num,
            #                   content_manager.page_count)
            # textctrl.SetValue(text)

            textctrl.SetBackgroundColour('white')
            textctrl.Refresh()
            return True
        else:
            textctrl.SetBackgroundColour('pink')
            textctrl.Refresh()
            return False
