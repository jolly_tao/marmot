# -*- coding: utf-8 -*-

import os
import sys
import wx
import wx.lib.agw.aui as aui
import traceback
from itertools import ifilter
from rtree import index

from marmot.common.label import *
from marmot.common.union_find_set import UnionFindSet
from marmot.claw.data.menudata import MENU_BAR_DATA

from marmot.claw.data.iddata import *
from marmot.claw.model.content_manager import ContentManager
from marmot.claw.view.flat import FlatView
from marmot.claw.view.flat import LAYER_CONTENT
from marmot.claw.view.flat import LAYER_GT_LABEL
from marmot.claw.view.flat import LAYER_SELECTION
from marmot.claw.view.flat import LAYER_PREDICTION
from marmot.claw.view.flat import LAYER_MST
from marmot.claw.view.flat import LAYER_COL
from marmot.claw.view.flat import LAYER_OVERLAP
from marmot.claw.view.flat import LAYER_TT_A
from marmot.claw.view.flat import LAYER_TT_N
from marmot.claw.view.flat import LAYER_NT_C
from marmot.claw.view.flat import LAYER_WHITESPACE
from marmot.claw.view.flat import LAYER_ERROR_LABEL
from marmot.claw.view.locator import PageLocator

from marmot.claw.view.tree import TreeView
from marmot.claw.view.list import ListView

from marmot.claw.model.editor import MixedSelectionError
from marmot.claw.model.editor import EmptySelectionError
from marmot.claw.model.editor import UnmatchedCreateError
from marmot.claw.model.editor import DeleteRawError
from marmot.claw.model.editor import LabelNonFragmentError
from marmot.claw.model.editor import SortNonFragmentError

from marmot.brain.util import label_encoder

# TOOLBAR_FLAGS = (wx.TB_HORIZONTAL | wx.NO_BORDER | wx.TB_FLAT)

label2menuid = {
    LABEL_CHAR:     ID_FILTER_CHAR,
    LABEL_IMAGE:    ID_FILTER_IMAGE,
    LABEL_PATH:     ID_FILTER_PATH,
    LABEL_FRAGMENT: ID_FILTER_FRAGMENT,
    LABEL_BLOCK:    ID_FILTER_BLOCK
    }

menuid2label = dict((v, k) for (k, v) in label2menuid.iteritems())

layer2menuid = {
    LAYER_CONTENT:      ID_LAYER_CONTENT,
    LAYER_GT_LABEL:     ID_LAYER_GT_LABEL,
    LAYER_MST:          ID_LAYER_MST,
    LAYER_COL:          ID_LAYER_COL,
    LAYER_OVERLAP:      ID_LAYER_OVERLAP,
    LAYER_TT_A:         ID_LAYER_TT_A,
    LAYER_TT_N:         ID_LAYER_TT_N,
    LAYER_NT_C:         ID_LAYER_NT_C,
    LAYER_WHITESPACE:   ID_LAYER_WHITESPACE
    }

menuid2layer = dict((v, k) for (k, v) in layer2menuid.iteritems())

class ClawFrame(wx.Frame):
    """Frame of Claw application."""
    def __init__(self, parent=None, id=wx.ID_ANY, pos=wx.DefaultPosition,
                 title='Marmot Claw'):
        wx.Frame.__init__(self, parent, id, title, pos)

        self._default_title = title

        # panels
        flat_panel = wx.Panel(self, -1, size=(200, 150))
        tree_panel = wx.Panel(self, -1, size=(200, 150))
        list_panel = wx.Panel(self, -1, size=(200, 150))

        # views
        self._flat_view = FlatView(flat_panel, self)
        self._tree_view = TreeView(tree_panel, self)
        self._list_view = ListView(list_panel, self)
        self._content_manager = None
        self._edit_mode = False

        # forwarding
        self.page_view = self._flat_view.page_view

        # bind view to panel with sizer
        flat_sizer = wx.BoxSizer(wx.VERTICAL)
        flat_sizer.Add(self._flat_view, 1, wx.ALL|wx.EXPAND)
        flat_panel.SetSizer(flat_sizer)

        tree_sizer = wx.BoxSizer(wx.VERTICAL)
        tree_sizer.Add(self._tree_view, 1, wx.ALL|wx.EXPAND)
        tree_panel.SetSizer(tree_sizer)

        list_sizer = wx.BoxSizer(wx.VERTICAL)
        list_sizer.Add(self._list_view, 1, wx.ALL|wx.EXPAND)
        list_panel.SetSizer(list_sizer)

        self._aui_mgr = aui.AuiManager(self)
        self._aui_mgr.AddPane(flat_panel, aui.AuiPaneInfo().Center())
        self._aui_mgr.AddPane(tree_panel, aui.AuiPaneInfo().Left())
        self._aui_mgr.AddPane(list_panel, aui.AuiPaneInfo().Bottom())
        self._aui_mgr.Update()
        
        self._status_bar = self.CreateStatusBar()
        self._InitMenuEventMap()
        self._CreateMenuBar()
        self._CreateToolBar()
        # event

        self.Bind(wx.EVT_CLOSE, self.OnExit)

        self._aui_mgr.Update()

    def _CreateMenuBar(self):
        self._menu_bar = wx.MenuBar()
        for caption, item_data in MENU_BAR_DATA:
            self._menu_bar.Append(self._CreateMenu(item_data), caption)
        self.SetMenuBar(self._menu_bar)

    def _CreateMenu(self, item_data):
        menu = wx.Menu()
        for id, text, help, kind in item_data:
            item = menu.Append(id, text, help, kind)
            if not id == wx.ID_SEPARATOR:
                self.Bind(wx.EVT_MENU, self._menu_event_map[id], item)
        return menu

    def _CreateToolBar(self):
        from marmot.claw.data.toolbardata import TOOL_BAR_DATA
        toolbar = self.CreateToolBar()
        for id, bmp, help, kind in TOOL_BAR_DATA:
            tool = toolbar.AddLabelTool(id, '', bmp, shortHelp=help, kind=kind)
            if not id == wx.ID_SEPARATOR:
                self.Bind(wx.EVT_MENU, self._menu_event_map[id], tool)

        self._locator = PageLocator(toolbar, self)
        toolbar.AddControl(self._locator)

        toolbar.Realize()

    def _UpdateMenu(self):
        if not self._content_manager:
            return

        for label in label2menuid:
            item = self._menu_bar.FindItemById(label2menuid[label])
            if item:
                item.Check(self.page_view.label_filter[label])

        for layer in layer2menuid:
            item = self._menu_bar.FindItemById(layer2menuid[layer])
            if item:
                item.Check(self.page_view.layer_flags[layer])

    def _UpdateTitle(self):
        title = self._default_title
        if self._content_manager:
            title = self._content_manager.doc_name
            if self._content_manager.editor.is_dirty():
                title += '*'
        self.SetTitle(title)

    def _UpdateLocator(self):
        self._locator.Invalidate()

    def UpdateList(self):
        if not self._edit_mode:
            self._list_view.Invalidate()

    def _DoOpen(self):
        dialog = wx.DirDialog(self, "Choose a document directory:",
                              style=wx.DD_DEFAULT_STYLE
                              |wx.DD_DIR_MUST_EXIST)
        if dialog.ShowModal() != wx.ID_OK:
            return

        dir = dialog.GetPath()
        # if same document, do nothing
        if self._content_manager and dir == self._content_manager.dir:
            return

        # close existing document
        if not self._DoClose():
            return

        # open new document
        if os.path.isdir(os.path.join(dir, 'raw')):
            self._content_manager = ContentManager(dir)
            self.SetTitle(self._content_manager.doc_name)
            self._flat_view.content_manager = self._content_manager
            self._UpdateMenu()
            self._UpdateLocator()
            self._tree_view.Invalidate()
        else:
            wx.MessageBox('Not a proper directory. Please select again')
            self._DoOpen()

    def _DoLoad(self):
        dialog = wx.FileDialog(self, "Choose a txt file:",
                              style=wx.FD_DEFAULT_STYLE)
        if dialog.ShowModal() != wx.ID_OK:
            return

        path = dialog.GetPath()
        prediction = dict()
        with open(path) as f:
            for line in f:
                id, klass = line.split(' ')
                prediction[id] = label_encoder.inverse_transform(int(klass) - 1)
        self._content_manager.prediction = prediction
        self.page_view.dirty_flags[LAYER_PREDICTION] = True
        self.page_view.dirty_flags[LAYER_ERROR_LABEL] = True

    def _DoClose(self):
        if not self._CheckSaveAndProceed():
            return False

        self._content_manager = None
        self._flat_view.content_manager = None
        self._UpdateTitle()
        self._UpdateLocator()
        self._tree_view.Invalidate()
        return True

    def _DoSave(self):
        if self._content_manager:
            self._content_manager.save()
            self._content_manager.editor.save()
            self._UpdateTitle()

    def TurnPage(self, page_num):
        if not self._content_manager:
            return

        if page_num == self._content_manager.page_num:
            return

        if not self._CheckSaveAndProceed():
            return

        if self._content_manager.turn_page(page_num):
            self._flat_view.Invalidate()

        self._UpdateLocator()

    def OnOpen(self, event):
        self._DoOpen()
    
    def OnLoad(self, event):
        self._DoLoad()

    def OnClose(self, event):
        self._DoClose()

    def OnSave(self, event):
        self._DoSave()

    def OnExit(self, event):
        if self._content_manager:
            self.OnClose(event)

        self.Destroy()

    def OnPageDown(self, event):
        if not self._content_manager:
            return

        self.TurnPage(self._content_manager.page_num + 1)

    def OnPageUp(self, event):
        if not self._content_manager:
            return

        self.TurnPage(self._content_manager.page_num - 1)

    def OnZoomIn(self, event):
        self._flat_view.ZoomIn()

    def OnZoomOut(self, event):
        self._flat_view.ZoomOut()

    def OnFilter(self, event):
        label = menuid2label[event.GetId()]
        self.page_view.label_filter[label] = not self.page_view.label_filter[label]
        self.page_view.Invalidate()

    def OnLayer(self, event):
        layer = menuid2layer[event.GetId()]
        layer_flags = self.page_view.layer_flags
        layer_flags[layer] = not layer_flags[layer]
        self._flat_view.Invalidate()

    def OnCreate(self, event):
        if not self._content_manager:
            return

        dlg = wx.SingleChoiceDialog(None,
                                    'Which label are you going to use?',
                                    'Label Choice',
                                    list(LABEL_PHYSICAL))
        if dlg.ShowModal() != wx.ID_OK:
            return

        label = dlg.GetStringSelection()
        container = self._content_manager.physical_page
        children = self.page_view.selector.selection
        try:
            self._content_manager.editor.create(container, label, children)
        except MixedSelectionError:
            self._status_bar.SetStatusText('The contents in selection '
                                           'have different labels.')
        except EmptySelectionError:
            self._status_bar.SetStatusText('The selection is empty.')
        except UnmatchedCreateError:
            self._status_bar.SetStatusText('The selection label and '
                                           'target label are unmatched.')

        self._AfterEdit()

    def OnDelete(self, event):
        if not self._content_manager:
            return
        container = self._content_manager.physical_page
        contents = self.page_view.selector.selection
        try:
            self._content_manager.editor.delete(container, contents)
        except EmptySelectionError:
            self._status_bar.SetStatusText('The selection is empty.')
        except DeleteRawError:
            self._status_bar.SetStatusText('You can not delete raw.')

        self._AfterEdit()

    def OnLabel(self, event):
        if not self._content_manager:
            return
        dlg = wx.SingleChoiceDialog(None,
                                    'Which logical label are you going to use?',
                                    'Label Choice',
                                    list(LABEL_LOGICAL_SELECTION))
        if dlg.ShowModal() != wx.ID_OK:
            return

        label = dlg.GetStringSelection()
        contents = self.page_view.selector.selection
        try:
            self._content_manager.editor.label(contents, label)
        except EmptySelectionError:
            self._status_bar.SetStatusText('The selection is empty.')
        except LabelNonFragmentError:
            self._status_bar.SetStatusText('Please label *fragments*.')

        self._AfterEdit()

    def OnSortX(self, event):
        self._OnSort(0)

    def OnSortY(self, event):
        self._OnSort(1)

    def _OnSort(self, direction):
        if not self._content_manager:
            return

        contents = self.page_view.selector.selection
        try:
            self._content_manager.editor.sort(contents, direction)
        except EmptySelectionError:
            self._status_bar.SetStatusText('The selection is empty.')
        except SortNonFragmentError:
            self._status_bar.SetStatusText('Please sort *fragments*.')

        self._AfterEdit()


    def OnUndo(self, event):
        if not self._content_manager:
            return
        self._content_manager.editor.undo()
        self._AfterEdit()

    def OnRedo(self, event):
        if not self._content_manager:
            return
        self._content_manager.editor.redo()
        self._AfterEdit()

    def OnEdit(self, event):
        self._edit_mode = not self._edit_mode

    def OnInclude(self, event):
        if not self._content_manager:
            return
        self.page_view.selector.include_mode = True

    def OnOverlap(self, event):
        if not self._content_manager:
            return
        self.page_view.selector.include_mode = False

    def _AfterEdit(self):
        self._UpdateTitle()
        self.page_view.dirty_flags[LAYER_CONTENT] = True
        self.page_view.dirty_flags[LAYER_GT_LABEL] = True
        self.page_view.selector.clear()
        self.page_view.dirty_flags[LAYER_SELECTION] = True

    def _CheckSaveAndProceed(self):
        if not self._content_manager:
            return True
        if not self._content_manager.editor.is_dirty():
            return True

        ret = wx.MessageBox('Contents modified. Save and proceed?', '',
                            wx.YES_NO | wx.CANCEL | wx.ICON_QUESTION)
        if ret == wx.ID_CANCEL:
            return False
        elif ret == wx.ID_NO:
            return True
        else:
            self._DoSave()
            return True

    def OnLineUp(self, event):
        raw_page = self.content_manager.raw_page
        chars = [c for c in raw_page.itervalues() if c.label == LABEL_CHAR]
        uset = UnionFindSet(range(len(chars)))
        
        # insert boxes into indexer
        indexer = index.Index()
        for i, char in enumerate(chars):
            box = char.box
            indexer.insert(i, (box.x0, box.y0, box.x1, box.y1))
            
        # connect adjacent chars
        for i, char in enumerate(chars):
            box = char.box
            delta = 2 * char.font_size
            neighbors = list(indexer.intersection((box.x0 - delta, box.y0,
                                                   box.x1 + delta, box.y1)))
            for n in neighbors:
                uset.union(n, i)

        sets = uset.sets()
        container = self._content_manager.physical_page
        for s in sets:
            children = [chars[i] for i in s]
            self._content_manager.editor.create(container, LABEL_FRAGMENT, children)
        self._AfterEdit()

    def OnAbout(self, event):
        info = wx.AboutDialogInfo()
        # Make a template for the description
        desc = 'A ground-truthing tool'
        # Populate with information
        info.SetName('Marmot Claw')
        info.SetVersion('0.10')
        info.SetDevelopers(['Jolly (jolly.tao@pku.edu.cn)'])
        info.SetDescription(desc)
        # Create and show the dialog
        wx.AboutBox(info)


    def _InitMenuEventMap(self):
        self._menu_event_map = {
            ID_FILE_OPEN:        self.OnOpen,
            ID_FILE_LOADPREDICT: self.OnLoad,
            ID_FILE_CLOSE:       self.OnClose,
            ID_FILE_EXIT:        self.OnExit,
            ID_FILE_SAVE:        self.OnSave,
            ID_VIEW_PGDN:        self.OnPageDown,
            ID_VIEW_PGUP:        self.OnPageUp,
            ID_VIEW_ZOOMIN:      self.OnZoomIn,
            ID_VIEW_ZOOMOUT:     self.OnZoomOut,
            # ID_VIEW_TREE:       self.__OnTree,
            ID_EDIT_CREATE:      self.OnCreate,
            ID_EDIT_DELETE:      self.OnDelete,
            ID_EDIT_LABEL:       self.OnLabel,
            ID_EDIT_SORT_X:      self.OnSortX,
            ID_EDIT_SORT_Y:      self.OnSortY,
            ID_EDIT_UNDO:        self.OnUndo,
            ID_EDIT_REDO:        self.OnRedo,
            ID_EDIT_INCLUDE:     self.OnInclude,
            ID_EDIT_OVERLAP:     self.OnOverlap,
            ID_EDIT_EDIT:        self.OnEdit,
            ID_EDIT_LINEUP:      self.OnLineUp,
            ID_FILTER_CHAR:      self.OnFilter,
            ID_FILTER_IMAGE:     self.OnFilter,
            ID_FILTER_PATH:      self.OnFilter,
            ID_FILTER_FRAGMENT:  self.OnFilter,
            ID_FILTER_BLOCK:     self.OnFilter,
            ID_LAYER_CONTENT:    self.OnLayer,
            ID_LAYER_GT_LABEL:   self.OnLayer,
            ID_LAYER_MST:        self.OnLayer,
            ID_LAYER_COL:        self.OnLayer,
            ID_LAYER_OVERLAP:    self.OnLayer,
            ID_LAYER_TT_A:       self.OnLayer,
            ID_LAYER_TT_N:       self.OnLayer,
            ID_LAYER_NT_C:       self.OnLayer,
            ID_LAYER_WHITESPACE: self.OnLayer,
            wx.ID_ABOUT:         self.OnAbout
            }

    @property
    def content_manager(self):
        return self._content_manager

