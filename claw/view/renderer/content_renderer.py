"""
Renderers that draw content boxes.
"""

from abc import ABCMeta
from abc import abstractmethod
import wx
from renderer import Renderer

from marmot.common.label import LABEL_FRAGMENT
from marmot.brain.feature.context import Context

class _ContentRenderer(Renderer):
    """Abstract base class used to render contents.
    """
    __metaclass__ = ABCMeta

    def __init__(self, view):
        Renderer.__init__(self, view)

    @abstractmethod
    def set_pen(self, pen, label, palette):
        pass

    @abstractmethod
    def set_brush(self, brush, label, palette):
        pass

    def draw_text(self, dc, text, x, y, scale):
        dc.DrawRotatedText(text, x*scale, y*scale, 15)

    def draw_contents(self, dc, contents, scale, palette, pen, brush):
        for content in contents:
            label = content.label
            box = content.box
            self.set_pen(pen, label, palette)
            self.set_brush(brush, label, palette)
            dc.SetPen(pen)
            dc.SetBrush(brush)
            self.draw_rect(dc, box, scale)

    def draw_logical_labels(self, dc, points, logical_labels, scale, color):
        dc.SetFont(wx.Font(20, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(color)
        
        for point, logical_label in zip(points, logical_labels):
            x, y = point
            self.draw_text(dc, logical_label, x, y, scale)

class ContentRenderer(_ContentRenderer):
    def __init__(self, view, palette):
        """
        @param palette  Dict, {label: color}
        """
        _ContentRenderer.__init__(self, view)
        self._palette = palette

    def set_pen(self, pen, label, palette):
        pen.SetColour(palette[label])

    def set_brush(self, brush, label, palette):
        pass

    def render_foreground(self, dc):
        """Draw raw contents onto target dc.
        """
        # if not self._page:
        if not self._view.content_manager:
            return

        gcdc = wx.GCDC(dc)

        # default pen
        pen = wx.Pen('black')
        pen.SetStyle(wx.SOLID)
        pen.SetWidth(2)

        # default brush
        brush = wx.Brush('white', style=wx.TRANSPARENT)

        # draw visible raw/physical contents
        contents = list(self._view.iter_visible_contents())

        scale = self._view.scale
        self.draw_contents(gcdc, contents, scale, self._palette, pen, brush)

        # draw logical labels of visible fragments
        # fragments = [content for content in contents
        #              if content.label == LABEL_FRAGMENT]
        # points = ((fragment.box.x, fragment.box.y) for fragment in fragments)
        # logical_labels = (fragment.logical_label for fragment in fragments)
        # self.draw_logical_labels(gcdc, points, logical_labels, scale, 'NAVY')

    def update(self):
        pass

class PredictionRenderer(_ContentRenderer):
    def __init__(self, view, palette):
        """
        @param palette  Dict, {label: color}
        """
        _ContentRenderer.__init__(self, view)
        self._palette = palette
        self._predict_error = dict()

    def set_pen(self, pen, label, palette):
        pen.SetColour(palette[label])

    def set_brush(self, brush, label, palette):
        pass

    def render_foreground(self, dc):
        """Draw raw contents onto target dc.
        """
        # if not self._page:
        if not self._view.content_manager:
            return

        gcdc = wx.GCDC(dc)

        # default pen
        pen = wx.Pen('black')
        pen.SetStyle(wx.SOLID)
        pen.SetWidth(2)

        # default brush
        brush = wx.Brush('white', style=wx.TRANSPARENT)
        scale = self._view.scale
        self.draw_contents(gcdc, self._contents, scale, self._palette, pen, brush)

        # # draw logical labels of visible fragments
        # points = ((fragment.box.x, fragment.box.y)
        #           for fragment in self._error_contents)
        # self.draw_logical_labels(gcdc, points, self._error_logical_labels,
        #                          scale, 'RED')

    def update(self):
        if self._view.content_manager:
            prediction = self._view.content_manager.prediction
            page = self._view.content_manager.physical_page
            self._contents = [page[id] for (id, logical_label) 
                              in prediction.iteritems()
                              if page[id].logical_label != logical_label]
        pass
        # if self._view.content_manager:
        #     prediction = self._view.content_manager.prediction
        #     page = self._view.content_manager.physical_page
        #     self._error_contents = [page[id] for (id, logical_label)
        #                             in prediction.iteritems()
        #                             if page[id].logical_label != logical_label]
        #     self._error_logical_labels = [prediction[content.id] for content
        #                                   in self._error_contents]
        
class SelectionRenderer(_ContentRenderer):
    def __init__(self, view, palette):
        """
        @param palette  Dict, {label: color}
        """
        _ContentRenderer.__init__(self, view)
        self._palette = palette
        self._selector = self._view.selector

    def set_pen(self, pen, label, palette):
        pass

    def set_brush(self, brush, label, palette):
        brush.SetColour(palette[label])

    def render_foreground(self, dc):
        if self._selector == None:
            return

        gcdc = wx.GCDC(dc)

        pen = wx.Pen('black')
        brush = wx.Brush('white', style=wx.TRANSPARENT)

        scale = self._view.scale

        # draw selection box
        pen.SetStyle(wx.DOT)
        gcdc.SetPen(pen)
        gcdc.SetBrush(brush)
        self.draw_rect(gcdc, self._selector.box, scale)

        # draw selection contents
        pen.SetStyle(wx.TRANSPARENT)
        brush.SetStyle(wx.SOLID)
        self.draw_contents(gcdc, self._selector.selection, scale,
                          self._palette, pen, brush)
        self.draw_contents(gcdc, self._selector.delta, scale,
                          self._palette, pen, brush)

    def update(self):
        pass
