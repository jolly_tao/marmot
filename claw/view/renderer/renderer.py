# -*- coding: utf-8 -*-

from abc import ABCMeta
from abc import abstractmethod
import wx

class Renderer(object):
    """Abstract base class used to render.
    """
    __metaclass__ = ABCMeta

    def __init__(self, view):
        self._view = view

    def render(self, dc):
        """How to render."""
        # background
        dc.SetBackground(wx.Brush('white', wx.SOLID))
        dc.Clear()

        # foreground
        self.render_foreground(dc)

    @abstractmethod
    def render_foreground(self, dc):
        pass

    @abstractmethod
    def update(self):
        """How to update data to render."""
        pass

    def draw_rect(self, dc, rect, scale):
        if rect.width == 0  or rect.height == 0:
            dc.DrawLine(rect.x * scale,
                        rect.y * scale,
                        (rect.x + rect.width) * scale,
                        (rect.y + rect.height) * scale)
        else:
            dc.DrawRectangle(rect.x * scale,
                             rect.y * scale,
                             rect.width * scale,
                             rect.height * scale)

    def draw_line(self, dc, pt0, pt1, scale):
        x0, y0 = pt0
        x1, y1 = pt1
        dc.DrawLine(x0 * scale, y0 * scale,
                    x1 * scale, y1 * scale)

    def draw_point(self, dc, pt, scale):
        x, y = pt
        dc.DrawPoint(x * scale, y * scale)
