import wx
from renderer import Renderer

class ThumbRenderer(Renderer):
    def __init__(self, view, palette):
        Renderer.__init__(self, view)
        self._thumb = None

    def render_foreground(self, dc):
        """Fill the target dc with current thumb.
        Make sure dc and thumb have the same aspect ratio.
        """
        if not self._thumb:
            return

        # wxpython 2.8 doesn't have DC.StretchBlit() method
        # scale manually
        old_x_scale, old_y_scale = dc.GetUserScale()

        # calculate scale factor
        tw, th = self._thumb.GetSize()
        w, h = dc.GetSizeTuple()
        scale = float(w) / tw

        # draw
        dc.SetUserScale(scale, scale)
        dc.DrawBitmap(self._thumb, 0, 0)

        # recover
        dc.SetUserScale(old_x_scale, old_y_scale)

    def update(self):
        if self._view.content_manager:
            self._thumb = self._view.content_manager.thumb
        else:
            self._thumb = None

