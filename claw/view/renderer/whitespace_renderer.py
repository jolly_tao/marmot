import wx
from renderer import Renderer

class WhitespaceRender(Renderer):
    def __init__(self, view, palette):
        Renderer.__init__(self, view)
        self._color = palette['whitespace']
        self._whitespaces = []
        self._scale = 1.0

    def render_foreground(self, dc):
        gcdc = wx.GCDC(dc)
        brush = wx.Brush(self._color)
        brush.SetStyle(wx.SOLID)
        gcdc.SetBrush(brush)
        for ws in self._whitespaces:
            self.draw_rect(gcdc, ws, self._scale)

    def update(self):
        if self._view.content_manager:
            self._whitespaces = self._view.content_manager.context.whitespaces
            self._scale = self._view.scale
