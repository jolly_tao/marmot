import wx
from itertools import chain
from renderer import Renderer

class GraphRenderer(Renderer):
    def __init__(self, view, palette):
        Renderer.__init__(self, view)
        self._color = palette[self.name()]
        self._points = []
        self._edges = []

    def render_foreground(self, dc):
        gcdc = wx.GCDC(dc)
        pen = wx.Pen('black')
        pen.SetStyle(wx.SOLID)
        pen.SetWidth(10)
        pen.SetColour(self._color)
        gcdc.SetPen(pen)

        point_indices = set(chain(*self._edges))
        for i in point_indices:
            self.draw_point(gcdc, self._points[i], self._scale)

        pen.SetWidth(3)
        gcdc.SetPen(pen)
        for (v0, v1) in self._edges:
            pt0, pt1 = self._points[v0], self._points[v1]
            self.draw_line(gcdc, pt0, pt1, self._scale)

    def update(self):
        if self._view.content_manager:
            context = self._view.content_manager.context
            boxes = (d.box for d in context.descriptors)
            self._points = [box.centroid for box in boxes]
            self._edges = self.edges(context)
            self._scale = self._view.scale

    def name(self):
        """Override this function to set correct name."""
        return 'graph'

    def edges(self, context):
        """Override this function to get desired edges."""
        return []

class MSTreeRenderer(GraphRenderer):
    def __init__(self, view, palette):
        GraphRenderer.__init__(self, view, palette)

    def name(self):
        return 'mst'

    def edges(self, context):
        return context.mst_graph

class VerticalEdgeRenderer(GraphRenderer):
    def __init__(self, view, palette):
        GraphRenderer.__init__(self, view, palette)

    def name(self):
        return 'col'

    def edges(self, context):
        return context.column_digraph.edges()

class OverlapEdgeRenderer(GraphRenderer):
    def __init__(self, view, palette):
        GraphRenderer.__init__(self, view, palette)

    def name(self):
        return 'overlap'
    
    def edges(self, context):
        return context.overlap_graph

class TextAlignEdgeRenderer(GraphRenderer):
    def __init__(self, view, palette):
        GraphRenderer.__init__(self, view, palette)

    def name(self):
        return 'tt-a'
    
    def edges(self, context):
        return context.text_align_graph

class TextNonAlignEdgeRenderer(GraphRenderer):
    def __init__(self, view, palette):
        GraphRenderer.__init__(self, view, palette)

    def name(self):
        return 'tt-n'
    
    def edges(self, context):
        return context.text_non_align_graph

class NonTextAboveTextEdgeRenderer(GraphRenderer):
    def __init__(self, view, palette):
        GraphRenderer.__init__(self, view, palette)

    def name(self):
        return 'nt-c'
    
    def edges(self, context):
        return context.non_text_above_text_graph
