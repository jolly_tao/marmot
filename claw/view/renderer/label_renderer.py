from abc import ABCMeta
from abc import abstractmethod
from renderer import Renderer
import wx

from marmot.common.label import LABEL_FRAGMENT

class _LabelRenderer(Renderer):
    """Abstract base class used to render contents.
    """
    __metaclass__ = ABCMeta

    def __init__(self, view, palette):
        Renderer.__init__(self, view)
        self._color = palette[self.name()]

    def draw_text(self, dc, text, x, y, scale):
        dc.DrawRotatedText(text, x*scale, y*scale, 15)

    def draw_labels(self, dc, points, labels, scale, color):
        dc.SetFont(wx.Font(20, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(color)
        
        for point, label in zip(points, labels):
            x, y = point
            self.draw_text(dc, label, x, y, scale)

    def render_foreground(self, dc):
        """Draw content logical labels target dc.
        """
        gcdc = wx.GCDC(dc)
        scale = self._view.scale

        # draw logical labels of visible fragments
        self.draw_labels(gcdc, self._points, self._labels, scale, self._color)

    def name(self):
        """Override this to get correct name"""
        return 'label'

class LogicalLabelRenderer(_LabelRenderer):
    def __init__(self, view, palette):
        _LabelRenderer.__init__(self, view, palette)

    def name(self):
        return 'gt-label'

    def update(self):
        fragments = [c for c in self._view.iter_visible_contents()
                     if c.label == LABEL_FRAGMENT]
        self._points = ((f.box.x, f.box.y) for f in fragments)
        self._labels = (f.logical_label for f in fragments)

class ErrorLabelRenderer(_LabelRenderer):
    def __init__(self, view, palette):
        _LabelRenderer.__init__(self, view, palette)
        self._points = []
        self._labels = []

    def name(self):
        return 'error-label'
    
    def update(self):
        if self._view.content_manager:
            prediction = self._view.content_manager.prediction
            page = self._view.content_manager.physical_page
            error_contents = [page[id] for (id, logical_label)
                              in prediction.iteritems()
                              if page[id].logical_label != logical_label]
            self._points = ((c.box.x, c.box.y) for c in error_contents)
            self._labels = (prediction[c.id] for c in error_contents)
            
