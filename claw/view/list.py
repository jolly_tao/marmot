# -*- coding: utf-8 -*-

import wx
import sys
from operator import itemgetter

class ListView(wx.Panel):
    def __init__(self, parent, frame):
        wx.Panel.__init__(self, parent)
        self._frame = frame
        
        self._geometric = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self._geometric.InsertColumn(0, 'geometric')
        self._geometric.InsertColumn(1, 'value')
        
        self._textual = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self._textual.InsertColumn(0, 'textual')
        self._textual.InsertColumn(1, 'value')
        
        self._typesetting = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self._typesetting.InsertColumn(0, 'typesetting')
        self._typesetting.InsertColumn(1, 'value')

        self._local_context = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self._local_context.InsertColumn(0, 'local contest')
        self._local_context.InsertColumn(1, 'value')

        sizer = wx.GridSizer(rows=1, cols=4)
        sizer.Add(self._geometric, border=1, flag=wx.ALL|wx.EXPAND)
        sizer.Add(self._textual, border=1, flag=wx.ALL|wx.EXPAND)
        sizer.Add(self._typesetting, flag=wx.ALL|wx.EXPAND)
        sizer.Add(self._local_context, flag=wx.ALL|wx.EXPAND)
        self.SetSizer(sizer)
        self.Fit()
        # self._list = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        # self._list.InsertColumn(0, 'feature')
        # self._list.InsertColumn(1, 'value')

        # self.Bind(wx.EVT_SIZE, self.OnSize)

    # def OnSize(self, event):
    #     # self._list.SetSize(self.GetSize())
    #     pass

    def _UpdateFeatureName(self, ctrl, features):
        nr_items = ctrl.GetItemCount()
        if not nr_items:
            get_name = itemgetter(0)
            for item in features:
                ctrl.InsertStringItem(sys.maxint, get_name(item))

    def _SetFeatureValue(self, ctrl, features):
        get_value = itemgetter(1)
        for i, item in enumerate(features):
            ctrl.SetStringItem(i, 1, unicode(get_value(item)))

    def _ClearFeatureValue(self, ctrl):
        nr_items = ctrl.GetItemCount()
        for i in range(nr_items):
            ctrl.SetStringItem(i, 1, '')
            
    def Invalidate(self):
        selection = self._frame.page_view.selector.selection
        if len(selection) != 1:
            self._ClearFeatureValue(self._geometric)
            self._ClearFeatureValue(self._textual)
            self._ClearFeatureValue(self._typesetting)
        else:
            content = list(selection)[0]
            descriptor = self._frame.content_manager.context.by_id(content.id)

            self._UpdateFeatureName(self._geometric, descriptor.geometric)
            self._UpdateFeatureName(self._textual, descriptor.textual)
            self._UpdateFeatureName(self._typesetting, descriptor.typesetting)
            self._UpdateFeatureName(self._local_context, descriptor.local_context)
            # if not nr_items:
            #     get_name = itemgetter(0)
            #     for item in descriptor.feature_list:
            #         self._list.InsertStringItem(sys.maxint, get_name(item))

            self._SetFeatureValue(self._geometric, descriptor.geometric)
            self._SetFeatureValue(self._textual, descriptor.textual)
            self._SetFeatureValue(self._typesetting, descriptor.typesetting)
            self._SetFeatureValue(self._local_context, descriptor.local_context)
