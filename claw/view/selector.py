# -*- coding: utf-8 -*-

from marmot.common.rect import Rect

class Selector(object):
    def __init__(self, view):
        self._view = view
        self._box = Rect(0, 0, 0, 0)
        self._include_mode = True
        self._delta = []
        self._selection = set()

    @property
    def box(self):
        return self._box

    @box.setter
    def box(self, box):
        self._box = box

    def update_selection(self):
        self._selection.update(self._delta)
        del self._delta[:]

    def update_delta(self):
        # visible and inside/overlap selection box
        if self._include_mode:
            pred = lambda lhs, rhs: lhs.contain(rhs)
        else:
            pred = lambda lhs, rhs: lhs.overlap(rhs)
        contents = self._view.iter_visible_contents()
        self._delta = [content for content in contents
                       if (pred(self._box, content.box))]

    @property
    def selection(self):
        return self._selection

    @property
    def delta(self):
        return self._delta

    def clear(self):
        self._box = Rect(0, 0, 0, 0)
        self._selection.clear()

    @property
    def include_mode(self):
        return self._include_mode

    @include_mode.setter
    def include_mode(self, mode):
        self._include_mode = mode
