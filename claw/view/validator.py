# -*- coding: utf-8 -*-

import wx
import re

pattern = re.compile('\s*(\d+)\s*(/\s*\d+)*')

class PageNumberValidator(wx.PyValidator):
    """Page number validator."""
    def __init__(self, frame):
        wx.PyValidator.__init__(self)
        self._frame = frame
        self.Bind(wx.EVT_TEXT_ENTER, self.Validate)
        self.Bind(wx.EVT_CHAR, self.OnChar)

    def Clone(self):
        return PageNumberValidator(self._frame)

    def TransferToWindow(self):
        """Overridden to skip data transfer."""
        return True

    def TransferFromWindow(self):
        """Overridden to skip data transfer."""
        return True

    def OnChar(self, event):
        event.Skip()

    def Validate(self, win):
        textctrl = self.GetWindow()
        text = textctrl.GetValue()

        content_manager = self._frame.content_manager
        if not content_manager:
            textctrl.SetValue('0 / 0')
            textctrl.Refresh()
            return True

        match = pattern.match(text)
        if match:
            page_num = int(text)
            self._frame.TurnPage(page_num)
            text = '%d / %d' % (content_manager.page_num,
                                content_manager.page_count)
            textctrl.SetValue(text)

            textctrl.SetBackgroundColour('white')
            textctrl.Refresh()
            return True
        else:
            textctrl.SetBackgroundColour('pink')
            textctrl.Refresh()
            return False
