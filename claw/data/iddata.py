# -*- coding: utf-8 -*-

import wx

#------------------------------------------------------------
# menu item id
ID_SEPERATOR = wx.NewId()

ID_FILE_OPEN = wx.NewId()
ID_FILE_LOADPREDICT = wx.NewId()
ID_FILE_SAVE = wx.NewId()
ID_FILE_CLOSE = wx.NewId()
ID_FILE_EXIT = wx.NewId()

ID_VIEW_PGDN = wx.NewId()
ID_VIEW_PGUP = wx.NewId()
ID_VIEW_ZOOMIN = wx.NewId()
ID_VIEW_ZOOMOUT = wx.NewId()
#ID_VIEW_TREE = wx.NewId()

ID_EDIT_CREATE = wx.NewId()
ID_EDIT_DELETE = wx.NewId()
ID_EDIT_LABEL = wx.NewId()
ID_EDIT_SORT_X = wx.NewId()
ID_EDIT_SORT_Y = wx.NewId()
ID_EDIT_UNDO = wx.NewId()
ID_EDIT_REDO = wx.NewId()
ID_EDIT_LINEUP = wx.NewId()
ID_EDIT_HOME = wx.NewId()
ID_EDIT_END = wx.NewId()
ID_EDIT_NEXT = wx.NewId()
ID_EDIT_PREV = wx.NewId()
ID_EDIT_OVERLAP = wx.NewId()
ID_EDIT_INCLUDE = wx.NewId()
ID_EDIT_EDIT = wx.NewId()
ID_EDIT_MOVEUP = wx.NewId()
ID_EDIT_MOVEDOWN = wx.NewId()
ID_EDIT_SORT_X = wx.NewId()
ID_EDIT_SORT_Y = wx.NewId()
ID_EDIT_PROPERTY = wx.NewId()

ID_FILTER_CHAR = wx.NewId()
ID_FILTER_IMAGE = wx.NewId()
ID_FILTER_PATH = wx.NewId()
ID_FILTER_FRAGMENT = wx.NewId()
ID_FILTER_BLOCK = wx.NewId()

ID_LAYER_CONTENT = wx.NewId()
ID_LAYER_GT_LABEL = wx.NewId()
ID_LAYER_MST = wx.NewId()
ID_LAYER_COL = wx.NewId()
ID_LAYER_OVERLAP = wx.NewId()
ID_LAYER_TT_A = wx.NewId()
ID_LAYER_TT_N = wx.NewId()
ID_LAYER_NT_C = wx.NewId()
ID_LAYER_WHITESPACE = wx.NewId()
