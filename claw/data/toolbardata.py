# -*- coding: utf-8 -*-

import os
import wx
from marmot.claw.data.iddata import *

############################################################
# tool bitmaps
BITMAP_SIZE = (16, 16)
ICON_DIR = os.path.join('data', 'icon')

# file tool
BITMAP_OPEN = wx.ArtProvider.GetBitmap(
    wx.ART_FILE_OPEN, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_SAVE = wx.ArtProvider.GetBitmap(
    wx.ART_FILE_SAVE, wx.ART_TOOLBAR, BITMAP_SIZE)

# view tool
BITMAP_PGUP = wx.ArtProvider.GetBitmap(
    wx.ART_GO_UP, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_PGDN = wx.ArtProvider.GetBitmap(
    wx.ART_GO_DOWN, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_ZOOMIN = wx.Bitmap(
    os.path.join(ICON_DIR, 'zoomin.bmp'), wx.BITMAP_TYPE_BMP)
BITMAP_ZOOMOUT = wx.Bitmap(
    os.path.join(ICON_DIR, 'zoomout.bmp'), wx.BITMAP_TYPE_BMP)

# edit tool
BITMAP_CREATE = wx.ArtProvider.GetBitmap(
    wx.ART_ADD_BOOKMARK, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_DELETE = wx.ArtProvider.GetBitmap(
    wx.ART_DEL_BOOKMARK, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_UNDO = wx.ArtProvider.GetBitmap(
    wx.ART_UNDO, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_REDO = wx.ArtProvider.GetBitmap(
    wx.ART_REDO, wx.ART_TOOLBAR, BITMAP_SIZE)
BITMAP_INCLUDE = wx.Bitmap(
    os.path.join(ICON_DIR, 'include.bmp'), wx.BITMAP_TYPE_BMP)
BITMAP_OVERLAP = wx.Bitmap(
    os.path.join(ICON_DIR, 'overlap.bmp'), wx.BITMAP_TYPE_BMP)
BITMAP_EDIT = wx.ArtProvider.GetBitmap(
    wx.ART_EXECUTABLE_FILE, wx.ART_TOOLBAR, BITMAP_SIZE)

############################################################
# tool
TOOL_SEPARATOR = (
    wx.ID_SEPARATOR, wx.NullBitmap, '', wx.ITEM_SEPARATOR)

TOOL_FILE_OPEN = (
    ID_FILE_OPEN, BITMAP_OPEN, 'Open File', wx.ITEM_NORMAL)
TOOL_FILE_SAVE = (
    ID_FILE_SAVE, BITMAP_SAVE, 'Save File', wx.ITEM_NORMAL)

TOOL_VIEW_PGUP = (
    ID_VIEW_PGUP, BITMAP_PGUP, 'Page Up', wx.ITEM_NORMAL)
TOOL_VIEW_PGDN = (
    ID_VIEW_PGDN, BITMAP_PGDN, 'Page Down', wx.ITEM_NORMAL)
TOOL_VIEW_ZOOMIN = (
    ID_VIEW_ZOOMIN, BITMAP_ZOOMIN, 'Zoom In', wx.ITEM_NORMAL)
TOOL_VIEW_ZOOMOUT = (
    ID_VIEW_ZOOMOUT, BITMAP_ZOOMOUT, 'Zoom Out', wx.ITEM_NORMAL)

TOOL_EDIT_CREATE = (
    ID_EDIT_CREATE, BITMAP_CREATE, 'Create', wx.ITEM_NORMAL)
TOOL_EDIT_DELETE = (
    ID_EDIT_DELETE, BITMAP_DELETE, 'Delete', wx.ITEM_NORMAL)
TOOL_EDIT_UNDO = (
    ID_EDIT_UNDO, BITMAP_UNDO, 'Undo', wx.ITEM_NORMAL)
TOOL_EDIT_REDO = (
    ID_EDIT_REDO, BITMAP_REDO, 'Redo', wx.ITEM_NORMAL)
TOOL_EDIT_INCLUDE = (
    ID_EDIT_INCLUDE, BITMAP_INCLUDE, 'Include Mode', wx.ITEM_RADIO)
TOOL_EDIT_OVERLAP = (
    ID_EDIT_OVERLAP, BITMAP_OVERLAP, 'Overlap Mode', wx.ITEM_RADIO)
TOOL_EDIT_EDIT = (
    ID_EDIT_EDIT, BITMAP_EDIT, 'Edit Mode', wx.ITEM_CHECK)

TOOL_BAR_DATA = (TOOL_FILE_OPEN,
                 TOOL_FILE_SAVE,
                 TOOL_SEPARATOR,
                 TOOL_VIEW_PGUP,
                 TOOL_VIEW_PGDN,
                 TOOL_VIEW_ZOOMIN,
                 TOOL_VIEW_ZOOMOUT,
                 TOOL_SEPARATOR,
                 TOOL_EDIT_CREATE,
                 TOOL_EDIT_DELETE,
                 TOOL_EDIT_UNDO,
                 TOOL_EDIT_REDO,
                 TOOL_EDIT_INCLUDE,
                 TOOL_EDIT_OVERLAP,
                 TOOL_SEPARATOR,
                 TOOL_EDIT_EDIT,
                 )
