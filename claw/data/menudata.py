# -*- coding: utf-8 -*-

import wx

from marmot.claw.data.iddata import *

# Menu items
ITEM_SEPARATOR = (
    wx.ID_SEPARATOR, '', '', wx.ITEM_SEPARATOR)

ITEM_FILE_OPEN = (
    ID_FILE_OPEN, 'Open File\tCtrl-O', 'Open File', wx.ITEM_NORMAL)
ITEM_FILE_LOADPREDICT = (
    ID_FILE_LOADPREDICT, 'Load Prediction\tCtrl-L', 'Load Prediction', wx.ITEM_NORMAL)
ITEM_FILE_CLOSE = (
    ID_FILE_CLOSE, 'Close File\tCtrl-W', 'Close File', wx.ITEM_NORMAL)
ITEM_FILE_SAVE = (
    ID_FILE_SAVE, 'Save File\tCtrl-S', 'Save File', wx.ITEM_NORMAL)
ITEM_FILE_EXIT = (
    ID_FILE_EXIT, 'Exit', 'Exit', wx.ITEM_NORMAL)

ITEM_VIEW_PGUP = (
    ID_VIEW_PGUP, 'Prev Page\tPGUP', 'Previous Page', wx.ITEM_NORMAL)
ITEM_VIEW_PGDN = (
    ID_VIEW_PGDN, 'Next Page\tPGDN', 'Next Page', wx.ITEM_NORMAL)
ITEM_VIEW_ZOOMIN = (
    ID_VIEW_ZOOMIN, 'Zoom In', 'Zoom In', wx.ITEM_NORMAL)
ITEM_VIEW_ZOOMOUT = (
    ID_VIEW_ZOOMOUT, 'Zoom Out', 'Zoom Out', wx.ITEM_NORMAL)

ITEM_EDIT_CREATE = (
    ID_EDIT_CREATE, 'Create ...\tINS', 'Create Content', wx.ITEM_NORMAL)
ITEM_EDIT_DELETE = (
    ID_EDIT_DELETE, 'Delete\tDEL', 'Delete Content', wx.ITEM_NORMAL)
ITEM_EDIT_LABEL = (
    ID_EDIT_LABEL, 'Label ...\tF2', 'Logical Labeling', wx.ITEM_NORMAL)
ITEM_EDIT_SORT_X = (
    ID_EDIT_SORT_X, 'Sort by x', 'Sort by x', wx.ITEM_NORMAL)
ITEM_EDIT_SORT_Y = (
    ID_EDIT_SORT_Y, 'Sort by y', 'Sort by y', wx.ITEM_NORMAL)
ITEM_EDIT_UNDO = (
    ID_EDIT_UNDO, 'Undo\tCtrl-Z', 'Undo', wx.ITEM_NORMAL)
ITEM_EDIT_REDO = (
    ID_EDIT_REDO, 'Redo\tCtrl-Y', 'Redo', wx.ITEM_NORMAL)
ITEM_EDIT_LINEUP = (
    ID_EDIT_LINEUP, 'LineUp\tCtrl-L', 'LineUp', wx.ITEM_NORMAL)

ITEM_FILTER_CHAR = (
    ID_FILTER_CHAR, 'Char', 'Char', wx.ITEM_CHECK)
ITEM_FILTER_IMAGE = (
    ID_FILTER_IMAGE, 'Image', 'Image', wx.ITEM_CHECK)
ITEM_FILTER_PATH = (
    ID_FILTER_PATH, 'Path', 'Path', wx.ITEM_CHECK)
ITEM_FILTER_FRAGMENT = (
    ID_FILTER_FRAGMENT, 'Fragment', 'Fragment', wx.ITEM_CHECK)
ITEM_FILTER_BLOCK = (
    ID_FILTER_BLOCK, 'Block', 'Block', wx.ITEM_CHECK)

ITEM_LAYER_CONTENT = (
    ID_LAYER_CONTENT, 'Content', 'Content', wx.ITEM_CHECK)
ITEM_LAYER_GT_LABEL = (
    ID_LAYER_GT_LABEL, 'Label', 'Label', wx.ITEM_CHECK)
ITEM_LAYER_MST = (
    ID_LAYER_MST, 'MSTree', 'MSTree', wx.ITEM_CHECK)
ITEM_LAYER_COL = (
    ID_LAYER_COL, 'Vertical Edge', 'Vertical Edge', wx.ITEM_CHECK)
ITEM_LAYER_OVERLAP = (
    ID_LAYER_OVERLAP, 'Overlap Edge', 'Overlap Edge', wx.ITEM_CHECK)
ITEM_LAYER_TT_A = (
    ID_LAYER_TT_A, 'Text Align Edge', 'Text Align Edge', wx.ITEM_CHECK)
ITEM_LAYER_TT_N = (
    ID_LAYER_TT_N, 'Text Non-Align Edge', 'Text Non-Align Edge', wx.ITEM_CHECK)
ITEM_LAYER_NT_C = (
    ID_LAYER_NT_C, 'Non-Text Above Text Edge', 'Non-Text Above Text Edge', wx.ITEM_CHECK)

ITEM_LAYER_WHITESPACE = (
    ID_LAYER_WHITESPACE, 'Whitespace', 'Whitespace', wx.ITEM_CHECK)

ITEM_HELP_ABOUT = (
    wx.ID_ABOUT, 'About', 'About', wx.ITEM_NORMAL)

# Item groups
FILE_MENU_ITEM_DATA = (ITEM_FILE_OPEN,
                       ITEM_FILE_LOADPREDICT,
                       ITEM_FILE_CLOSE,
                       ITEM_SEPARATOR,
                       ITEM_FILE_SAVE,
                       ITEM_SEPARATOR,
                       ITEM_FILE_EXIT,
                       )
VIEW_MENU_ITEM_DATA = (ITEM_VIEW_PGUP,
                       ITEM_VIEW_PGDN,
                       ITEM_VIEW_ZOOMIN,
                       ITEM_VIEW_ZOOMOUT,
                       )
EDIT_MENU_ITEM_DATA = (ITEM_EDIT_CREATE,
                       ITEM_EDIT_DELETE,
                       ITEM_EDIT_LABEL,
                       ITEM_EDIT_SORT_X,
                       ITEM_EDIT_SORT_Y,
                       ITEM_EDIT_UNDO,
                       ITEM_EDIT_REDO,
                       ITEM_EDIT_LINEUP,
                       )
FILTER_MENU_ITEM_DATA = (ITEM_FILTER_CHAR,
                         ITEM_FILTER_IMAGE,
                         ITEM_FILTER_PATH,
                         ITEM_FILTER_FRAGMENT,
                         ITEM_FILTER_BLOCK,
                         )

LAYER_MENU_ITEM_DATA = (ITEM_LAYER_CONTENT,
                        ITEM_LAYER_GT_LABEL,
                        ITEM_LAYER_MST,
                        ITEM_LAYER_COL,
                        ITEM_LAYER_OVERLAP,
                        ITEM_LAYER_TT_A,
                        ITEM_LAYER_TT_N,
                        ITEM_LAYER_NT_C,
                        ITEM_LAYER_WHITESPACE
                        )

HELP_MENU_ITEM_DATA = (ITEM_HELP_ABOUT,
                       )

# Menu
FILE_MENU_DATA = ('File', FILE_MENU_ITEM_DATA)
VIEW_MENU_DATA = ('View', VIEW_MENU_ITEM_DATA)
EDIT_MENU_DATA = ('Edit', EDIT_MENU_ITEM_DATA)
FILTER_MENU_DATA = ('Filter', FILTER_MENU_ITEM_DATA)
LAYER_MENU_DATA = ('Layer', LAYER_MENU_ITEM_DATA)
HELP_MENU_DATA = ('Help', HELP_MENU_ITEM_DATA)

# Menu bar
MENU_BAR_DATA = (FILE_MENU_DATA,
                 VIEW_MENU_DATA,
                 EDIT_MENU_DATA,
                 FILTER_MENU_DATA,
                 LAYER_MENU_DATA,
                 HELP_MENU_DATA,
                 )

# pop-up menu
FLAT_POPUP_MENU_ITEM_DATA = (ITEM_EDIT_CREATE,
                             ITEM_EDIT_DELETE,
                             ITEM_EDIT_LABEL,
                             ITEM_EDIT_SORT_X,
                             ITEM_EDIT_SORT_Y,
                             ITEM_EDIT_UNDO,
                             ITEM_EDIT_REDO,
                             )
