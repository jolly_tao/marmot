import wx
import sys
import os

path = os.path.abspath(__file__)
for i in range(3):
    path = os.path.dirname(path)
sys.path.insert(0, path)

from marmot.claw.view.frame import ClawFrame

class ClawApp(wx.App):
    """Application class."""

    def OnInit(self):
        self.frame = ClawFrame()
        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True

def main():
    app = ClawApp()
    app.MainLoop()

if __name__ == '__main__':
    main()
