# coding: utf-8

import os
import wx
from lazy import lazy
import traceback

from marmot.common.rect import Rect
from marmot.common.label import LABEL_RAW
from marmot.common.label import LABEL_PHYSICAL
from marmot.common.document import Document
from marmot.claw.model.editor import PhysicalEditor
from marmot.brain.feature.context import Context

class ContentManager(object):
    """claw.model.Document manages data for labeling.
    """
    def __init__(self, dir):
        """
        @param directory    Dir containing the target document, where
                            directory/raw contains raw content,
                            directory/physical contains physical content,
                            directory/logical contains logical content
        """
        # TODO: bury page access into document class
        self._dir = dir
        self._doc_name = os.path.basename(dir)
        self._prediction = dict()
   

        self.doc = Document(self._dir)

        self._editor = PhysicalEditor()
        self._page_num = 0

        self.turn_page(min(self.doc.raw.iterkeys()))

    @property
    def page_num(self):
        return self._page_num

    @property
    def page_count(self):
        return self.doc.raw.page_count

    @property
    def dir(self):
        return self._dir

    def turn_page(self, page_num):
        """Turn to page #page_num
        """
        # same page
        if page_num == self._page_num:
            return False

        # invalid page
        if page_num not in self.doc.raw:
            return False

        self._page_num = page_num

        # update raw page and thumb
        self._thumb = wx.Bitmap(self.raw_page.thumb)

        # clear editor
        self._editor.clear()
        self._prediction = {}

        # invalidate context
        lazy.invalidate(self, 'context')
        return True

    def save(self):
        self.doc.physical.flush(self._page_num)

    @property
    def doc_name(self):
        return self._doc_name

    @property
    def thumb(self):
        return self._thumb

    @property
    def raw_page(self):
        return self.doc.raw[self._page_num]

    @property
    def physical_page(self):
        return self.doc.physical[self._page_num]
    
    @property
    def prediction(self):
        return self._prediction

    @prediction.setter
    def prediction(self, prediction):
        self._prediction = prediction    

    @property
    def editor(self):
        return self._editor
    
    @lazy
    def context(self):
        img_path = os.path.join(self.dir, 'raw', str(self.page_num) + '.png')
        return Context(self.physical_page, img_path)
