# -*- coding: utf-8 -*-

from abc import ABCMeta
from abc import abstractmethod

from marmot.common.label import LABEL_FRAGMENT
from marmot.common.label import LABEL_BLOCK
from marmot.common.physical.content import Fragment
from marmot.common.physical.content import Block

label2class = {
    LABEL_FRAGMENT: Fragment,
    LABEL_BLOCK:    Block
    }

class Command(object):
    """Abstract base class for edit command.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def do(self):
        pass

    @abstractmethod
    def undo(self):
        pass

class CreateCommand(Command):

    def __init__(self, container, label, children):
        self._done = False
        self._label = label
        self._container = container
        self._children_ids = [child.id for child in children]

    def do(self):
        # TODO: exception handling
        if not self._done:
            klass = label2class[self._label]
            self._id = self._container.id_manager.allocate_id(self._label)
            self._content = klass(self._id, self._container,
                                  self._children_ids)
        else:
            self._container.id_manager.claim_id(self._id)
        self._container[self._id] = self._content

    def undo(self):
        del self._container[self._id]
        self._container.id_manager.reclaim_id(self._id)

class DeleteCommand(Command):

    def __init__(self, container, content):
        self._container = container
        self._content = content

    def do(self):
        del self._container[self._content.id]
        self._container.id_manager.reclaim_id(self._content.id)

    def undo(self):
        self._container.id_manager.claim_id(self._content.id)
        self._container[self._content.id] = self._content

class LabelCommand(Command):

    def __init__(self, content, logical_label):
        self._content = content
        self._label = logical_label
        self._old_label = content.logical_label

    def do(self):
        self._content.logical_label = self._label

    def undo(self):
        self._content.logical_label = self._old_label

class SortCommand(Command):

    def __init__(self, content, direction):
        self._content = content
        self._direction = direction

    def do(self):
        if self._direction == 0:
            self._content.sort(key=lambda x: x.box.x0)
        else:
            self._content.sort(key=lambda x: x.box.y0)

    def undo(self):
        # not implemented
        pass
        
class MacroCommand(Command):

    def __init__(self, commands):
        self._commands = commands

    def do(self):
        for command in self._commands:
            command.do()

    def undo(self):
        for command in reversed(self._commands):
            command.undo()
