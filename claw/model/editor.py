# -*- coding: utf-8 -*-

from marmot.claw.model.command import CreateCommand
from marmot.claw.model.command import DeleteCommand
from marmot.claw.model.command import LabelCommand
from marmot.claw.model.command import SortCommand
from marmot.claw.model.command import MacroCommand
from marmot.common.label import LABEL_FRAGMENT
from marmot.common.label import LABEL_BLOCK

class Editor(object):

    def __init__(self):
        self._commands = []
        # commands[todo_index:] are commands to commit
        self._todo_index = 0
        self._is_saved = True
        self._saved_index = -1

    def append(self, command):
        del self._commands[self._todo_index:]
        self._commands.append(command)
        if self._is_saved and self._todo_index <= self._saved_index:
            self._is_saved = False
        command.do()
        self._todo_index += 1

    def undo(self):
        if self._todo_index > 0:
            self._todo_index -= 1
            self._commands[self._todo_index].undo()
            return True
        return False

    def redo(self):
        if self._todo_index < len(self._commands):
            self._commands[self._todo_index].do()
            self._todo_index += 1
            return True
        return False

    def save(self):
        self._is_saved = True
        self._saved_index = self._todo_index - 1

    def clear(self):
        del self._commands[:]
        self._todo_index = 0
        self._is_saved = True
        self._saved_index = -1

    def is_dirty(self):
        return not (self._is_saved and
                    self._todo_index == self._saved_index + 1)

# TODO: define concrete edit error
class EditError(Exception):
    """Edit error base class."""
    pass

class MixedSelectionError(EditError):
    """Selection contains contents of different label or category."""
    pass

class EmptySelectionError(EditError):
    """Selection contains no contents."""
    pass

class UnmatchedCreateError(EditError):
    """Create physical content from wrong type of contents."""
    pass

class DeleteRawError(EditError):
    """Delete raw content."""
    pass
class LabelNonFragmentError(EditError):
    """Put a logical label on a non-fragment content."""
    pass

class SortNonFragmentError(EditError):
    """Sort a non-fragment content."""
    pass

class PhysicalEditor(Editor):
    def __init__(self):
        Editor.__init__(self)

    def create(self, container, label, children):
        if not children:
            raise EmptySelectionError
        children = list(children)
        if not all(x.label == children[0].label for x in children):
            raise MixedSelectionError
        if not ((label == LABEL_FRAGMENT and
                 children[0].category == 'raw') or
                 (label == LABEL_BLOCK and
                  children[0].label == LABEL_FRAGMENT)):
            raise UnmatchedCreateError

        self.append(CreateCommand(container, label, children))

    def delete(self, container, contents):
        if not contents:
            raise EmptySelectionError
        if any(x.category == 'raw' for x in contents):
            raise DeleteRawError

        commands = [DeleteCommand(container, content)
                    for content in contents]
        self.append(MacroCommand(commands))

    def label(self, contents, label):
        if not contents:
            raise EmptySelectionError
        if not all(x.label == LABEL_FRAGMENT for x in contents):
            raise LabelNonFragmentError

        commands = [LabelCommand(content, label)
                    for content in contents]
        self.append(MacroCommand(commands))

    def sort(self, contents, direction):
        if not contents:
            raise EmptySelectionError
        if not all(x.label == LABEL_FRAGMENT for x in contents):
            raise SortNonFragmentError
        commands = [SortCommand(content, direction)
                    for content in contents]
        self.append(MacroCommand(commands))
        
