Modules
=======
* common: Shared definition, e.g. pages and documents.
* claw:   Wxpython based ground-truthing gui.
* brain:  Training and predicting page fragments using CRF.

Requirements:
=============
* lxml
* lazy
* wxpython
* rtree
* scipy
* opencv
* python-graph-core
* regex
* scikit-learn
* opengm
